#include "read_abc.h"

void print_num_table(){
	printf("CONSTS:\n");
	printf("%u\n", num_no);

	if(num_no>0){
		for(unsigned i=0; i<num_no; i++){
			printf("%f\n", num_consts[i].d);
		}
	}
}

void print_string_table(){
	printf("STRINGS:\n");
	printf("%u\n", str_no);

	if(str_no>0){
		for(unsigned i=0; i<str_no; i++){
			printf("%u %s\n", (unsigned)strlen(str_consts[i].str), str_consts[i].str);
		}
	}
}

void print_libfunc_table(){
	printf("LIBRARY FUNCS:\n");
	printf("%u\n", lib_no);

	if(lib_no>0){
		for(unsigned i=0; i<lib_no; i++){
			printf("%s\n", lib_consts[i].str);
		}
	}
}

void print_userfunc_table(){
	printf("USER FUNCS:\n");
	printf("%u\n", usr_no);

	if(usr_no>0){
		for(unsigned i=0; i<usr_no; i++){
			printf("%u %u %s \n", usr_consts[i].address, usr_consts[i].localsize, usr_consts[i].id);
		}
	}
}


void print_vmarg(vmarg* arg){
	if(arg!=NULL)
		printf("%d:%u ", arg->type, arg->value);
}


void print_code(){
	printf("INSTRUCTIONS:\n");
	printf("%u\n", code_no);

	if(code_no>0){
		for(unsigned i=0; i<code_no; i++){
			printf("%u ", code[i].opcode);
			print_vmarg(&code[i].arg1);
			print_vmarg(&code[i].arg2);
			print_vmarg(&code[i].result);
			printf("\n");
		}
	}
}


void print_tables(){
	print_string_table();
	print_num_table();
	print_userfunc_table();
	print_libfunc_table();
}


void print_magicNum(){
	printf("%d\n",magicnum);
}


void print_globals_size(){
	printf("%d\n",globals_size);
}


void print_input_binary(){
	print_magicNum();
	print_tables();
	print_code();
}


int read_binary(){
	FILE *fd;
	unsigned i, size;

	fd=fopen(BINARY_FILE,"rb");
	if (!fd){
		fprintf(stderr, "Unable to open file '%s'!", BINARY_FILE);
		return 1;
	}

	// magic number
	fread(&magicnum,sizeof(unsigned),1,fd);

	// number of globals
	fread(&globals_size,sizeof(unsigned),1,fd);

	// constant strings
	fread(&str_no,sizeof(unsigned),1,fd);

	if(str_no==0)
		str_consts= NULL;
	else{
		str_consts= malloc(sizeof(lista_string) * str_no);
		
		for(i=0; i<str_no; i++){
			fread(&size,sizeof(unsigned),1,fd);
			str_consts[i].str= malloc(size);

			fread(str_consts[i].str,sizeof(char),size,fd);
		}
	}

	// constant numbers
	fread(&num_no,sizeof(unsigned),1,fd);

	if(num_no==0)
		num_consts= NULL;
	else{
		num_consts= malloc(sizeof(lista_numbers) * num_no);
		
		for(i=0; i<num_no; i++){
			fread(&num_consts[i].d,sizeof(double),1,fd);
		}
	}

	// user functions	
	fread(&usr_no,sizeof(unsigned),1,fd);

	if(usr_no==0)
		usr_consts= NULL;
	else{
		usr_consts= malloc(sizeof(lista_userfunc) * usr_no);
		
		for(i=0; i<usr_no; i++){
			fread(&usr_consts[i].address,sizeof(unsigned),1,fd);
			fread(&usr_consts[i].localsize,sizeof(unsigned),1,fd);

			fread(&size,sizeof(unsigned),1,fd);
			usr_consts[i].id= malloc(size);

			fread(usr_consts[i].id,sizeof(char),size,fd);
		}
	}
	
	// library functions
	fread(&lib_no,sizeof(unsigned),1,fd);

	if(lib_no==0)
		lib_consts= NULL;
	else{
		lib_consts= malloc(sizeof(lista_libfunc) * lib_no);
		
		for(i=0; i<lib_no; i++){
			fread(&size,sizeof(unsigned),1,fd);
			lib_consts[i].str= malloc(size);

			fread(lib_consts[i].str,sizeof(char),size,fd);
		}
	}

	// code
	fread(&code_no,sizeof(unsigned),1,fd);

	if(code_no==0)
		code= NULL;
	else{
		code= malloc(sizeof(instruction) * code_no);
		
		for(i=0; i<code_no; i++){
			fread(&code[i].opcode,sizeof(unsigned char),1,fd);

			fread(&code[i].arg1.type,sizeof(unsigned char),1,fd);
			fread(&code[i].arg1.value,sizeof(unsigned),1,fd);

			fread(&code[i].arg2.type,sizeof(unsigned char),1,fd);
			fread(&code[i].arg2.value,sizeof(unsigned),1,fd);

			fread(&code[i].result.type,sizeof(unsigned char),1,fd);
			fread(&code[i].result.value,sizeof(unsigned),1,fd);
		}
	}

	fclose(fd);
	return 0;
}
