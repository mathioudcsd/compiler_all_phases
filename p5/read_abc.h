#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BINARY_FILE "target.abc"
typedef enum vmarg_t{
	label_a	=0,
	global_a =1,
	formal_a =2,
	local_a =3,
	number_a =4,
	string_a =5,
	bool_a =6,
	nil_a =7,
	userfunc_a =8,
	libfunc_a =9,
	retval_a =10,
	unused_a = 86
}vmarg_t;

typedef enum vmopcode{
	assign_v, /*0*/
	add_v, /*1*/
	sub_v, /*2*/
	mul_v, /*3*/ 
	div_v, /*4*/
	mod_v, /*5*/
	uminus_v, /*6*/
	and_v, /*7*/
	or_v, /*8*/
	not_v, /*9*/
	jeq_v, /*10*/
	jne_v, /*11*/
	jle_v, /*12*/
	jge_v, /*13*/
	jlt_v, /*14*/
	jgt_v, /*15*/
	call_v, /*16*/
	pusharg_v, /*17*/
	funcenter_v, /*18*/
	funcexit_v, /*19*/
	newtable_v, /*20*/
	tablegetelem_v, /*21*/
	tablesetelem_v, /*22*/
	nop_v, /*23*/
	jump_v /*24*/
}vmopcode;

typedef struct vmarg{
	unsigned char type;
	unsigned value;
}vmarg;

typedef struct instruction {
	unsigned char opcode;
	vmarg arg1;
	vmarg arg2;
	vmarg result;
	unsigned srcLine;
}instruction;

typedef struct lista_string{
	char* str;
}lista_string;

typedef struct lista_numbers{
	double d;
}lista_numbers;

typedef struct lista_libfunc{
	char* str;
}lista_libfunc;

typedef struct lista_userfunc{
	unsigned address;
	unsigned localsize;
	char* id;
}lista_userfunc;


unsigned magicnum;
unsigned globals_size;
	
unsigned str_no;
lista_string* str_consts;

unsigned num_no;
lista_numbers* num_consts;

unsigned usr_no;
lista_userfunc* usr_consts;

unsigned lib_no;
lista_libfunc* lib_consts;

unsigned code_no;
instruction* code;


void print_num_table();
void print_string_table();
void print_libfunc_table();
void print_userfunc_table();
void print_vmarg(vmarg* arg);
void print_code();
void print_tables();
void print_magicNum();
void print_globals_size();
void print_input_binary();

int read_binary();
