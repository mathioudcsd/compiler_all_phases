#include "avm_gc.h"
#include <string.h>
#include <math.h>



typedef void (*execute_func_t)(instruction*);

execute_func_t executeFuncs[]={

	execute_assign,
	execute_arithmetic,
	execute_arithmetic,
	execute_arithmetic,
	execute_arithmetic,
	execute_arithmetic,
	execute_uminus,
	execute_and,
	execute_or,
	execute_not,
	execute_jeq,
	execute_jne,
	execute_jle,
	execute_jge,
	execute_jlt,
	execute_jgt,
	execute_call,
	execute_pusharg,
	execute_funcenter,
	execute_funcexit,
	execute_newtable,
	execute_tablegetelem,
	execute_tablesetelem,
	execute_nop,
	execute_jump
};

typedef void (*memclear_func_t) (avm_memcell*);

memclear_func_t memclearFuncs[]={
	0, /* number */
	memclear_string,
	0, /* bool */
	memclear_table,
	0, /* userfunc */
	0, /* libfunc */
	0, /* nil */
	0  /* undef */ 
};


typedef char * (*tostring_func_t) (avm_memcell*);

tostring_func_t tostringFuncs[]={
	number_tostring,
	string_tostring,
	bool_tostring,
	table_tostring,
	userfunc_tostring,
	libfunc_tostring,
	nil_tostring,
	undef_tostring
};


typedef double (*arithmetic_func_t) (double x, double y);

arithmetic_func_t arithmeticFuncs[]={ /*Dispatcher just for arithmetic functions */
	add_impl,
	sub_impl,
	mul_impl,
	div_impl,
	mod_impl
};

typedef unsigned char (*tobool_func_t) (avm_memcell*);

tobool_func_t toboolFuncs[]={
	number_tobool,
	string_tobool,
	bool_tobool,
	table_tobool,
	userfunc_tobool,
	libfunc_tobool,
	nil_tobool,
	undef_tobool
};



char *typeStrings[] = {
	"number",
	"string",
	"bool",
	"table",
	"userfunc",
	"libfunc",
	"nil",
	"undef"
};

//-------------------------------------------------------------
unsigned str_sum(char *str){
	unsigned sum=0;
	int i=0;
	while(i < strlen(str) -1){
		sum += str[i];
		i++;
	}
	return sum;
}

int hashThat(avm_memcell* a){
	double leftpart;
	double rightpart;
	switch (a->type){

	case number_m:	
		rightpart = modf(a->data.numVal, &leftpart);
		return (int)leftpart%211;
	case string_m:
		return str_sum(a->data.strVal)%211;
	case bool_m:
		if(a->data.boolVal)
			return 1;
		return 0;
	case table_m:
		return a->data.tableVal->randomID; 
	case userfunc_m:
		return a->data.funcVal%211;
	case libfunc_m:
		return atoi(a->data.libfuncVal)%211;
	case nil_m:
		assert(0);
	case undef_m:
		assert(0);
	}
}

void avm_table_destroy(avm_table* t){
	avm_tablebucket_destroy(t->strIndexed);
	avm_tablebucket_destroy(t->numIndexed);
	free(t);
}

avm_memcell_e decide_value_type(avm_table_bucket* b){
	return b->value->type;
}


/* !!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!! 
 * To "tbl" einai proswrini metabliti
 * mexri na dw PWS STO MPOUTSO
 * tha ksekatharizoume kai tha anaferomaste
 * se pollous pinakes
 * 
 * Otan kanoume access ta strIndexed, numIndexed etc
 * prepei na kseroume gia poion ap olous tous pinakes anaferomaste
 */
avm_memcell* avm_tablegetelem(avm_table* tbl, avm_memcell* key){
	int result = hashThat(key);
	printf("hashkey-get: %d\n",result );
	avm_table_bucket* ptr;
	switch (key->type){
	
	case number_m:
		ptr = tbl->numIndexed[result];
		while(ptr->key->data.numVal != key->data.numVal)
			ptr=ptr->next;
		assert(ptr!=NULL && "Out of bounds(avm_getelem)");
		return ptr->value;
		
	case string_m:
		ptr = tbl->strIndexed[result];
		while(ptr->key->data.strVal != key->data.strVal)
			ptr=ptr->next;
		assert(ptr!=NULL && "Out of bounds(avm_getelem)");
		return ptr->value;
		
	case bool_m:
		ptr = tbl->boolIndexed[result];
		while(ptr->key->data.boolVal != key->data.boolVal)
			ptr=ptr->next;
		assert(ptr!=NULL && "Out of bounds(avm_getelem)");
		return ptr->value;
		
	case table_m:
		assert(0 && "Table?? nop.");
		return NULL;
		
	case userfunc_m:
		ptr=tbl->userIndexed[result];
		while(ptr->key->data.funcVal != key->data.funcVal)
			ptr=ptr->next;
		assert(ptr!=NULL && "Out of bounds(avm_getelem)");
		return ptr->value;
		
	case libfunc_m:
		ptr=tbl->libIndexed[result];
		while(ptr->key->data.libfuncVal != key->data.libfuncVal)
			ptr=ptr->next;
		assert(ptr!=NULL && "Out of bounds(avm_getelem)");
		return ptr->value;
	case nil_m:
		assert(0);
	case undef_m:
		assert(0);
	}
	
}

/* Table buckets init, will initialize the first bucket pointer 
 * in an avm_table_bucket cell. The first bucket pointer in the cell
 * remains unallocated, just like a HEAD pointer.
 */

void avm_table_buckets_init(avm_table_bucket** b){
	int i;
		b = (avm_table_bucket**) 0;	
}
void avm_memcell_clear(avm_memcell* m){
	switch (m->type){

	case number_m:
		m->type=undef_m;
		m->data.numVal=0;
	case string_m:
		m->type=undef_m;
		m->data.strVal="";
	case bool_m:
		m->type=undef_m;
		m->data.boolVal=0;
    case libfunc_m:
		m->type=undef_m;
		m->data.libfuncVal="";
	case userfunc_m:
		m->type=undef_m;
		m->data.funcVal=0;		
	}
}

void avm_tablesetelem(avm_table *tbl, avm_memcell *key, avm_memcell *value){
	int result = hashThat(key);
	
	avm_table_bucket* buff = (avm_table_bucket*)malloc(sizeof(avm_table_bucket));
	buff->key=malloc(sizeof(avm_memcell));
	buff->value=malloc(sizeof(avm_memcell));
	switch (key->type){
		case number_m:
			if(tbl->numIndexed[result]==NULL){ 
				tbl->numIndexed[result]=buff;
				buff->next=NULL;

				buff->key->type=key->type;
				buff->key->data.numVal=key->data.numVal;
			}
			else{
				buff->next = tbl->numIndexed[result];
				tbl->numIndexed[result]=buff;

				buff->key->type=key->type;
				buff->key->data.numVal=key->data.numVal;
			}
			tbl->totalnum++;
			break ;
		case string_m:
			if(tbl->strIndexed[result]==NULL){ 
				tbl->strIndexed[result]=buff;
				buff->next=NULL;

				buff->key->type=key->type;
				buff->key->data.strVal=key->data.strVal;
			}
			else{
				buff->next = tbl->strIndexed[result];
				tbl->strIndexed[result]=buff;

				buff->key->type=key->type;
				buff->key->data.strVal=key->data.strVal;
			}
			tbl->totalstr++;
			break ;
		case bool_m:
			if(tbl->boolIndexed[result]==NULL){ 
				tbl->boolIndexed[result]=buff;
				buff->next=NULL;

				buff->key->type=key->type;
				buff->key->data.boolVal=key->data.boolVal;
			}
			else{
				buff->next = tbl->boolIndexed[result];
				tbl->boolIndexed[result]=buff;

				buff->key->type=key->type;
				buff->key->data.boolVal=key->data.boolVal;
			}
			tbl->totalbool++;
			break ;
		case table_m:
			assert(0 && "E OXI KAI TABLE STO KEY.......... krima");
			return;
		case userfunc_m:
			if(tbl->userIndexed[result]==NULL){ 
				tbl->userIndexed[result]=buff;
				buff->next=NULL;

				buff->key->type=key->type;
				buff->key->data.funcVal=key->data.funcVal;
			}
			else{
				buff->next = tbl->userIndexed[result];
				tbl->userIndexed[result]=buff;

				buff->key->type=key->type;
				buff->key->data.funcVal=key->data.funcVal;
			}
			tbl->totaluser++;
			break ;
		case libfunc_m:
			if(tbl->libIndexed[result]==NULL){ 
				tbl->libIndexed[result]=buff;
				buff->next=NULL;

				buff->key->type=key->type;
				buff->key->data.libfuncVal=key->data.libfuncVal;
			}
			else{
				buff->next = tbl->libIndexed[result];
				tbl->libIndexed[result]=buff;

				buff->key->type=key->type;
				buff->key->data.libfuncVal=key->data.libfuncVal;
			}
			tbl->totallib++;
			break ;
		case nil_m:
		case undef_m:
			assert(0);
	}

	switch(value->type){
		case number_m:
			buff->value->type=value->type;
			buff->value->data.numVal=value->data.numVal;
			return;
		case string_m:
			buff->value->type=value->type;
			buff->value->data.strVal=value->data.strVal;
			return;
		case bool_m:
			buff->value->type=value->type;
			buff->value->data.boolVal=value->data.boolVal;
			return;
		case table_m:
			buff->value->type=value->type;
			buff->value->data.tableVal=value->data.tableVal;
			return;
		case userfunc_m:
			buff->value->type=value->type;
			buff->value->data.tableVal=value->data.tableVal;
			return;
		case libfunc_m:
			buff->value->type=value->type;
			buff->value->data.libfuncVal=value->data.libfuncVal;
			return;
		case nil_m:
			assert("value of table element is nil");
			return;
		case undef_m:
			assert("value of table element is undef");
			return;
	}
	
}

void avm_tableincrefcounter(avm_table* t){
	++t->refCounter;
}

void avm_tabledecrefcounter(avm_table* t){
	assert(t->refCounter > 0);
	if(!--t->refCounter)
		avm_table_destroy(t);
}

avm_table* avm_tablenew(){

	avm_table* t = (avm_table*)malloc(sizeof(avm_table));
	AVM_WIPEOUT(*t);
	
  
	t->refCounter = t->totalstr = t->totalnum = 0;

	avm_table_buckets_init(t->strIndexed);
	avm_table_buckets_init(t->numIndexed);
	avm_table_buckets_init(t->boolIndexed);
	avm_table_buckets_init(t->libIndexed);
	avm_table_buckets_init(t->userIndexed);
	
	return t;
}

void avm_tablebucket_destroy(avm_table_bucket** p){
	unsigned i;
	for(i=0; i<AVM_TABLE_HASHSIZE; ++i, ++p){
		avm_table_bucket* b;
		for( b = *p; b;){
			avm_table_bucket* del = b;
			b=b->next;
			avm_memcell_clear(del->key);
			avm_memcell_clear(del->value);
			free(del);
		}
		p[i] = (avm_table_bucket*) 0; /* dafuq is this cast */
	}
}

/* initialize the whole stack with undef */
static void avm_initstack(void){
	unsigned i;
	for(i=0; i<AVM_STACKSIZE; ++i){
		AVM_WIPEOUT(stack[i]);
		stack[i].type = undef_m;
	}

}


//---------------------------------------------------------

double consts_getnumber (unsigned index){
	return num_consts[index].d;
}

char* consts_getstring (unsigned index){
	return str_consts[index].str;
}

char* libfuncs_getused (unsigned index){
	return lib_consts[index].str;
}

unsigned usrfuncs_getaddress (unsigned index){
	return usr_consts[index].address;
}




avm_memcell* avm_translate_operand(vmarg *arg, avm_memcell *reg){

	switch(arg->type){
		/*variable*/
		case global_a: 
			return &stack[AVM_STACKSIZE -1 -arg->value];
		case local_a:
			return &stack[topsp - arg->value];
		case formal_a:
			return &stack[topsp + AVM_STACKENV_SIZE + 1 + arg->value];
		case retval_a:
			return &retval;
		case number_a:
			reg->type = number_m;
			reg->data.numVal = consts_getnumber(arg->value);
			return reg;
		case string_a:
			reg->type = string_m;
			reg->data.strVal = strdup( consts_getstring(arg->value)) ;
			return reg;
		case bool_a:
			reg->type = bool_m;
			reg->data.boolVal = arg->value;
			return reg;
		case nil_a:
			reg->type = nil_m;
			return reg;
		case userfunc_a:
			reg->type = userfunc_m;
			reg->data.funcVal = usrfuncs_getaddress(arg->value); /*address already stored */
			return reg;
		case libfunc_a:
			reg->type = libfunc_m;
			reg->data.libfuncVal = libfuncs_getused(arg->value);
			return reg;
		case unused_a:
			reg->type = nil_m;
			return reg;
		default: assert(0);
			/* prosoxh an einai unused_a 8a kanei assert */
	}
}


void execute_cycle(){

	while(pc < AVM_ENDING_PC ){
		skip_function_definition();

		if(executionFinised)
			return;
		else if(pc == AVM_ENDING_PC){
			executionFinised = 1;
			return;
		}else{
			assert(pc < AVM_ENDING_PC);
			
			instruction* instr = code + pc;
			
			assert(instr->opcode >= 0 && instr->opcode <= AVM_MAX_INSTRUCTIONS );
			/*
			if(instr->srcLine)
				currLine = instr->srcLine;
			*/
		//	printf("--> pc: %u\n", pc);
			unsigned oldPC = pc;
			(*executeFuncs[instr->opcode])(instr);
			
			if(pc == oldPC)
				++pc;
		}
	}

}


void avm_memcellclear (avm_memcell *m){
	if (m->type != undef_m){
		memclear_func_t f = memclearFuncs[m->type];
		if(f)
			(*f)(m);
		m->type = undef_m;
	}
}

void execute_assign (instruction* instr){
	avm_memcell *lv = avm_translate_operand(&instr->result, (avm_memcell*) 0);
	avm_memcell *rv = avm_translate_operand(&instr->arg1, &ax);

	assert(lv && (&stack[AVM_STACKSIZE-1] >= lv && lv > &stack[top] || lv == &retval));
	assert(rv); //should do similar assertion tests here

	avm_assign(lv,rv);
}

void avm_assign (avm_memcell *lv, avm_memcell *rv){
	if(lv == rv)
		return;
	if( (lv->type == table_m) && (rv->type == table_m) && (lv->data.tableVal == rv->data.tableVal))
		return;
	if (rv->type == undef_m)
		printf("warning: assigning from 'undef' content!\n");

	avm_memcellclear(lv);

	memcpy(lv, rv, sizeof(avm_memcell));

	/*now take care of copied values or reference counting*/
	if( lv->type == string_m)
		lv->data.strVal =strdup( rv->data.strVal );
	else if ( lv->type == table_m )
		avm_tableincrefcounter(lv->data.tableVal);	
}

void execute_call (instruction *instr){
	char* s;
	avm_memcell *func = avm_translate_operand(&instr->arg1, &ax);
	assert(func);
	avm_callsaveenviroment();

	called_function=1;

	switch (func->type) {
		case userfunc_m:
			pc = func->data.funcVal;
			assert(pc < AVM_ENDING_PC);
			assert(code[pc].opcode == funcenter_v);
			break;
		case string_m:
			avm_calllibfunc(func->data.strVal);
			break;
		case libfunc_m:
			avm_calllibfunc(func->data.libfuncVal);
			break;
		default:
			s = avm_tostring(func);
			printf("error: call: cannot bind '%s' to function!", s);
			free(s);
			executionFinised = 1;
	}
}

void avm_dec_top (){
	if( !top ){ 
		printf("error: stack overflow\n");
		executionFinised = 1 ;
	}else
		--top;
}

void avm_push_envvalue (unsigned val){
	stack[top].type = number_m;
	stack[top].data.numVal = val;
	avm_dec_top();
}

void avm_callsaveenviroment(){
	avm_push_envvalue(totalActuals);
	avm_push_envvalue(pc +1);
	avm_push_envvalue(top + totalActuals +2);
	avm_push_envvalue(topsp);
}



void execute_funcenter (instruction *instr){
	avm_memcell *func = avm_translate_operand(&instr->arg1, &ax);
	assert(func);
	assert(pc == func->data.funcVal);

	/* callee actions here */
	totalActuals = 0;
	lista_userfunc *funcInfo = avm_getfuncinfo(pc);
	topsp = top;
	top = top - funcInfo->localsize;
}

unsigned avm_get_envvalue (unsigned i){
	assert(stack[i].type == number_m);
	unsigned val = (unsigned) stack[i].data.numVal;
	assert(stack[i].data.numVal == ((double) val) );
	return val;
}

void execute_funcexit(instruction *unused){
	unsigned oldTop = top;
	top = avm_get_envvalue(topsp + AVM_SAVEDTOP_OFFSET);
	pc = avm_get_envvalue(topsp + AVM_SAVEDPC_OFFSET);
	topsp = avm_get_envvalue(topsp + AVM_SAVEDTOPSP_OFFSET);

	while( ++oldTop <= top ){
		avm_memcellclear(&stack[oldTop]);
	}
}

void avm_calllibfunc (char *id){
	library_func_t f = avm_getlibraryfunc(id);

	if( !f ){
		printf("error: error: unsupported lib func '%s' called!", id);
		executionFinised = 1;
	}else{
		/* notice that enter function and exit function are called manually! */
		topsp = top; //enter function sequence. No stack locals.
		totalActuals = 0;
		(*f)();	//call library function
		if(!executionFinised) //an error may naturally occur inside
			execute_funcexit((instruction*) 0); //return sequence
	}
}

unsigned avm_totalactuals (void){
	return avm_get_envvalue(topsp + AVM_NUMACTUALS_OFFSET);
}

avm_memcell* avm_getactual (unsigned i){
	assert(i < avm_totalactuals());
	return &stack[topsp + AVM_STACKENV_SIZE +1 +i];
}


/*with the following every library function is 
manually added in the VM library function resolution map */

void execute_pusharg (instruction *instr){
	avm_memcell *arg = avm_translate_operand(&instr->arg1, &ax);
	assert(arg);

	/*this is actually stack[top]=arg, but we have to use avm_assign*/
	avm_assign(&stack[top], arg);
	++totalActuals;
	avm_dec_top();
}

char* avm_tostring (avm_memcell *m){
	assert(m->type >= 0 && m->type <= undef_m);
	return (*tostringFuncs[m->type]) (m) ;
}

double add_impl (double x, double y){ return x+y; }
double sub_impl (double x, double y){ return x-y; }
double mul_impl (double x, double y){ return x*y; }
double div_impl (double x, double y){
	if(y==0){
		printf("error: can not divide by zero!");
		executionFinised = 1;
		assert(0);
	}
	return x/y;
}
double mod_impl (double x, double y){
	if(y==0){
		printf("error: can not divide by zero!");
		executionFinised = 1;
		assert(0);
	}
	return ((unsigned) x) % ((unsigned) y);
}

void execute_arithmetic (instruction *instr){
	avm_memcell *lv = avm_translate_operand(&instr->result, (avm_memcell*) 0);
	avm_memcell *rv1 = avm_translate_operand(&instr->arg1, &ax);
	avm_memcell *rv2 = avm_translate_operand(&instr->arg2, &bx);

	assert(lv && (&stack[AVM_STACKSIZE-1] >= lv && lv > &stack[top] || lv==&retval));
	assert(rv1 && rv2);

	if(rv1->type != number_m || rv2->type != number_m){
		printf("error: not a number in arithmetic!");
		executionFinised = 1;
	}else{
		arithmetic_func_t op = arithmeticFuncs[instr->opcode - add_v];
		avm_memcellclear(lv);
		lv->type = number_m;
		lv->data.numVal = (*op) (rv1->data.numVal, rv2->data.numVal);
	}
}


unsigned char number_tobool (avm_memcell *m){ return m->data.numVal != 0; }
unsigned char string_tobool (avm_memcell *m){ return m->data.strVal[0] != 0; }
unsigned char bool_tobool (avm_memcell *m){ return m->data.boolVal; }
unsigned char table_tobool (avm_memcell *m){ return 1; }
unsigned char userfunc_tobool (avm_memcell *m){ return 1; }
unsigned char libfunc_tobool (avm_memcell *m){ return 1; }
unsigned char nil_tobool (avm_memcell *m){ return 0; }
unsigned char undef_tobool (avm_memcell *m){ assert(0); return 0; }

unsigned char avm_tobool (avm_memcell *m){
	assert(m->type >= 0 && m->type < undef_m);
	return (*toboolFuncs[m->type]) (m);
}



/* IMPLEMENTATION OF DISPATCHER 'EQUAL' */
typedef unsigned char (*eq_func_t) (avm_memcell*, avm_memcell*);


unsigned char number_eq (avm_memcell *m1, avm_memcell *m2){
	return m1->data.numVal == m2->data.numVal;
}
unsigned char string_eq (avm_memcell *m1, avm_memcell *m2){
	if(strcmp(m1->data.strVal, m2->data.strVal)==0)
		return 1;
	else
		return 0;
}
unsigned char bool_eq (avm_memcell *m1, avm_memcell *m2){
	return m1->data.boolVal == m2->data.boolVal;
}
unsigned char table_eq (avm_memcell *m1, avm_memcell *m2){
	return 0;
}
unsigned char userfunc_eq (avm_memcell *m1, avm_memcell *m2){
	return m1->data.funcVal == m2->data.funcVal;
}
unsigned char libfunc_eq (avm_memcell *m1, avm_memcell *m2){
	if(strcmp(m1->data.libfuncVal, m2->data.libfuncVal)==0)
		return 1;
	else
		return 0;
}
unsigned char nil_eq (avm_memcell *m1, avm_memcell *m2){
	return 1;
}
unsigned char undef_eq (avm_memcell *m1, avm_memcell *m2){
	return 0;
}

eq_func_t eqFuncs[]={
	number_eq,
	string_eq,
	bool_eq,
	table_eq,
	userfunc_eq,
	libfunc_eq,
	nil_eq,
	undef_eq
};

unsigned char avm_eq (avm_memcell *m1, avm_memcell *m2){
	assert(m1->type >= 0 && m1->type < undef_m);
	assert(m2->type >= 0 && m2->type < undef_m);
	return (*eqFuncs[m1->type]) (m1, m2);
}


/* IMPLEMENTATION OF DISPATCHER 'LESS' */
typedef unsigned char (*less_func_t) (avm_memcell*, avm_memcell*);


unsigned char number_less (avm_memcell *m1, avm_memcell *m2){
	return m1->data.numVal < m2->data.numVal;
}
unsigned char string_less (avm_memcell *m1, avm_memcell *m2){
	if(strcmp(m1->data.strVal, m2->data.strVal)<0)
		return 1;
	else
		return 0;
}
unsigned char bool_less (avm_memcell *m1, avm_memcell *m2){
	return m1->data.boolVal == m2->data.boolVal;
}
unsigned char table_less (avm_memcell *m1, avm_memcell *m2){
	return 0;
}
unsigned char userfunc_less (avm_memcell *m1, avm_memcell *m2){
	return m1->data.funcVal < m2->data.funcVal;
}
unsigned char libfunc_less (avm_memcell *m1, avm_memcell *m2){
	if(strcmp(m1->data.libfuncVal, m2->data.libfuncVal)<0)
		return 1;
	else
		return 0;
}
unsigned char nil_less (avm_memcell *m1, avm_memcell *m2){
	return 1;
}
unsigned char undef_less (avm_memcell *m1, avm_memcell *m2){
	return 0;
}


less_func_t lessFuncs[]={
	number_less,
	string_less,
	bool_less,
	table_less,
	userfunc_less,
	libfunc_less,
	nil_less,
	undef_less
};

unsigned char avm_less (avm_memcell *m1, avm_memcell *m2){
	assert(m1->type >= 0 && m1->type < undef_m);
	assert(m2->type >= 0 && m2->type < undef_m);
	return (*lessFuncs[m1->type]) (m1, m2);
}



/* JUMP IF EQUAL */
void execute_jeq(instruction *instr){
	assert(instr->result.type == label_a);

	avm_memcell *rv1 = avm_translate_operand(&instr->arg1, &ax);
	avm_memcell *rv2 = avm_translate_operand(&instr->arg2, &bx);

	unsigned char result = 0;

	if(rv1->type == undef_m || rv2->type == undef_m){
		printf("error: 'undef' involved in equality!");
		executionFinised = 1;
	}
	else if(rv1->type == nil_m || rv2->type == nil_m)
		result = (rv1->type == nil_m && rv2->type == nil_m);
	else if(rv1->type == bool_m || rv2->type == bool_m)
		result = (avm_tobool(rv1) == avm_tobool(rv2));
	else if (rv1->type != rv2->type){
		printf("error: %s == %s is illegal!", typeStrings[rv1->type], typeStrings[rv2->type]);
		executionFinised = 1;
	}
	else{
		/*equality  check with dispatching */
		result = avm_eq(rv1, rv2);
	}

	if(!executionFinised && result){
		pc = instr->result.value;
	}
}

/* JUMP IF NOT EQUAL */
void execute_jne(instruction *instr){
	assert(instr->result.type == label_a);

	avm_memcell *rv1 = avm_translate_operand(&instr->arg1, &ax);
	avm_memcell *rv2 = avm_translate_operand(&instr->arg2, &bx);

	unsigned char result = 0;

	if(rv1->type == undef_m || rv2->type == undef_m){
		printf("error: 'undef' involved in equality!");
		executionFinised = 1;
	}
	else if(rv1->type == nil_m || rv2->type == nil_m)
		result = (rv1->type == nil_m && rv2->type == nil_m);
	else if(rv1->type == bool_m || rv2->type == bool_m)
		result = (avm_tobool(rv1) == avm_tobool(rv2));
	else if (rv1->type != rv2->type){
		printf("error: %s == %s is illegal!", typeStrings[rv1->type], typeStrings[rv2->type]);
		executionFinised = 1;
	}
	else{
		/*equality  check with dispatching */
		result = avm_eq(rv1, rv2);
	}
	/* call avm_eq and flip the result, to get 'avm_neq' */
	if(result==0)
		result= 1;
	else
		result= 0;

	if(!executionFinised && result){
		pc = instr->result.value;
	}
}

/* JUMP IF LESS EQUAL */
void execute_jle(instruction *instr){
	assert(instr->result.type == label_a);

	avm_memcell *rv1 = avm_translate_operand(&instr->arg1, &ax);
	avm_memcell *rv2 = avm_translate_operand(&instr->arg2, &bx);

	unsigned char result = 0;

	if(rv1->type == undef_m || rv2->type == undef_m){
		printf("error: 'undef' involved in equality!");
		executionFinised = 1;
	}
	else if(rv1->type == nil_m || rv2->type == nil_m)
		result = (rv1->type == nil_m && rv2->type == nil_m);
	else if(rv1->type == bool_m || rv2->type == bool_m)
		result = (avm_tobool(rv1) == avm_tobool(rv2));
	else if (rv1->type != rv2->type){
		printf("error: %s == %s is illegal!", typeStrings[rv1->type], typeStrings[rv2->type]);
		executionFinised = 1;
	}
	else{
		/*equality  check with dispatching */
		result = avm_eq(rv1, rv2) || avm_less(rv1, rv2);
	}

	if(!executionFinised && result){
		pc = instr->result.value;
	}
}

/* JUMP IF GREATER EQUAL */
void execute_jge(instruction *instr){
	assert(instr->result.type == label_a);

	avm_memcell *rv1 = avm_translate_operand(&instr->arg1, &ax);
	avm_memcell *rv2 = avm_translate_operand(&instr->arg2, &bx);

	unsigned char result = 0;

	if(rv1->type == undef_m || rv2->type == undef_m){
		printf("error: 'undef' involved in equality!");
		executionFinised = 1;
	}
	else if(rv1->type == nil_m || rv2->type == nil_m)
		result = (rv1->type == nil_m && rv2->type == nil_m);
	else if(rv1->type == bool_m || rv2->type == bool_m)
		result = (avm_tobool(rv1) == avm_tobool(rv2));
	else if (rv1->type != rv2->type){
		printf("error: %s == %s is illegal!", typeStrings[rv1->type], typeStrings[rv2->type]);
		executionFinised = 1;
	}
	else{
		/*equality  check with dispatching */
		/* call avm_less with switched arguments, to get 'avm_greater' */
		result = avm_eq(rv1, rv2) || avm_less(rv2, rv1);
	}

	if(!executionFinised && result){
		pc = instr->result.value;
	}
}

/* JUMP IF LESS */
void execute_jlt(instruction *instr){
	assert(instr->result.type == label_a);

	avm_memcell *rv1 = avm_translate_operand(&instr->arg1, &ax);
	avm_memcell *rv2 = avm_translate_operand(&instr->arg2, &bx);

	unsigned char result = 0;

	if(rv1->type == undef_m || rv2->type == undef_m){
		printf("error: 'undef' involved in equality!");
		executionFinised = 1;
	}
	else if(rv1->type == nil_m || rv2->type == nil_m)
		result = (rv1->type == nil_m && rv2->type == nil_m);
	else if(rv1->type == bool_m || rv2->type == bool_m)
		result = (avm_tobool(rv1) == avm_tobool(rv2));
	else if (rv1->type != rv2->type){
		printf("error: %s == %s is illegal!", typeStrings[rv1->type], typeStrings[rv2->type]);
		executionFinised = 1;
	}
	else{
		/*equality  check with dispatching */
		result = avm_less(rv1, rv2);
	}

	if(!executionFinised && result){
		pc = instr->result.value;
	}
}

/* JUMP IF GREATER */
void execute_jgt(instruction *instr){
	assert(instr->result.type == label_a);

	avm_memcell *rv1 = avm_translate_operand(&instr->arg1, &ax);
	avm_memcell *rv2 = avm_translate_operand(&instr->arg2, &bx);

	unsigned char result = 0;

	if(rv1->type == undef_m || rv2->type == undef_m){
		printf("error: 'undef' involved in equality!");
		executionFinised = 1;
	}
	else if(rv1->type == nil_m || rv2->type == nil_m)
		result = (rv1->type == nil_m && rv2->type == nil_m);
	else if(rv1->type == bool_m || rv2->type == bool_m)
		result = (avm_tobool(rv1) == avm_tobool(rv2));
	else if (rv1->type != rv2->type){
		printf("error: %s == %s is illegal!", typeStrings[rv1->type], typeStrings[rv2->type]);
		executionFinised = 1;
	}
	else{
		/*equality  check with dispatching */
		/* call avm_less with switched arguments, to get 'avm_greater' */
		result = avm_less(rv2, rv1);
	}

	if(!executionFinised && result){
		pc = instr->result.value;
	}
}

/* JUMP */
void execute_jump(instruction *instr){
	assert(instr->result.type == label_a);

	pc = instr->result.value;
}

/* NO OPERATION */
void execute_nop (instruction* i){}


void execute_newtable(instruction *instr){
	avm_memcell *lv = avm_translate_operand(&instr->result, (avm_memcell*) 0);
	assert(lv && (&stack[AVM_STACKSIZE-1] >= lv && lv > &stack[top] || lv==&retval));

	avm_memcellclear(lv);

	lv->type = table_m;

	lv->data.tableVal = avm_tablenew();
	
	avm_tableincrefcounter(lv->data.tableVal);
}


void execute_tablegetelem ( instruction *instr){
	avm_memcell *lv = avm_translate_operand(&instr->arg1, (avm_memcell*) 0);
	avm_memcell *t = avm_translate_operand(&instr->arg2, (avm_memcell*) 0);
	avm_memcell *i = avm_translate_operand(&instr->result, &ax);

	assert(lv && (&stack[AVM_STACKSIZE-1] >= lv && lv > &stack[top] || lv==&retval));
	assert(t && &stack[AVM_STACKSIZE-1] >= t && t > &stack[top] );
	assert(i);

	avm_memcellclear(lv);
	lv->type = nil_m;

	if(t->type != table_m){
		printf("error: illegal use of type %s as table!",typeStrings[t->type]);
		executionFinised = 1;
	}
	else{
		avm_memcell *content = avm_tablegetelem(t->data.tableVal, i);
		if(content)
			avm_assign(lv, content);
		else{
			char *ts = avm_tostring(t);
			char *is = avm_tostring(i);
			printf("warning: %s[%s] not found!\n", ts, is); 
			free(ts);
			free(is);
		}
		
	}

}

void execute_tablesetelem (instruction *instr){
	avm_memcell *t = avm_translate_operand(&instr->arg1, (avm_memcell*) 0);
	avm_memcell *i = avm_translate_operand(&instr->arg2, &ax);
	avm_memcell *c = avm_translate_operand(&instr->result, &bx);

	assert(t && &stack[AVM_STACKSIZE-1] >= t && t > &stack[top] );
	assert(i && c );

	if(t->type != table_m){
		printf("error: illegal use of type %s as table!", typeStrings[t->type]);
		executionFinised = 1;
	}
	else{
		avm_tablesetelem(t->data.tableVal, i, c);
	}

}



//--------LIBRARY FUNCTIONS----------------------------------

/*implementation of the library function 'print'.
	it displays every argument at the console */

void libfunc_print (void){
	//printf("priiiint\n");
	unsigned n = avm_totalactuals();
	unsigned i;
	for (i=0; i<n; ++i){
		char *s = avm_tostring(avm_getactual(i));
		puts(s);
		free(s);
	}
	//printf("priiiint\n");
}

void libfunc_typeof (void){
	unsigned n = avm_totalactuals();

	if( n != 1){
		printf("error: one argument (not %d) expected in 'typeof'!", n);
		executionFinised = 1;
	}
	else{
		/*thats how a library function return a result. 
		It was to only set the 'retval' register */
		avm_memcellclear(&retval);
		retval.type = string_m;
		retval.data.strVal = strdup( typeStrings[avm_getactual(0)->type]);
	}
}

void libfunc_totalarguments (){
	/* get topsp of previous activation record */
	unsigned p_topsp = avm_get_envvalue(topsp + AVM_SAVEDTOPSP_OFFSET);
	avm_memcellclear(&retval);

	if( !p_topsp ){
		// o panmegistos pansofos AS to exei san error. 8eoroume, oi potapoi douloi
		// tou oti einai warning kai oti mallon o panagiotatos programmatistaras ekane
		// typo, suggnomh
		printf("warning: 'totalarguments' called outside a function!");
		//executionFinised = 1;
		retval.type = nil_m;
	}else{
		/*extract the number of actual arguments for
		previous activation record */
		retval.type = number_m;
		retval.data.numVal = avm_get_envvalue(p_topsp + AVM_NUMACTUALS_OFFSET);
	}
}

void libfunc_input (void){
	avm_memcellclear(&retval);

	char *input = malloc(1000);
	double flot;
	scanf("%s", input); // exei null
	int dots=0;
	int i=0;
	int str=0;
	int numresult;
	char c = input[i];

	while(c!='\0'){
		if ((c>=48 && c<=57)){ //sunexizei na vriksei arithmous
			i++;
			c=input[i];
		}
		else if(c==46){
			dots++;
			if(dots>1){
				str=1;
				break;
			}
			i++;
			c=input[i];
		}
		else{
			str=1;
			break;
		}
	}

	if(str==1){
		if(strcmp(input, "true")==0){
			retval.type = bool_m;
			retval.data.boolVal = 1;
		}
		else if(strcmp(input, "false")==0){
			retval.type = bool_m;
			retval.data.boolVal = 0;
		}
		else if(strcmp(input, "nil")==0)
			retval.type = nil_m;
		else{
			retval.type = string_m;
			retval.data.strVal = strdup(input);
		}

	}
	else{
		if(dots==0){
			numresult = atoi(input);
			retval.type = number_m;
			retval.data.numVal = numresult;
		}
		else{
			flot = atof(input);
			retval.type = number_m;
			retval.data.numVal = flot;
		}
	}


}

void libfunc_objectmemberkeys (void){

}

void libfunc_objectcopy (void){
	assert(avm_getactual(0)->type != table_m);


	avm_memcellclear(&retval);

	avm_table *new = avm_tablenew();
	avm_table *this_table= avm_getactual(0)->data.tableVal;

	char* bigBuffer=(char*)malloc(4096);
	avm_table_bucket* b;
	int i;
	/* strIndexed[] */
	for(i=0; i<211; i++){
		b= this_table->strIndexed[i];
		while(b!=NULL){
			avm_tablesetelem(new,b->key,b->value);				
			b=b->next;
		}
	}
	/* numIndexed[] */
	for(i=0; i<211; i++){
		b= this_table->strIndexed[i];
		while(b!=NULL){
			avm_tablesetelem(new,b->key,b->value);	
			b=b->next;
		}
	}
	/* boolIndexed[] */
	for(i=0; i<211; i++){
		b= this_table->strIndexed[i];
		while(b!=NULL){
			avm_tablesetelem(new,b->key,b->value);	
			b=b->next;
		}
	}
	/* libIndexed[] */
	for(i=0; i<211; i++){
		b= this_table->strIndexed[i];
		while(b!=NULL){
			avm_tablesetelem(new,b->key,b->value);	
			b=b->next;
		}
	}
	/* userIndexed[]*/
	for(i=0; i<211; i++){
		b= this_table->strIndexed[i];
		while(b!=NULL){
			avm_tablesetelem(new,b->key,b->value);
			b=b->next;
		}
	}
	retval.type = table_m;
	retval.data.tableVal = new;
}


void libfunc_objecttotalmembers (void){
	avm_memcellclear(&retval);

	int numresult=0;
	avm_memcell* t = avm_getactual(0);

	numresult+= t->data.tableVal->totalnum;
	numresult+= t->data.tableVal->totalstr;
	numresult+= t->data.tableVal->totalbool;
	numresult+= t->data.tableVal->totallib;
	numresult+= t->data.tableVal->totaluser;

	retval.type = number_m;
	retval.data.numVal = numresult;
}


void libfunc_argument (void){
	unsigned p_topsp = avm_get_envvalue(topsp + AVM_SAVEDTOPSP_OFFSET);
	avm_memcellclear(&retval);

	if( !p_topsp ){
		printf("warning: 'totalarguments' called outside a function!");
		//executionFinised = 1;
		retval.type = nil_m;
	}else{
		//avm_memcell *tmp = stack[p_topsp + AVM_NUMACTUALS_OFFSET + avm_getactual(0)->data.numVal +1];
		assert(avm_totalactuals() != 0);
		switch(avm_getactual(0)->type){
			case number_m:
				retval.type = number_m;
				retval.data.numVal = avm_getactual(0)->data.numVal; 
				break;
			case string_m:
				retval.type = string_m;
				retval.data.strVal = strdup( avm_getactual(0)->data.strVal );
				break;
			case bool_m:
				retval.type = bool_m;
				retval.data.boolVal = avm_getactual(0)->data.boolVal ;
				break;
			case table_m:
				retval.type = table_m;
				retval.data.tableVal = avm_getactual(0)->data.tableVal ;
				break;
			case userfunc_m:
				retval.type = userfunc_m;
				retval.data.funcVal = avm_getactual(0)->data.funcVal ;
				break;
			case libfunc_m:
				retval.type = libfunc_m;
				retval.data.libfuncVal = strdup( avm_getactual(0)->data.libfuncVal );
				break;
		}
	}

}

void libfunc_strtonum (void){

	double res;
	avm_memcellclear(&retval);
	avm_memcell* t = avm_getactual(0);


	if(t->type==string_m){
		if(strcmp(t->data.strVal, "0")==0){
			retval.type=number_m;
			retval.data.numVal=0;
		}
		else{
			res=atof(t->data.strVal);
			if(res==0){
				retval.type=nil_m;
			}
			else{
				retval.data.numVal=res;
				retval.type=number_m;
			}
			retval.type=number_m;
		}
	}
	else
		retval.type=nil_m;
}

void libfunc_sqrt (void){

	avm_memcellclear(&retval);
	avm_memcell* t = avm_getactual(0);
	double res;

	assert(t->type==number_m);
	res = sqrt(t->data.numVal);

	retval.type=number_m;
	retval.data.numVal=res;
}

void libfunc_cos (void){

	avm_memcellclear(&retval);
	avm_memcell* t = avm_getactual(0);
	double res;

	assert(t->type==number_m);
	res=cos(t->data.numVal);

	retval.type=number_m;
	retval.data.numVal=res;
}

void libfunc_sin (void){

	avm_memcellclear(&retval);
	avm_memcell* t = avm_getactual(0);
	double res;

	assert(t->type==number_m);
	res=sin(t->data.numVal);

	retval.type=number_m;
	retval.data.numVal=res;
}


void avm_initialize (){
	avm_initstack();

	// avm_registerlibfunc("print", libfunc_print);
	// avm_registerlibfunc("typeof", libfunc_typeof);
	// avm_registerlibfunc("totalarguments", libfunc_totalarguments);
	// avm_registerlibfunc("input", libfunc_input);
	// avm_registerlibfunc("objectmemberkeys", libfunc_objectmemberkeys);
	// avm_registerlibfunc("objecttotalmembers", libfunc_objecttotalmembers);
	// avm_registerlibfunc("objectcopy", libfunc_objectcopy);
	// avm_registerlibfunc("argument", libfunc_argument);
	// avm_registerlibfunc("strtonum", libfunc_strtonum);
	// avm_registerlibfunc("sqrt", libfunc_sqrt);
	// avm_registerlibfunc("cos", libfunc_cos);
	// avm_registerlibfunc("sin", libfunc_sin);

	/* for all lib funcs */
}

//--------END OF LIBRARY FUNCTIONS----------------------------------




void memclear_table (avm_memcell* m){
	assert(m->data.tableVal);
	avm_tabledecrefcounter(m->data.tableVal);
}

void memclear_string(avm_memcell* m){
	assert(m->data.strVal);
	free(m->data.strVal);
}


lista_userfunc* avm_getfuncinfo (unsigned address){
	unsigned i=0;

	while(i<usr_no){
		if(usr_consts[i].address==address)
			return &usr_consts[i];
		i++;
	}
	assert(0);
}

library_func_t libfunc_map[]={
	libfunc_print,
	libfunc_typeof,
	libfunc_totalarguments,
	libfunc_input,
	libfunc_objectmemberkeys,
	libfunc_objecttotalmembers,
	libfunc_objectcopy,
	libfunc_argument,
	libfunc_strtonum,
	libfunc_sqrt,
	libfunc_cos,
	libfunc_sin
};

// unsigned hash_libfunc(char *name){
// 	return atoi(name)%12;
// }

// void avm_registerlibfunc (char *id, library_func_t addr){
//  	libfunc_map[hash_libfunc(id)] = addr;	
//  }

library_func_t avm_getlibraryfunc( char *id){

	if(strcmp(id,"print")==0)
		return libfunc_map[0];
	else if(strcmp(id,"typeof")==0)
		return libfunc_map[1];
	else if(strcmp(id,"totalarguments")==0)
		return libfunc_map[2];
	else if(strcmp(id,"input")==0)
		return libfunc_map[3];
	else if(strcmp(id,"objectmemberkeys")==0)
		return libfunc_map[4];
	else if(strcmp(id,"objecttotalmembers")==0)
		return libfunc_map[5];
	else if(strcmp(id,"objectcopy")==0)
		return libfunc_map[6];
	else if(strcmp(id,"argument")==0)
		return libfunc_map[7];
	else if(strcmp(id,"strtonum")==0)
		return libfunc_map[8];
	else if(strcmp(id,"sqrt")==0)
		return libfunc_map[9];
	else if(strcmp(id,"cos")==0)
		return libfunc_map[10];
	else 
		return libfunc_map[11];

}




void execute_uminus (instruction* i){} //we do not implment this
void execute_and (instruction* i){} //we do not implment this
void execute_or (instruction* i){} //we do not implment this
void execute_not (instruction* i){} //we do not implment this

char* number_tostring (avm_memcell* c){
	char* bigBuffer=(char*)malloc(512);
	sprintf(bigBuffer, "%0.3f", c->data.numVal);

	return bigBuffer;
}
char* string_tostring (avm_memcell* c){
	char* bigBuffer=(char*)malloc(1024);
	sprintf(bigBuffer, "%s", c->data.strVal);
	return bigBuffer;
}
char* bool_tostring (avm_memcell* c){
	char* bigBuffer=(char*)malloc(100);
	if(c->data.boolVal)
		sprintf(bigBuffer,"true");
	else
		sprintf(bigBuffer,"false");
	return bigBuffer;
}
char* table_tostring (avm_memcell* c){

	char* bigBuffer=malloc(400);
	avm_table_bucket* b;
	int i;
	sprintf(bigBuffer, "[");
	/* strIndexed[] */
	for(i=0; i<211; i++){

		b= c->data.tableVal->strIndexed[i];
		while(b!=NULL){

			switch (b->value->type){
				case number_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%s:%f] ",b->key->data.strVal, b->value->data.numVal );
					break;
				case string_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%s:%s] ",b->key->data.strVal, b->value->data.strVal );
					break;

				case bool_m:
					if(b->value->data.boolVal)
						sprintf(bigBuffer+strlen(bigBuffer), "[%s:true] ",b->key->data.strVal);
					else
						sprintf(bigBuffer+strlen(bigBuffer), "[%s:false] ",b->key->data.strVal);
					break;

				case table_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%s:%s] ",b->key->data.strVal, table_tostring(b->value) );
					break;

				case userfunc_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%s:%u] ",b->key->data.strVal, b->value->data.funcVal );
					break;

				case libfunc_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%s:%s] ",b->key->data.strVal, b->value->data.libfuncVal );
					break;

				case nil_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%s:nil] ",b->key->data.strVal);
					break;

				case undef_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%s:undef] ",b->key->data.strVal);
					break;
			}
			b=b->next;
		}
	}
	/* numIndexed[] */
	for(i=0; i<211; i++){
		b= c->data.tableVal->numIndexed[i];
		//b->key->data.numVal = 56;
		while(b!=NULL){
			switch (b->value->type){
				case number_m:
					sprintf(bigBuffer+strlen(bigBuffer), "{%f:%f} ",b->key->data.numVal, b->value->data.numVal );
					break;
				case string_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%f:%s] ",b->key->data.numVal, b->value->data.strVal );
					break;
				case bool_m:
					if(b->value->data.boolVal)
						sprintf(bigBuffer+strlen(bigBuffer), "[%f:true] ",b->key->data.numVal);
					else
						sprintf(bigBuffer+strlen(bigBuffer), "[%f:false] ",b->key->data.numVal);
					break;
				case table_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%f:%s] ",b->key->data.numVal, table_tostring(b->value) );
					break;
				case userfunc_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%f:%u] ",b->key->data.numVal, b->value->data.funcVal );
					break;
				case libfunc_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%f:%s] ",b->key->data.numVal, b->value->data.libfuncVal );
					break;
				case nil_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%f:nil] ",b->key->data.numVal);
					break;
				case undef_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%f:undef] ",b->key->data.numVal);
					break;
			}
			b=b->next;
		}
	}
	/* boolIndexed[] */
	for(i=0; i<211; i++){
		b= c->data.tableVal->boolIndexed[i];
		while(b!=NULL){
			switch (b->value->type){
				case number_m:
					if(b->key->data.boolVal)
						sprintf(bigBuffer+strlen(bigBuffer), "[true:%f] ",b->value->data.numVal);
					else
						sprintf(bigBuffer+strlen(bigBuffer), "[false:%f] ",b->value->data.numVal);
					break;
				case string_m:
					if(b->key->data.boolVal)
						sprintf(bigBuffer+strlen(bigBuffer), "[true:%s] ",b->value->data.strVal);
					else
						sprintf(bigBuffer+strlen(bigBuffer), "[false:%s] ",b->value->data.strVal);
					break;
				case bool_m:
					if(b->key->data.boolVal){
						if(b->value->data.boolVal)
							sprintf(bigBuffer+strlen(bigBuffer), "[true:true] ");
						else
							sprintf(bigBuffer+strlen(bigBuffer), "[true:false] ");
					}
					else{
						if(b->value->data.boolVal)
							sprintf(bigBuffer+strlen(bigBuffer), "[false:true] ");
						else
							sprintf(bigBuffer+strlen(bigBuffer), "[false:false] ");
					}
					break;
				case table_m:
					if(b->key->data.boolVal)
						sprintf(bigBuffer+strlen(bigBuffer), "[true:%s] ", table_tostring(b->value) );
					else
						sprintf(bigBuffer+strlen(bigBuffer), "[false:%s] ", table_tostring(b->value) );
					break;
				case userfunc_m:
					if(b->key->data.boolVal)
						sprintf(bigBuffer+strlen(bigBuffer), "[true:%u] ",b->value->data.funcVal);
					else
						sprintf(bigBuffer+strlen(bigBuffer), "[false:%u] ",b->value->data.funcVal);
					break;
				case libfunc_m:
					if(b->key->data.boolVal)
						sprintf(bigBuffer+strlen(bigBuffer), "[true:%s] ",b->value->data.libfuncVal);
					else
						sprintf(bigBuffer+strlen(bigBuffer), "[false:%s] ",b->value->data.libfuncVal);
					break;
				case nil_m:
					if(b->key->data.boolVal)
						sprintf(bigBuffer+strlen(bigBuffer), "[true:nil] ");
					else
						sprintf(bigBuffer+strlen(bigBuffer), "[false:nil] ");
					break;
				case undef_m:
					if(b->key->data.boolVal)
						sprintf(bigBuffer+strlen(bigBuffer), "[true:undef] ");
					else
						sprintf(bigBuffer+strlen(bigBuffer), "[false:undef] ");
					break;
			}
			b=b->next;
		}
	}
	/* libIndexed[] */
	for(i=0; i<211; i++){
		b= c->data.tableVal->libIndexed[i];
		while(b!=NULL){
			switch (b->value->type){
				case number_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%s:%f] ",b->key->data.libfuncVal, b->value->data.numVal );
					break;
				case string_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%s:%s] ",b->key->data.libfuncVal, b->value->data.strVal );
					break;
				case bool_m:
					if(b->value->data.boolVal)
						sprintf(bigBuffer+strlen(bigBuffer), "[%s:true] ",b->key->data.libfuncVal);
					else
						sprintf(bigBuffer+strlen(bigBuffer), "[%s:false] ",b->key->data.libfuncVal);
					break;
				case table_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%s:%s] ",b->key->data.libfuncVal, table_tostring(b->value) );
					break;
				case userfunc_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%s:%u] ",b->key->data.libfuncVal, b->value->data.funcVal );
					break;
				case libfunc_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%s:%s] ",b->key->data.libfuncVal, b->value->data.libfuncVal );
					break;
				case nil_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%s:nil] ",b->key->data.libfuncVal);
					break;
				case undef_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%s:undef] ",b->key->data.libfuncVal);
					break;
			}
			b=b->next;
		}
	}
	/* userIndexed[]*/
	for(i=0; i<211; i++){
		b= c->data.tableVal->userIndexed[i];
		while(b!=NULL){
			switch (b->value->type){
				case number_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%u:%f] ",b->key->data.funcVal, b->value->data.numVal );
					break;
				case string_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%u:%s] ",b->key->data.funcVal, b->value->data.strVal );
					break;
				case bool_m:
					if(b->value->data.boolVal)
						sprintf(bigBuffer+strlen(bigBuffer), "[%u:true] ",b->key->data.funcVal);
					else
						sprintf(bigBuffer+strlen(bigBuffer), "[%u:false] ",b->key->data.funcVal);
					break;
				case table_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%u:%s] ",b->key->data.funcVal, table_tostring(b->value) );
					break;
				case userfunc_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%u:%u] ",b->key->data.funcVal, b->value->data.funcVal );
					break;
				case libfunc_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%u:%s] ",b->key->data.funcVal, b->value->data.libfuncVal );
					break;
				case nil_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%u:nil] ",b->key->data.funcVal);
					break;
				case undef_m:
					sprintf(bigBuffer+strlen(bigBuffer), "[%u:undef] ",b->key->data.funcVal);
					break;
			}
			b=b->next;

		}
	}
	sprintf(bigBuffer+strlen(bigBuffer), "]");
	return bigBuffer;
}
char* userfunc_tostring (avm_memcell* c){
	char* bigBuffer=(char*)malloc(256);
	sprintf(bigBuffer, "user function %u", c->data.funcVal);
	return bigBuffer;
}
char* libfunc_tostring (avm_memcell* c){
	char* bigBuffer=(char*)malloc(256);
	sprintf(bigBuffer, "library function %s", c->data.libfuncVal);
	return bigBuffer;
}
char* nil_tostring (avm_memcell* c){
	char* bigBuffer=(char*)malloc(100);
	sprintf(bigBuffer, "nil");
	return bigBuffer;
}
char* undef_tostring(avm_memcell* c){
	char* bigBuffer=(char*)malloc(100);
	sprintf(bigBuffer, "undefined");
	return bigBuffer;
}


//--------------------------------------------------

void print_num_table(){
	printf("CONSTS:\n");
	printf("%u\n", num_no);
	unsigned i;

	if(num_no>0){
		for(i=0; i<num_no; i++){
			printf("%f\n", num_consts[i].d);
		}
	}
}

void print_string_table(){
	printf("STRINGS:\n");
	printf("%u\n", str_no);
	unsigned i;

	if(str_no>0){
		for(i=0; i<str_no; i++){
			printf("%u %s\n", (unsigned)strlen(str_consts[i].str), str_consts[i].str);
		}
	}
}

void print_libfunc_table(){
	printf("LIBRARY FUNCS:\n");
	printf("%u\n", lib_no);
	unsigned i;

	if(lib_no>0){
		for(i=0; i<lib_no; i++){
			printf("%s\n", lib_consts[i].str);
		}
	}
}

void print_userfunc_table(){
	printf("USER FUNCS:\n");
	printf("%u\n", usr_no);
	unsigned i;

	if(usr_no>0){
		for(i=0; i<usr_no; i++){
			printf("%u %u %s \n", usr_consts[i].address, usr_consts[i].localsize, usr_consts[i].id);
		}
	}
}


void print_vmarg(vmarg* arg){
	if(arg!=NULL)
		printf("%d:%u ", arg->type, arg->value);
}


void print_code(){
	printf("INSTRUCTIONS:\n");
	printf("%u\n", code_no);
	unsigned i;

	if(code_no>0){
		for(i=0; i<code_no; i++){
			printf("%u ", code[i].opcode);
			print_vmarg(&code[i].arg1);
			print_vmarg(&code[i].arg2);
			print_vmarg(&code[i].result);
			printf("\n");
		}
	}
}


void print_tables(){
	print_string_table();
	print_num_table();
	print_userfunc_table();
	print_libfunc_table();
}


void print_magicNum(){
	printf("%d\n",magicnum);
}


void print_globals_size(){
	printf("%d\n",globals_size);
}


void print_input_binary(){
	print_magicNum();
	print_globals_size();
	print_tables();
	print_code();
}


int read_binary(){
	FILE *fd;
	unsigned i, size;

	fd=fopen(BINARY_FILE,"rb");
	if (!fd){
		fprintf(stderr, "Unable to open file '%s'!", BINARY_FILE);
		return 1;
	}

	// magic number
	fread(&magicnum,sizeof(unsigned),1,fd);

	// number of globals
	fread(&globals_size,sizeof(unsigned),1,fd);

	// constant strings
	fread(&str_no,sizeof(unsigned),1,fd);

	if(str_no==0)
		str_consts= NULL;
	else{
		str_consts= malloc(sizeof(lista_string) * str_no);
		
		for(i=0; i<str_no; i++){
			fread(&size,sizeof(unsigned),1,fd);
			str_consts[i].str= malloc(size);

			fread(str_consts[i].str,sizeof(char),size,fd);
		}
	}

	// constant numbers
	fread(&num_no,sizeof(unsigned),1,fd);

	if(num_no==0)
		num_consts= NULL;
	else{
		num_consts= malloc(sizeof(lista_numbers) * num_no);
		
		for(i=0; i<num_no; i++){
			fread(&num_consts[i].d,sizeof(double),1,fd);
		}
	}

	// user functions	
	fread(&usr_no,sizeof(unsigned),1,fd);

	if(usr_no==0)
		usr_consts= NULL;
	else{
		usr_consts= malloc(sizeof(lista_userfunc) * usr_no);
		
		for(i=0; i<usr_no; i++){
			fread(&usr_consts[i].address,sizeof(unsigned),1,fd);
			fread(&usr_consts[i].localsize,sizeof(unsigned),1,fd);

			usr_consts[i].address--;

			fread(&size,sizeof(unsigned),1,fd);
			usr_consts[i].id= malloc(size);

			fread(usr_consts[i].id,sizeof(char),size,fd);
		}
	}
	
	// library functions
	fread(&lib_no,sizeof(unsigned),1,fd);

	if(lib_no==0)
		lib_consts= NULL;
	else{
		lib_consts= malloc(sizeof(lista_libfunc) * lib_no);
		
		for(i=0; i<lib_no; i++){
			fread(&size,sizeof(unsigned),1,fd);
			lib_consts[i].str= malloc(size);

			fread(lib_consts[i].str,sizeof(char),size,fd);
		}
	}

	// code
	fread(&code_no,sizeof(unsigned),1,fd);

	if(code_no==0)
		code= NULL;
	else{
		code= malloc(sizeof(instruction) * code_no);
		
		for(i=0; i<code_no; i++){
			fread(&code[i].opcode,sizeof(unsigned char),1,fd);

			fread(&code[i].arg1.type,sizeof(unsigned char),1,fd);
			fread(&code[i].arg1.value,sizeof(unsigned),1,fd);

			fread(&code[i].arg2.type,sizeof(unsigned char),1,fd);
			fread(&code[i].arg2.value,sizeof(unsigned),1,fd);

			fread(&code[i].result.type,sizeof(unsigned char),1,fd);
			fread(&code[i].result.value,sizeof(unsigned),1,fd);
		}
	}

	tune_labels();

	AVM_ENDING_PC = code_no;

	fclose(fd);
	return 0;
}


void tune_labels(){
	unsigned i;

	for(i=0; i<code_no; i++){
		if(code[i].arg1.type==label_a)
			code[i].arg1.value--;

		if(code[i].arg2.type==label_a)
			code[i].arg2.value--;

		if(code[i].result.type==label_a)
			code[i].result.value--;
	}
}


void skip_function_definition(){
	if(called_function){
		called_function=0;
		return;
	}

	int my_pc= pc;
	unsigned func_def_count=0;

	if(code[my_pc].opcode==funcenter_v){
		while(my_pc<AVM_ENDING_PC){
			if(code[my_pc].opcode==funcenter_v){
				func_def_count++;
			}

			if(code[my_pc].opcode==funcexit_v){
				func_def_count--;
			}

			if(func_def_count==0){
				pc= my_pc+1;
				if(code[my_pc+1].opcode == funcenter_v){
					skip_function_definition();
				}
				return;
			}

			my_pc++;
		}
	}
}



void init_vars(){
	executionFinised = 0;
	pc = 0;
	currLine = 0;
	codeSize = 0;
	totalActuals = 0;
	top = AVM_STACKSIZE -1;
	called_function= 0;
}



int main(){

	init_vars();

	read_binary();
	print_input_binary();
	printf("\nTranceferring to execution...\n\n");
	top -= globals_size;

	avm_initialize();

	execute_cycle();


	return 0;
}
