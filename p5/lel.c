#include "lel.h"

bc_stack* BCHEAD=NULL;

void push(bc_stack* head, unsigned label){
	bc_stack* tmp;
	tmp=malloc(sizeof(bc_stack));
	if(tmp == NULL)
        exit(0);
    if(head==NULL){
    	tmp->nextbc=NULL;
    	tmp->prevLoop=NULL;
    	tmp->label=label;
    	head=tmp;
    	printf("%d\n", head->label);
    }	
    else{
    	tmp->nextbc=NULL;
    	tmp->prevLoop=head;
    	tmp->label=label;
    	head=tmp;

    }
}

void pop(bc_stack* head){
	if(head!=NULL)
		head = head->prevLoop;
	else
		printf("NO LOOPS\n");
}

void add(bc_stack* head, unsigned data){
	bc_stack* tmp =malloc(sizeof(bc_stack));
	tmp->nextbc=head->nextbc;
	tmp->label = data;
	head = tmp;
}

bc_stack* top(bc_stack* head){
	return head;
}

unsigned top_label(bc_stack* head){
	if(head!=NULL)
		return head->label;
	return 0;
}

void printBC(bc_stack* head){
	bc_stack* tmpHead = head;
	bc_stack* curr;
	while(tmpHead!=NULL){
		curr=tmpHead;
		while(curr!=NULL){
			printf("%d ", curr->label );
			curr=curr->nextbc;
		}
		printf("\n");
	}
	tmpHead=tmpHead->prevLoop;
}