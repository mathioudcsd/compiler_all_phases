#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "symtable.h"
#include "function_stack.h"

// unsigned quadNo =0;
// unsigned	total = 0;
// unsigned int 	currQuad = 0;

#define EXPAND_SIZE 1024
#define	CURR_SIZE (total * sizeof(quad))
#define	NEW_SIZE (EXPAND_SIZE * sizeof(quad)+CURR_SIZE)

/* ENUMS */
typedef enum expr_e{
	var_e,
	tableitem_e,

	programfunc_e,
	libraryfunc_e,

	arithexpr_e,
	boolexpr_e,
	assignexpr_e,
	newtable_e,

	constnum_e,
	constbool_e,
	conststring_e,

	nil_e
}expr_t;

typedef enum iopcode_e{
	assign, /* 0 */
	add,	/* 1 */
	sub,
	mul,
	Div,
	mod,	/* 5 */
	uminus,
	and,	/* 7 */
	or,
	not,	/* 9 */
	if_eq,	/* 10 */
	if_noteq, /* 11 */
	if_lesseq,
	if_greatereq,
	if_less,
	if_greater, /* 15 */
	call,	/* 16 */
	param,  /* 17 */
	ret,	/* 18 */
	getretval, /* 19 */
	funcstart, /* 20 */
	funcend, /* 21 */	
	tablecreate, /* 22 */
	tablegetelement, /* 23 */
	tablesetelement, /* 24 */
	jump /* 25 */
}iopcode;


/* STRUCTS */
/*
typedef struct symbol_s {
	symbol_t	type;
	char*		name;
	scopespace_t	space;
	unsigned	offset;
	unsigned	scope;
	unsigned 	line;
}symbol;
*/


typedef struct expr_s{
	expr_t	type;
	SymbolTableEntry	*sym;
	struct expr_s *index;
	double	numConst;
	char	*strConst;
	unsigned char	boolConst;
	struct expr_s *next;
}expr;

typedef struct param_expr_s{
	expr* exp;
	struct param_expr_s *left;
	struct param_expr_s *right;
}param_expr;

typedef struct indexed_expr_s{
	expr* exp1;
	expr* exp2;
	struct indexed_expr_s *next;
}indexed_expr;


typedef struct quad_s{
	iopcode	op;
	expr	*result;
	expr	*arg1;
	expr	*arg2;
	unsigned	label;
	unsigned	line;
	unsigned	jLabel;
	struct quad_s *next;
	unsigned taddress;
}quad;



/*	this struct is used in rule FORSTMT to move around 2 labels	*/
struct label_carrier{
	unsigned test;
	unsigned enter;
};


quad* quadHEAD;
// quad* quads = (quad*) 0;
param_expr* elist_paramets;
indexed_expr *indexed_paramets ;


/* FUNCTIONS */
void emit(iopcode op, expr*	arg1, expr*	arg2,expr* result, unsigned	label, unsigned	line);

void expand(void);

expr* newexpr(expr_t t);

expr* newexpr_conststring(char* s);

expr* newexpr_constnum(double i);

expr* newexpr_constbool(unsigned char c);

expr* newexpr_constnil();

void myEmit(iopcode op, expr* arg1, expr* arg2, expr* result, unsigned	jlabel, unsigned line);

quad* findQuad(unsigned label);

unsigned getQuadNo();

unsigned nextQuadNo();

unsigned incrQuadNo();

expr* emitArithExpr(iopcode op, expr* arg1, expr* arg2, int scope, int line);

expr* emitBoolExpr(iopcode op, expr* arg1, expr* arg2, int scope, int line, unsigned char c);

expr* emitLogicOp(iopcode op, expr* arg1, expr* arg2, int scope, int line, unsigned jlabel);

unsigned emitIf(expr* arg, int line);

void emitJump(unsigned jumpLabel, int line);

expr* checkBoolExp(expr* exp);

void patchlabel(unsigned quad_num, unsigned label);

struct f_stack* patchlabel_bc(struct f_stack* list, unsigned label, int loopnum);

void print_helper(expr* exp);

void printQuads();

expr* emit_ifTableItem(expr *exp, int scope, int line);

expr* member_item(expr *lvalue, char *name , int scope, int line);

expr* emitCall(expr* fun_name, param_expr* param_list, int scope, int line);

void insert_param(expr* this);

void free_param(void);

void insert_indexed(expr* exp1, expr* exp2);

void free_indexed();
