#define AVM_TABLE_HASHSIZE 211
#define AVM_STACKSIZE 4096

#include <time.h>
#include <math.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <math.h>

#include <stdarg.h>
#define AVM_WIPEOUT(m) memset(&(m), 0, sizeof(m))

#define BINARY_FILE "target.abc"
typedef enum vmarg_t{
	label_a	=0,
	global_a =1,
	formal_a =2,
	local_a =3,
	number_a =4,
	string_a =5,
	bool_a =6,
	nil_a =7,
	userfunc_a =8,
	libfunc_a =9,
	retval_a =10,
	unused_a = 86
}vmarg_t;

typedef enum vmopcode{
	assign_v, /*0*/
	add_v, /*1*/
	sub_v, /*2*/
	mul_v, /*3*/ 
	div_v, /*4*/
	mod_v, /*5*/
	uminus_v, /*6*/
	and_v, /*7*/
	or_v, /*8*/
	not_v, /*9*/
	jeq_v, /*10*/
	jne_v, /*11*/
	jle_v, /*12*/
	jge_v, /*13*/
	jlt_v, /*14*/
	jgt_v, /*15*/
	call_v, /*16*/
	pusharg_v, /*17*/
	funcenter_v, /*18*/
	funcexit_v, /*19*/
	newtable_v, /*20*/
	tablegetelem_v, /*21*/
	tablesetelem_v, /*22*/
	nop_v, /*23*/
	jump_v /*24*/
}vmopcode;

typedef struct vmarg{
	unsigned char type;
	unsigned value;
}vmarg;

typedef struct instruction {
	unsigned char opcode;
	vmarg arg1;
	vmarg arg2;
	vmarg result;
	//unsigned srcLine;
}instruction;

typedef struct lista_string{
	char* str;
}lista_string;

typedef struct lista_numbers{
	double d;
}lista_numbers;

typedef struct lista_libfunc{
	char* str;
}lista_libfunc;

typedef struct lista_userfunc{
	unsigned address;
	unsigned localsize;
	char* id;
}lista_userfunc;


unsigned magicnum;
unsigned globals_size;
	
unsigned str_no;
lista_string* str_consts;

unsigned num_no;
lista_numbers* num_consts;

unsigned usr_no;
lista_userfunc* usr_consts;

unsigned lib_no;
lista_libfunc* lib_consts;

unsigned code_no;
instruction* code;


unsigned AVM_ENDING_PC ;

void print_num_table();
void print_string_table();
void print_libfunc_table();
void print_userfunc_table();
void print_vmarg(vmarg* arg);
void print_code();
void print_tables();
void print_magicNum();
void print_globals_size();
void print_input_binary();

int read_binary();

typedef void (*library_func_t) (void);

struct avm_memcell;
struct avm_table;
struct avm_table_bucket;

typedef enum avm_memcell_enum{
	number_m   = 0,
	string_m   = 1,
	bool_m     = 2,
	table_m    = 3,
	userfunc_m = 4,
	libfunc_m  = 5,
	nil_m      = 6,
	undef_m    = 7
}avm_memcell_e;


typedef struct avm_table_bucket{
	struct avm_memcell* key; /* pointer ??? or not */
	struct avm_memcell* value;
	struct avm_table_bucket* next;
}avm_table_bucket;

typedef struct avm_table{
	unsigned randomID;
	unsigned refCounter;  
    avm_table_bucket* strIndexed[AVM_TABLE_HASHSIZE]; /*hashtable for strings */
	avm_table_bucket* numIndexed[AVM_TABLE_HASHSIZE]; /*hashtable for nums */
	avm_table_bucket* boolIndexed[AVM_TABLE_HASHSIZE];
	avm_table_bucket* libIndexed[AVM_TABLE_HASHSIZE];
	avm_table_bucket* userIndexed[AVM_TABLE_HASHSIZE];
	
	unsigned totalstr;
	unsigned totalnum;
	unsigned totalbool;
	unsigned totallib;
	unsigned totaluser;
}avm_table;

typedef struct avm_memcell{
	avm_memcell_e type;
	union{
		double        numVal;
		char*         strVal;
		unsigned char boolVal;
		avm_table*    tableVal;
		unsigned      funcVal;
		char*         libfuncVal;
	}data;
}avm_memcell;

avm_memcell_e decide_value_type(avm_table_bucket* b);

avm_memcell stack[AVM_STACKSIZE];

void avm_table_destroy(avm_table* t);

avm_memcell* avm_tablegetelem(avm_table *table, avm_memcell *key);

void avm_tablesetelem (avm_table *table, avm_memcell *key, avm_memcell *value);
//void avm_tablesetelem (avm_table *table, avm_memcell *key, avm_memcell *value);

 void avm_tableincrefcounter(avm_table* t);


void avm_tabledecrefcounter(avm_table* t);

void avm_table_buckets_init(avm_table_bucket** b);

void avm_memcell_clear(avm_memcell* m);

void avm_tablebucket_destroy(avm_table_bucket** p);

avm_table* avm_tablenew(void);

static void avm_initstack(void);

//----------------------------------------------------
void init_vars();


#define AVM_STACKENV_SIZE 4
#define AVM_NUMACTUALS_OFFSET +4
#define AVM_SAVEDPC_OFFSET +3
#define AVM_SAVEDTOP_OFFSET +2
#define AVM_SAVEDTOPSP_OFFSET +1
avm_memcell ax, bx, cx;
avm_memcell retval;
unsigned top, topsp; 

unsigned char executionFinised;
unsigned pc;
unsigned currLine;
unsigned codeSize ;

/*reverse traslation for constants: getting constant value from index*/

double consts_getnumber (unsigned index);
char* consts_getstring (unsigned index);
char* libfuncs_getused (unsigned index);
unsigned usrfuncs_getaddress (unsigned index);


void execute_arithmetic (instruction *);

#define execute_add execute_arithmetic
#define execute_sub execute_arithmetic
#define execute_mul execute_arithmetic
#define	execute_div execute_arithmetic
#define	execute_mod execute_arithmetic


/*o savvidis leei ksexwrista arxeia me thn ylopoihsh twn diaforetikwn omadwn*/


#define AVM_MAX_INSTRUCTIONS (unsigned) jump_v

extern void execute_assign (instruction*);

extern void execute_uminus (instruction*); //we do not implment this
extern void execute_and (instruction*); //we do not implment this
extern void execute_or (instruction*); //we do not implment this
extern void execute_not (instruction*); //we do not implment this

extern void execute_jeq (instruction*);
extern void execute_jne (instruction*);
extern void execute_jle (instruction*);
extern void execute_jge (instruction*);
extern void execute_jlt (instruction*);
extern void execute_jgt (instruction*);

extern void execute_call (instruction*);
extern void execute_pusharg (instruction*);
extern void execute_funcenter (instruction*);
extern void execute_funcexit (instruction*);

extern void execute_newtable (instruction*);
extern void execute_tablegetelem (instruction*);
extern void execute_tablesetelem (instruction*);

extern void execute_nop (instruction*);
extern void execute_jump (instruction*);



extern void memclear_string(avm_memcell* m);
extern void memclear_table (avm_memcell* m);



void avm_memcellclear (avm_memcell *m);

extern void avm_warning (char * format, ...);
extern void avm_assign (avm_memcell *lv, avm_memcell *rv);

extern void avm_error (char* format);
extern char* avm_tostring (avm_memcell*);
extern void avm_calllibfunc (char *funcName);
extern void avm_callsaveenviroment (void);



unsigned totalActuals ;

void avm_dec_top (void);
void avm_push_envvalue (unsigned val);


extern lista_userfunc* avm_getfuncinfo (unsigned address);

unsigned avm_get_envvalue (unsigned i);



typedef void (*library_func_t) (void);
library_func_t avm_getlibraryfunc( char *id);

unsigned avm_totalactuals (void);
avm_memcell* avm_getactual (unsigned i);

void avm_registerlibfunc (char *id, library_func_t addr);
unsigned hash_libfunc(char *name);



extern char* number_tostring (avm_memcell*);
extern char* string_tostring (avm_memcell*);
extern char* bool_tostring (avm_memcell*);
extern char* table_tostring (avm_memcell*);
extern char* userfunc_tostring (avm_memcell*);
extern char* libfunc_tostring (avm_memcell*);
extern char* nil_tostring (avm_memcell*);
extern char* undef_tostring(avm_memcell*);


/* gia ta arithmetic expressions */

double add_impl (double x, double y);
double sub_impl (double x, double y);
double mul_impl (double x, double y);
double div_impl (double x, double y);
double mod_impl (double x, double y);


/* aytomati metatropi se bool */

unsigned char number_tobool (avm_memcell *m);
unsigned char string_tobool (avm_memcell *m);
unsigned char bool_tobool (avm_memcell *m);
unsigned char table_tobool (avm_memcell *m);
unsigned char userfunc_tobool (avm_memcell *m);
unsigned char libfunc_tobool (avm_memcell *m);
unsigned char nil_tobool (avm_memcell *m);
unsigned char undef_tobool (avm_memcell *m);

unsigned char avm_tobool (avm_memcell *m);

void avm_initialize (void);

//TODO argument(i)

void execute_cycle();



void libfunc_print (void);
void libfunc_typeof (void);
void libfunc_totalarguments (void);
void libfunc_input (void);
void libfunc_objectmemberkeys (void);
void libfunc_objecttotalmembers (void);
void libfunc_objectcopy (void);
void libfunc_argument (void);
void libfunc_strtonum (void);
void libfunc_sqrt (void);
void libfunc_cos (void);
void libfunc_sin (void);

unsigned called_function;
void skip_function_definition(void);
void tune_labels(void);
