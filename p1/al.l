 /*	
 * bomb-style
 *	Project Name: Lexical Analysis cs340
 *	Authors: 
 *			Fotios Mathioudakis
 *			Leonidas Anagnostou
 *			Evaggelia Athanasaki
 */

/*
 *	Definitions Section
 *		> User code				: {% code %}
 *		> Regex.aliases			: e.g. string 	\"[^\n]*\" 
 *		> Lex. params			: %options
 *		> user-def conditions
 */

 /* ==== CODE (includes etc) ==== */

%{

 	#include <stdio.h>
	#include <string.h>
	#include <stdlib.h>
	#define YY_DECL int alpha_yylex (void* yylval)

	unsigned int counter=0;
	unsigned int opencomm=0;

	char* buffer;

	unsigned int buf_len=500;
	unsigned int buf_line;
	unsigned int err_flag=0;

	typedef struct alpha_token_t{
		const char* text;
		char* type;
		unsigned int line;
		unsigned int count;	// # of tokens
		struct alpha_token_t* next;
	} a_token;

	a_token* head=NULL;
	a_token* curr=NULL;

	void add_token(char* text, unsigned int lineno, const char* type){
		a_token* buff;
		buff=(a_token*)malloc(sizeof(struct alpha_token_t));
		if(!buff){
			fprintf(stderr,"Error in add_token malloc\n");
			exit(-1);
		}
		buff->text=(char*)malloc(sizeof(text));
		if(!buff){
			fprintf(stderr,"Error in text_malloc\n");
			exit(-1);
		}
		buff->text=strdup(text);
		buff->next=NULL;
		buff->line=lineno;
		buff->count=++counter;
		buff->type=strdup(type);
		if(head==NULL){
			head=buff;
			curr=buff;
			return;
		}
		else{
			curr->next=buff;
			curr=buff;
			return;
		}
		return;
	}
	
	void print_tokens(){
		a_token* ptr=head;
		if(ptr==NULL){
			fflush(stdout);
			fprintf(stderr,"The list was empty\n");
		}
		while(ptr!=NULL){
			fflush(stdout);
			fprintf(stdout,"%d\t\t#%d\t\t%s\t\t\t\t\t\t%s\n",ptr->line,ptr->count,ptr->text,ptr->type);
			ptr=ptr->next;
		}
		return;
	}

	void increase_buf(char **buf){

		if((unsigned int)strlen(yytext) > buf_len-(unsigned int)strlen(*buf)){
			*buf=(char *)realloc(*buf, buf_len+strlen(yytext)*2);
			buf_len += (unsigned int)strlen(yytext);
		}
	}
	
%}

%option header-file="./scanner.h"
%option noyywrap
%option yylineno


IF			"if"
ELSE		"else"
WHILE		"while"
FOR			"for"
FUNCTION	"function"
RETURN		"return"
BREAK		"break"
CONTINUE	"continue"
AND			"and"
NOT			"not"
OR			"or"
LOCAL 		"local"
TRUE		"true"
FALSE		"false"
NIL			"nil"


ASSIGN		"="
PLUS		"+"
MINUS		"-"
MUL			"*"
DIV			"/"
MOD			"%"
EQ 			"=="
NEQ			"!="
PP			"++"
MM			"--"
GREATER		">"
LESS		"<"
GEQ			">="
LEQ			"<="


DIGIT		[0-9]
INTEGER		[0-9]+
REAL		[0-9]+\.[0-9]*
STR 		"\""
WHITESPACE  [ \r\t]+
NEWLINE		[\n]


BRKT_O		"{"
BRKT_C		"}"
SBRKT_O		"["
SBRKT_C		"]"
PAR_O		"("
PAR_C		")"
SEMICOL		";"
COMMA		","
COLON		":"
COLON2		"::"
DOT			"."
DOT2		".."


ID			[A-Za-z][0-9_A-Za-z]*


SCOMM 		"//".*
MCOMM_O		"/*"
MCOMM_C		"*/"

%x STRING COMMENT
%%



{IF} 				{	
						add_token(yytext, yylineno, "IF");
 					}

{ELSE} 				{
						add_token(yytext, yylineno, "ELSE");
 					}
 					
{WHILE} 			{
						add_token(yytext, yylineno, "WHILE");
 					}
 					
{FOR} 				{
						add_token(yytext, yylineno, "FOR");
 					}
 					
{FUNCTION} 			{
						add_token(yytext, yylineno, "FUNCTION");
 					}
 					
{RETURN} 			{
						add_token(yytext, yylineno, "RETURN");
 					}
 					
{BREAK} 			{
						add_token(yytext, yylineno, "BREAK");
 					}
 					
{CONTINUE} 			{
						add_token(yytext, yylineno, "CONTINUE");
 					}
 					
{AND} 				{
						add_token(yytext, yylineno, "AND");
 					}
 					
{NOT} 				{
						add_token(yytext, yylineno, "NOT");
 					}
 					
{OR} 				{
						add_token(yytext, yylineno, "OR");
 					}
 					
{LOCAL} 			{
						add_token(yytext, yylineno, "LOCAL");
 					}
 					
{TRUE} 				{
						add_token(yytext, yylineno, "TRUE");
 					}
 					
{FALSE} 			{
						add_token(yytext, yylineno, "FALSE");
 					}
 					
{NIL} 				{
						add_token(yytext, yylineno, "NIL");
 					}
 					
{ASSIGN}			{
						add_token(yytext, yylineno, "ASSIGN");
 					}
 					
{PLUS} 				{
						add_token(yytext, yylineno, "PLUS");
 					}
 					
{MINUS} 			{
						add_token(yytext, yylineno, "MINUS");
 					}
 					
{MUL} 				{
						add_token(yytext, yylineno, "MUL");
 					}
 					
{DIV} 				{
						add_token(yytext, yylineno, "DIV");
 					}
 					
{MOD} 				{
						add_token(yytext, yylineno, "MOD");
 					}
 					
{EQ} 				{
						add_token(yytext, yylineno, "EQ");
 					}
 					
{NEQ} 				{
						add_token(yytext, yylineno, "NEQ");
 					}
 					
{PP} 				{
						add_token(yytext, yylineno, "PP");
 					}
 					
{MM} 				{
						add_token(yytext, yylineno, "MM");
 					}
 					
{GREATER} 			{
						add_token(yytext, yylineno, "GREATER");
 					}
 					
{LESS} 				{
						add_token(yytext, yylineno, "LESS");
 					}
 					
{GEQ} 				{
						add_token(yytext, yylineno, "GEQ");
 					}
 					
{LEQ} 				{
						add_token(yytext, yylineno, "LEQ");
 					}
 					
{DIGIT} 			{
						add_token(yytext, yylineno, "DIGIT");
 					}
 					
{INTEGER} 			{
						add_token(yytext, yylineno, "INTEGER");
 					}
 					
{REAL} 				{
						add_token(yytext, yylineno, "REAL");
 					}

{WHITESPACE}		{}

{NEWLINE}			{}

{ID}				{
						add_token(yytext, yylineno, "ID");
					}

{BRKT_O} 			{	
						add_token(yytext, yylineno, "BRKT_O");
 					}

{BRKT_C} 			{	
						add_token(yytext, yylineno, "BRKT_C");
 					}

{SBRKT_O} 			{	
						add_token(yytext, yylineno, "SBRKT_O");
 					}

{SBRKT_C} 			{	
						add_token(yytext, yylineno, "SBRKT_C");
 					}

{PAR_O} 			{	
						add_token(yytext, yylineno, "PAR_O");
 					}

{PAR_C} 			{	
						add_token(yytext, yylineno, "PAR_C");
 					}

{SEMICOL} 			{	
						add_token(yytext, yylineno, "semicol");
 					}

{COMMA} 			{	
						add_token(yytext, yylineno, "COMMA");
 					}

{COLON} 			{	
						add_token(yytext, yylineno, "COLON");
 					}

{COLON2} 			{	
						add_token(yytext, yylineno, "COLON2");
 					}

{DOT} 				{	
						add_token(yytext, yylineno, "DOT");
 					}

{DOT2} 				{	
						add_token(yytext, yylineno, "DOT2");
 					}

{STR}				{
						buffer= malloc(buf_len);

						buffer= memset(buffer, 0, buf_len);
						buf_line= yylineno;
						err_flag= 0;
						BEGIN(STRING);
					}
<STRING>{STR}		{	
						if(err_flag){
							//printf("FAULTY_STRING: %s\n\tline: %d\n", buffer, buf_line);
							add_token(buffer, buf_line, "ERROR");

						}
						else{
							//printf("STRING: %s\n\tline: %d\n", buffer, buf_line);
							add_token(buffer, buf_line, "STRING");
						}
						free(buffer);
						BEGIN(INITIAL);
					}

<STRING>([^\\|\"])*	{
						increase_buf(&buffer);
						strcat(buffer, yytext);
					}
					
<STRING>\\			{
						increase_buf(&buffer);
						err_flag= 1;
					}

<STRING>\\"n"		{
						increase_buf(&buffer);
						strcat(buffer, "\n");
					}

<STRING>\\"t"		{
						increase_buf(&buffer);
						strcat(buffer, "\t");
					}

<STRING>\\\\		{
						increase_buf(&buffer);
						strcat(buffer, "\\");
					}

<STRING>\\\"		{
						increase_buf(&buffer);
						strcat(buffer, "\"");
					}

<STRING><<EOF>>			{
						//printf("FAULTY_STRING: %s\n\tline: %d\n", buffer, buf_line);
						add_token(buffer, buf_line, "ERROR");
						free(buffer);
						BEGIN(INITIAL);
						
					}



{SCOMM} 			{
						add_token("comment", yylineno, "SCOMM");
 					}
 					
{MCOMM_O}			{	
						buf_line= yylineno;
						opencomm++;
						BEGIN(COMMENT);
					}
<COMMENT>{MCOMM_O}	{	//kainourio multiline comment
						opencomm++;
						buf_line= yylineno;
						add_token("nested comment", buf_line, "MCOMM");
					}

<COMMENT>[^*]*[^*/]	{ 
						//trwei tous arxistous xaraktires 
					}

<COMMENT>"*"+[^/]	{	//otan dei asteraki mono tou osesdipote fores
						
					}

<COMMENT>"/"		{	//otan vrei monitis ksemarki mia katheto
						
					}

<COMMENT>"*"+"/"	{
						opencomm--;
						if(opencomm==0){
							add_token("comment", buf_line, "MCOMM");
							BEGIN(INITIAL);
						}
					}
<COMMENT><<EOF>>		{
 						//printf("ERROR: COMMENT NEVER ENDS\n");
 						add_token("comment", buf_line, "ERROR");
 						BEGIN(INITIAL);
 						
 					}




.					{add_token(yytext, yylineno, "UNDEF");}


%%

int main(int argc, char ** argv){
	int l;
	if(argc>1){
		if(!(yyin= fopen(argv[1], "r") )){
			fprintf(stderr, "cannot read file: %s\n\n", argv[1]);
			return 1;
		}
	}else
		yyin= stdin;

	l=alpha_yylex(head);
	print_tokens();
	return 0;
}



