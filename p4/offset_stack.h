#include <stdio.h>
#include <stdlib.h>

typedef struct offsetNode_s{
	unsigned offset;
	struct offsetNode_s *next;
}offsetNode;

offsetNode *topHead;

void addOffsetNode(unsigned offset);

unsigned popOffsetNode();

void printOffsets();