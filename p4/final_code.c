#include "final_code.h"
//extern struct incomplete_jump *incomplete_jump_HEAD;
/*struct vmarg{
	vmarg_t type;
	unsigned val;
}

struct instruction {
	vmopcode opcode;
	vmarg result;
	vmarg arg1;
	vmarg arg2;
	unsigned srcLine;
};
*/
/*
 * Edw prpei na exoume 4 pinakes opou tha apothikeuoume katallhla strings, 
 * numbers, libfuncs, userfuncs.
 * 
 * Oi parakatw sunarthseis tha mas epistrefoun jswemblpoem :)
 */
struct incomplete_jump *incomplete_jump_HEAD= (incomplete_jump*) 0;

unsigned ij_total= 0;

struct instruction* instructionHEAD;

unsigned instructionNo=0;

unsigned currProcessedQuad=0;

struct funcStack* funcstack=NULL;

typedef void (*generator_func_t)(quad*);

generator_func_t generators[]= {
	generate_ASSIGN,
	generate_ADD,
	generate_SUB,
	generate_MUL,
	generate_DIV,
	generate_MOD,
	//-------------------------
	generate_UMINUS,
	//-------------------------
	generate_AND,
	generate_OR,
	generate_NOT,
	generate_IF_EQ,
	generate_IF_NOTEQ,
	generate_IF_LESSEQ,
	generate_IF_GREATEREQ,
	generate_IF_LESS,
	generate_IF_GREATER,
	//-------------------------
	generate_CALL,
	generate_PARAM,
	generate_RETURN,
	generate_GETRETVAL,
	generate_FUNCSTART,
	generate_FUNCEND,
	//-------------------------
	generate_NEWTABLE,
	generate_TABLEGETELEM,
	generate_TABLESETELEM,
	generate_JUMP,
	generate_NOP
};


unsigned consts_newstring(char* s){
	lista_string* tmp=malloc(sizeof(struct lista_string_s));
	lista_string* curr;
	tmp->str = malloc(strlen(s));
	tmp->str=strdup(s);
	
	if(lista_string_HEAD==NULL){
		lista_string_HEAD=tmp;
		tmp->next=NULL;
		tmp->offset = 0;
		totalStringConsts = 1;
	}
	else{
		curr=lista_string_HEAD;
		while(curr->next!=NULL)
			curr=curr->next;
		curr->next=tmp;
		tmp->next=NULL;
		tmp->offset = curr->offset + 1;
		totalStringConsts++;
	}

	return totalStringConsts - 1;
}

unsigned consts_newnumber(double n){
	lista_numbers* tmp=malloc(sizeof(struct lista_numbers_s));
	lista_numbers* curr;
	tmp->d = n;
	
	if(lista_numbers_HEAD==NULL){
		lista_numbers_HEAD=tmp;
		tmp->next=NULL;
		tmp->offset = 0;
		totalNumConsts = 1;
	}
	else{
		curr=lista_numbers_HEAD;
		while(curr->next!=NULL)
			curr=curr->next;
		curr->next=tmp;
		tmp->next=NULL;
		tmp->offset = curr->offset + 1;
		totalNumConsts++;
	}
	return totalNumConsts - 1;
}
unsigned libfuncs_newused(char* s){
	lista_libfunc* tmp=malloc(sizeof(struct lista_libfunc_s));
	lista_libfunc* curr;
	tmp->str = malloc(strlen(s));
	tmp->str=strdup(s);
	
	if(lista_libfunc_HEAD==NULL){
		lista_libfunc_HEAD=tmp;
		tmp->next=NULL;
		tmp->offset = 0;
		totalNamedLibfuncs = 1;
	}
	else{
		curr=lista_libfunc_HEAD;
		while(curr->next!=NULL)
			curr=curr->next;
		curr->next=tmp;
		tmp->next=NULL;
		tmp->offset = curr->offset + 1;
		totalNamedLibfuncs++;
	}
	return totalNamedLibfuncs - 1;
}
unsigned userfuncs_newfunc(SymbolTableEntry *sym){
	lista_userfunc* tmp=malloc(sizeof(struct lista_userfunc_s));
	lista_userfunc* curr;
	tmp->id = malloc(strlen(sym->name));
	tmp->id = strdup (sym->name);
	tmp->localsize = sym->local_size; 
	tmp->address = sym->taddress;

	if(lista_userfunc_HEAD==NULL){
		lista_userfunc_HEAD=tmp;
		tmp->next=NULL;
		totalUserFuncs = 1;
	}
	else{
		curr=lista_userfunc_HEAD;
		while(curr->next!=NULL)
			curr=curr->next;
		curr->next=tmp;
		tmp->next=NULL;
		totalUserFuncs++;
	}

	return totalUserFuncs - 1;
}

void make_operand(expr* e, vmarg* arg){
	if(e==NULL){
		//arg=NULL;
		unused_operand(arg);
	}
	else{	
		switch(e->type){
			case var_e:
			case tableitem_e:
			case arithexpr_e:
			case boolexpr_e:
			case assignexpr_e:
			case newtable_e:
				assert(e->sym);
				arg->value= e->sym->offset;

				switch(e->sym->space){
					case programvar:
						arg->type= global_a;
						break;
					case functionLocal:
						arg->type= local_a;
						break;
					case formalarg:
						arg->type= formal_a;
						break;
					default:
						assert(0);
				}
				break;

			case constbool_e:
				if(e->boolConst=='T')
					arg->value= true;
				else
					arg->value= false;
				arg->type= bool_a;
				break;

			case conststring_e:
				arg->value= consts_newstring(e->strConst);
				arg->type= string_a;
				break;

			case constnum_e:
				arg->value= consts_newnumber(e->numConst);
				arg->type= number_a;
				break;

			case nil_e:
				arg->type= nil_a;
				break;

			case programfunc_e:
				arg->type= userfunc_a;
				arg->value= e->sym->offset;
				/*	or alternatively
				arg->value= userfuncs_newfunc(e->sym);
				*/
				break;

			case libraryfunc_e:
				arg->type= libfunc_a;
				arg->value= libfuncs_newused(e->sym->name);
				break;

			default:
				assert(0);			
		}
	}
}


unsigned getInstructionNo(){
	return instructionNo;
}

unsigned incrInstructionNo(){
	return ++instructionNo;
}

unsigned nextInstructionLabel(){
	return instructionNo+1;
}

unsigned getCurrProcessedQuad(){
	return currProcessedQuad;
}

unsigned incrCurrProcessedQuad(){
	return ++currProcessedQuad;
}


void emit_final(struct instruction t){
	struct instruction *curr;
	struct instruction *tmp = malloc(sizeof(struct instruction));
	tmp->opcode = t.opcode;
	tmp->arg1 = t.arg1;
	tmp->arg2 = t.arg2;
	tmp->result = t.result;
	tmp->srcLine = incrInstructionNo();
	
	tmp->next = NULL;

	curr = instructionHEAD;
	if(instructionHEAD==NULL){
		instructionHEAD=tmp;
	}
	else{
		while(curr->next!=NULL){
			curr=curr->next;
		}
		curr->next = tmp;
	}
}

struct instruction* findInstruction(unsigned label){
	struct instruction *curr=instructionHEAD;
	while(curr!=NULL){
		if(curr->srcLine!=label){
			curr=curr->next;
		}
		else
			return curr;
	}
	return NULL;
}


void make_numberoperand(vmarg* arg, double val){
	arg->value= consts_newnumber(val); // fix this
	arg->type= number_a;
}

void make_booloperand(vmarg* arg, unsigned val){
	arg->value= val;
	arg->type= bool_a;
}

void make_retvaloperand(vmarg* arg){
	arg->value= 0;
	arg->type= retval_a;
}


void add_incomplete_jump(unsigned instrNo, unsigned iaddress){
	struct incomplete_jump* tmp=malloc(sizeof(struct incomplete_jump));
	struct incomplete_jump* curr;
	tmp->instrNo = instrNo;
	tmp->iaddress = iaddress;

	if(incomplete_jump_HEAD==NULL){
		tmp->next=NULL;
		incomplete_jump_HEAD=tmp;
	}
	else{
		tmp->next = incomplete_jump_HEAD->next;
		incomplete_jump_HEAD->next = tmp;
	}
	ij_total++;
}


void patch_incomplete_jumps(){
	struct instruction* t;
	struct incomplete_jump * ijump= incomplete_jump_HEAD;
	quad* q;

	while(ijump!=NULL){
		t= findInstruction(ijump->instrNo);
		q= findQuad(ijump->iaddress);

		t->result.type= label_a;

		if(q==NULL){
			t->result.value= nextInstructionLabel();
		}
		else{
			t->result.value= q->taddress;
		}

		ijump= ijump->next;
	} 

	/* if patch_incomplete_jumps() will be called just
	 once, following code is not necessery */
	incomplete_jump_HEAD= NULL;
	ij_total=0;
}


void generate(iopcode op, quad* q){
	instruction t;
	t.opcode= op; //opcode swsto
	make_operand(q->arg1, &t.arg1);
	make_operand(q->arg2, &t.arg2);
	make_operand(q->result, &t.result);
	q->taddress= nextInstructionLabel();
	emit_final(t);
}

void generate_ASSIGN(quad* q){
	generate(assign_v, q);
}

void generate_ADD(quad* q){
	generate(add_v, q);
}

void generate_SUB(quad* q){
	generate(sub_v, q);
}

void generate_MUL(quad* q){
	generate(mul_v, q);
}

void generate_DIV(quad* q){
	generate(div_v, q);
}

void generate_MOD(quad* q){
	generate(mod_v, q);
}

void generate_NEWTABLE(quad* q){
	generate(newtable_v, q);
}

void generate_TABLEGETELEM(quad* q){
	generate(tablegetelem_v, q);
}

void generate_TABLESETELEM(quad* q){
	generate(tablesetelem_v, q);
}

void generate_NOP(){
	instruction t;
	t.opcode= nop_v;
	unused_operand(&t.arg1);
	unused_operand(&t.arg2);
	unused_operand(&t.result);
	emit_final(t);
}

void generate_UMINUS(quad* q){
	instruction t;
	t.opcode= mul_v;
	make_operand(q->arg1, &t.arg1);
	make_operand(q->result, &t.result);
	make_numberoperand(&t.arg2, -1);

	q->taddress= nextInstructionLabel();
	emit_final(t);
}

void generate_relational(iopcode op, quad* q){
	instruction t;
	t.opcode= op;
	make_operand(q->arg1, &t.arg1);
	make_operand(q->arg2, &t.arg2);

	t.result.type= label_a;
	
	if(q->jLabel < getCurrProcessedQuad())
		t.result.value= findQuad(q->jLabel)->taddress;
	else
		add_incomplete_jump(nextInstructionLabel(), q->jLabel);

	q->taddress= nextInstructionLabel();
	emit_final(t);
}

void generate_JUMP(quad* q){
	generate_relational(jump_v, q);
}

void generate_IF_EQ(quad* q){
	generate_relational(jeq_v, q);
}

void generate_IF_NOTEQ(quad* q){
	generate_relational(jne_v, q);
}

void generate_IF_GREATER(quad* q){
	generate_relational(jgt_v, q);
}

void generate_IF_GREATEREQ(quad* q){
	generate_relational(jge_v, q);
}

void generate_IF_LESS(quad* q){
	generate_relational(jlt_v, q);
}

void generate_IF_LESSEQ(quad* q){
	generate_relational(jle_v, q);
}

void generate_NOT(quad* q){
	q->taddress= nextInstructionLabel();
	instruction t;

	t.opcode= jeq_v;
	make_operand(q->arg1, &t.arg1);
	make_booloperand(&t.arg2, false);
	t.result.type= label_a;
	t.result.value= nextInstructionLabel() +3;
	emit_final(t);

	t.opcode= assign_v;
	make_booloperand(&t.arg1, false);
	unused_operand(&t.arg2);
	make_operand(q->result, &t.result);
	emit_final(t);

	t.opcode= jump_v;
	unused_operand(&t.arg1);
	unused_operand(&t.arg2);
	t.result.type= label_a;
	t.result.value= nextInstructionLabel() +2;
	emit_final(t);

	t.opcode= assign_v;
	make_booloperand(&t.arg1, true);
	unused_operand(&t.arg2);
	make_operand(q->result, &t.result);
	emit_final(t);
}

void generate_OR(quad* q){
	q->taddress= nextInstructionLabel();
	instruction t;

	t.opcode= jeq_v;
	make_operand(q->arg1, &t.arg1);
	make_booloperand(&t.arg2, true);
	t.result.type= label_a;
	t.result.value= nextInstructionLabel() +4;
	emit_final(t);

	make_operand(q->arg2, &t.arg1);
	t.result.value= nextInstructionLabel() +3;
	emit_final(t);

	t.opcode= assign_v;
	make_booloperand(&t.arg1, false);
	unused_operand(&t.arg2);
	make_operand(q->result, &t.result);
	emit_final(t);

	t.opcode= jump_v;
	unused_operand(&t.arg1);
	unused_operand(&t.arg2);
	t.result.type= label_a;
	t.result.value= nextInstructionLabel() +2;
	emit_final(t);

	t.opcode= assign_v;
	make_booloperand(&t.arg1, true);
	unused_operand(&t.arg2);
	make_operand(q->result, &t.result);
	emit_final(t);

}

void generate_AND(quad* q){
	q->taddress= nextInstructionLabel();
	instruction t;

	t.opcode= jeq_v;
	make_operand(q->arg1, &t.arg1);
	make_booloperand(&t.arg2, false);
	t.result.type= label_a;
	t.result.value= nextInstructionLabel() +4;
	emit_final(t);

	make_operand(q->arg2, &t.arg1);
	t.result.value= nextInstructionLabel() +3;
	emit_final(t);

	t.opcode= assign_v;
	make_booloperand(&t.arg1, true);
	unused_operand(&t.arg2);
	make_operand(q->result, &t.result);
	emit_final(t);

	t.opcode= jump_v;
	unused_operand(&t.arg1);
	unused_operand(&t.arg2);
	t.result.type= label_a;
	t.result.value= nextInstructionLabel() +2;
	emit_final(t);

	t.opcode= assign_v;
	make_booloperand(&t.arg1, false);
	unused_operand(&t.arg2);
	make_operand(q->result, &t.result);
	emit_final(t);
}

void generate_PARAM(quad* q){
	q->taddress= nextInstructionLabel();
	instruction t;
	t.opcode= pusharg_v;
	make_operand(q->result, &t.arg1);
	unused_operand(&t.result);
	unused_operand(&t.arg2);
	emit_final(t);
}

void generate_CALL(quad* q){
	q->taddress= nextInstructionLabel();
	instruction t;
	t.opcode= call_v;
	make_operand(q->result, &t.arg1);
	unused_operand(&t.result);
	unused_operand(&t.arg2);
	emit_final(t);
}

void generate_GETRETVAL(quad* q){
	q->taddress= nextInstructionLabel();
	instruction t;
	t.opcode= assign_v;
	make_retvaloperand(&t.arg1);
	unused_operand(&t.arg2);
	make_operand(q->result, &t.result);

	emit_final(t);
}

void generate_FUNCSTART(quad* q){
	SymbolTableEntry* f;
	f= q->result->sym;
	// telika to parakato den einai akuro
	f->taddress= nextInstructionLabel();
	q->taddress= nextInstructionLabel();
	
	f->offset= userfuncs_newfunc(f);
	funcstack= push_stack(funcstack);

	instruction t;
	t.opcode= funcenter_v;
	make_operand(q->result, &t.arg1);
	unused_operand(&t.result);
	unused_operand(&t.arg2);
	emit_final(t);
}

void generate_RETURN(quad* q){
	q->taddress= nextInstructionLabel();
	instruction t;
	t.opcode= assign_v;
	make_retvaloperand(&t.result);	
	make_operand(q->result, &t.arg1);
	unused_operand(&t.arg2);
	emit_final(t);

	funcstack= add_return_stack(funcstack, nextInstructionLabel() );

	t.opcode= jump_v;
	reset_operand(&t.arg1);
	reset_operand(&t.arg2);
	t.result.type= label_a;
	emit_final(t);
}

void generate_FUNCEND(quad* q){
	struct rlist* r_list= top_stack(funcstack);
	printf("this is the debugger speaking %u\n", nextInstructionLabel());
	backpatch_returnlist(r_list, nextInstructionLabel() );

	funcstack=pop_stack(funcstack);

	q->taddress= nextInstructionLabel();
	instruction t;
	t.opcode= funcexit_v;
	make_operand(q->result, &t.arg1);
	unused_operand(&t.result);
	unused_operand(&t.arg2);
	emit_final(t);
}

void generate_final_code(){
	quad* q;
	unsigned i;

	for(i=1; i<=getQuadNo(); i++){
		incrCurrProcessedQuad();
		q=findQuad(i);
		//printf("op: %d\n", q->op);
		(*generators[q->op])(q);

	}

	patch_incomplete_jumps();
}


struct funcStack* push_stack(struct funcStack* head){
	struct funcStack* tmp= malloc(sizeof(struct funcStack));
	tmp->returnlist= NULL;

	if(head==NULL)
		tmp->next= NULL;
	else
		tmp->next= head;

	return tmp;
}

struct funcStack* pop_stack(struct funcStack* head){
	if(head!=NULL)
		head= head->next;

	return head;
}

struct rlist* top_stack(struct funcStack* head){
	if(head==NULL)
		return NULL;
	
	return head->returnlist;
}

struct funcStack* add_return_stack(struct funcStack* head, unsigned label){
	struct rlist* tmp= malloc(sizeof(struct rlist));
	tmp->label= label;

	if(head!=NULL){
		tmp->returnlist= head->returnlist;
		head->returnlist= tmp;
	}
	
	return head;
}

void backpatch_returnlist(struct rlist* list, unsigned label){
	struct instruction* t;
	struct rlist* curr= list;

	while(curr!=NULL){
		t= findInstruction(curr->label);
		t->result.type= label_a;
		t->result.value= label;

		curr= curr->returnlist;
	}
}

void reset_operand(vmarg* arg){
	arg= NULL;
}


void unused_operand(vmarg* arg){
	arg->type= unused_a;
	arg->value= 0;
}


unsigned magicNumber(){
	return (unsigned) MAGIC_NUMBER;
}


void print_vmarg(vmarg* arg){
	if(arg!=NULL)
		printf("%d:%u ", arg->type, arg->value);
}


void print_code(){
	instruction* t= instructionHEAD;
	printf("INSTRUCTIONS:\n");
	printf("%u\n", getInstructionNo());

	while(t!=NULL){
		printf("%u ", t->opcode);
		print_vmarg(&t->arg1);
		print_vmarg(&t->arg2);
		print_vmarg(&t->result);
		printf("\n");
		t=t->next;
	}
}

void print_num_table(){
	printf("CONSTS:\n");
	printf("%u\n", totalNumConsts);
	lista_numbers *curr = lista_numbers_HEAD;

	while(curr != NULL){
		printf("%u\t%f\n", curr->offset, curr->d);
		curr = curr->next;
	}
}

void print_string_table(){
	printf("STRINGS:\n");
	printf("%u\n", totalStringConsts);
	lista_string *curr = lista_string_HEAD;

	while(curr != NULL){
		printf("%u\t%u %s\n", curr->offset,  (unsigned)strlen(curr->str), curr->str);
		curr = curr->next;
	}

}

void print_libfunc_table(){
	printf("LIBRARY FUNCS:\n");
	printf("%u\n", totalNamedLibfuncs);
	lista_libfunc* curr = lista_libfunc_HEAD;
	
	while(curr != NULL){
		printf("%u\t%s\n", curr->offset, curr->str);
		curr = curr->next;
	}
}

void print_userfunc_table(){
	unsigned i = 0;
	printf("USER FUNCS:\n");
	printf("%u\n", totalUserFuncs);
	lista_userfunc *curr = lista_userfunc_HEAD;

	while(curr != NULL){
		printf("%u\t%u %u %s \n", i, curr->address, curr->localsize, curr->id);
		curr = curr->next;
		i++;
	}
}

void print_tables(){
	print_string_table();
	print_num_table();
	print_userfunc_table();
	print_libfunc_table();
}

void print_magicNumber(){
	printf("MAGIC NUMBER:\n");
	printf("%u\n", magicNumber());
}

void print_final_code(){
	print_magicNumber();
	print_tables();
	print_code();
}

int code_to_binary(){
	FILE *fd;
	int counter;
	unsigned size;
	
	unsigned magic= magicNumber();
	unsigned globals_coount= get_programVarOffset(); 
	instruction* t= instructionHEAD;
	lista_string* str_l= lista_string_HEAD;
	lista_numbers* num_l= lista_numbers_HEAD;
	lista_userfunc* userf_l= lista_userfunc_HEAD;
	lista_libfunc* libf_l= lista_libfunc_HEAD;

	fd= fopen(BINARY_FILE,"wb");
	if(!fd){
		fprintf(stderr,"Unable to open file '%s'!", BINARY_FILE);
		return 1;
	}

	//magic number
	fwrite(&magic, sizeof(unsigned), 1, fd);

	//number of globals
	fwrite(&globals_count, sizeof(unsigned), 1, fd);
	
	//size of string array
	fwrite(&totalStringConsts, sizeof(unsigned), 1, fd);
	// string array
	while(str_l!=NULL){
		size= (unsigned) strlen(str_l->str);
		fwrite(&size, sizeof(unsigned), 1, fd);
		fwrite(str_l->str, sizeof(char)*size, 1, fd);		

		str_l= str_l->next;
	}


	//size of num array
	fwrite(&totalNumConsts, sizeof(unsigned), 1, fd);
	// num array
	while(num_l!=NULL){
		fwrite(&num_l->d, sizeof(double), 1, fd);		

		num_l= num_l->next;
	}


	//size of userfunc array
	fwrite(&totalUserFuncs, sizeof(unsigned), 1, fd);
	// userfunc array
	while(userf_l!=NULL){
		size= (unsigned) strlen(userf_l->id);
		fwrite(&userf_l->address, sizeof(unsigned), 1, fd);
		fwrite(&userf_l->localsize, sizeof(unsigned), 1, fd);
		fwrite(&size, sizeof(unsigned), 1, fd);
		fwrite(userf_l->id, sizeof(char)*size, 1, fd);		

		userf_l= userf_l->next;
	}


	//size of string array
	fwrite(&totalNamedLibfuncs, sizeof(unsigned), 1, fd);
	// string array
	while(libf_l!=NULL){
		size= (unsigned) strlen(libf_l->str);
		fwrite(&size, sizeof(unsigned), 1, fd);
		fwrite(libf_l->str, sizeof(char)*size, 1, fd);		

		libf_l= libf_l->next;
	}


	//size of code
	fwrite(&instructionNo, sizeof(unsigned), 1, fd);
	// code
	while(t!=NULL){
		fwrite(&t->opcode, sizeof(unsigned char), 1, fd);

		fwrite(&t->arg1.type, sizeof(unsigned char), 1, fd);
		fwrite(&t->arg1.value, sizeof(unsigned), 1, fd);

		fwrite(&t->arg2.type, sizeof(unsigned char), 1, fd);
		fwrite(&t->arg2.value, sizeof(unsigned), 1, fd);

		fwrite(&t->result.type, sizeof(unsigned char), 1, fd);
		fwrite(&t->result.value, sizeof(unsigned), 1, fd);

		t= t->next;
	}

	fclose(fd);
	return 0;
}