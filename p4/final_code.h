
#define lala 

/*
	magic number is the version number of the compiler
	it is the current date, year, month, day and a serial
	number (two digits) that reinitialiazes every day
*/
#define MAGIC_NUMBER 2018052401
#define BINARY_FILE "target.abc"

#include "quadtools.h"


typedef enum vmopcode{
	assign_v, /*0*/
	add_v, /*1*/
	sub_v, /*2*/
	mul_v, /*3*/ 
	div_v, /*4*/
	mod_v, /*5*/
	uminus_v, /*6*/
	and_v, /*7*/
	or_v, /*8*/
	not_v, /*9*/
	jeq_v, /*10*/
	jne_v, /*11*/
	jle_v, /*12*/
	jge_v, /*13*/
	jlt_v, /*14*/
	jgt_v, /*15*/
	call_v, /*16*/
	pusharg_v, /*17*/
	funcenter_v, /*18*/
	funcexit_v, /*19*/
	newtable_v, /*20*/
	tablegetelem_v, /*21*/
	tablesetelem_v, /*22*/
	nop_v, /*23*/
	jump_v /*24*/
}vmopcode;

typedef enum vmarg_t{
	label_a	=0,
	global_a =1,
	formal_a =2,
	local_a =3,
	number_a =4,
	string_a =5,
	bool_a =6,
	nil_a =7,
	userfunc_a =8,
	libfunc_a =9,
	retval_a =10,
	unused_a = 86
}vmarg_t;

typedef enum boolean{
	false =0,
	true =1
}boolean;

typedef struct vmarg{
	unsigned char type;
	unsigned value;
}vmarg;

typedef struct instruction {
	unsigned char opcode;
	vmarg result;
	vmarg arg1;
	vmarg arg2;
	unsigned srcLine;
	struct instruction *next;
}instruction;

typedef struct incomplete_jump{
	unsigned instrNo;
	unsigned iaddress;
	struct incomplete_jump* next;
}incomplete_jump;


typedef struct lista_string_s{
	char* str;
	unsigned offset;
	struct lista_string_s *next;
}lista_string;

typedef struct lista_numbers_s{
	double d;
	unsigned offset;
	struct lista_numbers_s *next;
}lista_numbers;

typedef struct lista_libfunc_s{
	char* str;
	unsigned offset;
	struct lista_libfunc_s *next;
}lista_libfunc;

typedef struct lista_userfunc_s{
	char* id;
	unsigned address;
	unsigned localsize;
	struct lista_userfunc_s *next;
}lista_userfunc;



struct rlist{
	unsigned label;
	struct rlist* returnlist;
};

struct funcStack{
	struct rlist* returnlist;
	struct funcStack* next;
};


lista_string *lista_string_HEAD;
lista_numbers *lista_numbers_HEAD;
lista_libfunc *lista_libfunc_HEAD;
lista_userfunc *lista_userfunc_HEAD;


unsigned totalNumConsts;
unsigned totalStringConsts;
unsigned totalNamedLibfuncs;
unsigned totalUserFuncs;


// struct incomplete_jump *incomplete_jump_HEAD= (incomplete_jump*) 0;

// //struct incompete_jump* ij_head= (incompete_jump*) 0;

// unsigned ij_total= 0;

// struct instruction* instructionHEAD;

// unsigned instructionNo=0;

// unsigned currProcessedQuad=0;

// struct funcStack* funcstack=NULL;


unsigned consts_newstring (char* s);

unsigned consts_newnumber (double n);

unsigned libfuncs_newused (char* s);

unsigned userfuncs_newfunc (SymbolTableEntry *sym);

void make_operand(expr* e, vmarg* arg);

unsigned getInstructionNo();

unsigned incrInstructionNo();

unsigned nextInstructionLabel();

unsigned getCurrProcessedQuad();

unsigned incrCurrProcessedQuad();

void emit_final(struct instruction t);

struct instruction* findInstruction(unsigned label);

void make_numberoperand(vmarg* arg, double val);

void make_booloperand(vmarg* arg, unsigned val);

void make_retvaloperand(vmarg* arg);

void add_incomplete_jump(unsigned instrNo, unsigned iaddress);

void patch_incomplete_jumps();

void generate(iopcode op, quad* q);

void generate_ASSIGN(quad* q);

void generate_ADD(quad* q);

void generate_SUB(quad* q);

void generate_MUL(quad* q);

void generate_DIV(quad* q);

void generate_MOD(quad* q);

void generate_NEWTABLE(quad* q);

void generate_TABLEGETELEM(quad* q);

void generate_TABLESETELEM(quad* q);

void generate_NOP();

void generate_UMINUS(quad* q);

void generate_relational(iopcode op, quad* q);

void generate_JUMP(quad* q);

void generate_IF_EQ(quad* q);

void generate_IF_NOTEQ(quad* q);

void generate_IF_GREATER(quad* q);

void generate_IF_GREATEREQ(quad* q);

void generate_IF_LESS(quad* q);

void generate_IF_LESSEQ(quad* q);

void generate_NOT(quad* q);

void generate_OR(quad* q);

void generate_AND(quad* q);

void generate_PARAM(quad* q);

void generate_CALL(quad* q);

void generate_GETRETVAL(quad* q);

void generate_FUNCSTART(quad* q);

void generate_RETURN(quad* q);

void generate_FUNCEND(quad* q);

void generate_final_code();

struct funcStack* push_stack(struct funcStack* head);

struct funcStack* pop_stack(struct funcStack* head);

struct rlist* top_stack(struct funcStack* head);

struct funcStack* add_return_stack(struct funcStack* head, unsigned label);

void backpatch_returnlist(struct rlist* list, unsigned label);

void reset_operand(vmarg* arg);

void unused_operand(vmarg* arg);

unsigned magicNumber();

// prints
void print_vmarg(vmarg* arg);
void print_code();
void print_userfunc_table();
void print_libfunc_table();
void print_string_table();
void print_num_table();
void print_tables();
void print_magicNumber();
void print_final_code();

int code_to_binary();
