#include "tmpVars.h"

void initHeads(){
	activeTmpHEAD=NULL;
	inactTmpHEAD=NULL;
}

tmpVar_t* newTempName(){
	/*creation of string */
	extern int tempNameCounter;
	char *result = malloc(16);
	char *tmp = malloc(16);
	char *tmp2 = malloc(16);
	tmpVar_t* tvar;
	strcpy(tmp, "_t");
	sprintf(tmp2,"%d",tempNameCounter);
	strcat(tmp, tmp2);
	result = tmp;

	/* passing stuff to struct */
	tvar = malloc(sizeof(tmpVar_t));
	tvar-> tname = malloc(32);
	strcpy(tvar->tname, result);

	/* +1 counter */
	tempNameCounter +=1;
	addToActiveTempList(tvar);
	return tvar;
}

void addToActiveTempList( tmpVar_t* t){
	t->next=activeTmpHEAD;
	activeTmpHEAD=t;
	// if(activeTmpHEAD->tname!=NULL)
	// 	fprintf(stderr, "aHEAD: %s\n", activeTmpHEAD->tname);
	// if(inactTmpHEAD->tname!=NULL)
	// 	fprintf(stderr, "iHEAD: %s\n", inactTmpHEAD->tname);
}

void removeFromActiveTempList(tmpVar_t* t){
	//fprintf(stderr, "%s\n", t->tname);
	//tmpVar_t *tmp;
	tmpVar_t *curr = activeTmpHEAD;
	tmpVar_t *prev;
	if(strcmp(curr->tname, t->tname)==0){
		//tmp=curr;
		//fprintf(stderr, "prwtoIFremove\n");
		activeTmpHEAD=activeTmpHEAD->next;
		curr->next=inactTmpHEAD;
		inactTmpHEAD=curr;
	}
	else{
		prev=curr;
		curr=curr->next;
		while(1){
			if(strcmp(curr->tname, t->tname)==0){
				prev->next=curr->next;
				curr->next=inactTmpHEAD;
				inactTmpHEAD=curr;
				return;
			}
			else{
				prev=curr;
				curr=curr->next;
			}
		}
	}
}

void removeFromActiveTempList_n(char* s){
	tmpVar_t *curr = activeTmpHEAD;
	tmpVar_t *prev;
	if(strcmp(curr->tname, s)==0){
		//tmp=curr;
		//fprintf(stderr, "prwtoIFremove\n");
		activeTmpHEAD=activeTmpHEAD->next;
		curr->next=inactTmpHEAD;
		inactTmpHEAD=curr;
	}
	else{
		prev=curr;
		curr=curr->next;
		while(1){
			if(strcmp(curr->tname, s)==0){
				prev->next=curr->next;
				curr->next=inactTmpHEAD;
				inactTmpHEAD=curr;
				return;
			}
			else{
				prev=curr;
				curr=curr->next;
			}
		}
	}
}

tmpVar_t* bringTemp(){
	tmpVar_t *result;
	if(inactTmpHEAD==NULL)
		return newTempName();
	else{
		result = inactTmpHEAD;
		inactTmpHEAD=inactTmpHEAD->next;
		addToActiveTempList(result);
		return result;
	}
}

void printTmpLists(){
	tmpVar_t *curr=activeTmpHEAD;
	fprintf(stderr, "ACTIVE: ");
	while(curr!=NULL){
		fprintf(stderr, "%s ", curr->tname);
		curr=curr->next;
	}
	fprintf(stderr, "\n");
	fprintf(stderr, "INACTIVE: ");
	curr = inactTmpHEAD;
	while(curr!=NULL){
		fprintf(stderr, "%s ", curr->tname);
		curr=curr->next;
	}
	fprintf(stderr, "\n\n");
}

void resetTemps(){
	extern int tempNameCounter;
	tmpVar_t *curr=activeTmpHEAD;
	tmpVar_t *tmp;
	while(curr!=NULL){
		tmp = curr;
		curr = curr ->next;
		fprintf(stderr, "Deleting %s\n", tmp->tname );
		free(tmp->tname);
		free(tmp);
	}
	activeTmpHEAD=NULL;
	curr = inactTmpHEAD;
	while(curr!=NULL){
		tmp = curr;
		curr = curr ->next;
		fprintf(stderr, "Deleting %s\n", tmp->tname );
		free(tmp->tname);
		free(tmp);
	}
	inactTmpHEAD=NULL;
	tempNameCounter=0;
}

void popActive(){
	tmpVar_t *temp;
	if(activeTmpHEAD!=NULL){
		removeFromActiveTempList(activeTmpHEAD);
	}
	else{
		fprintf(stderr, "cannot POPactive\n");
		return;
	}
}
/*
void printHEADS(){
	fprintf("ACTIVEHEAD: %s", activeTmpHEAD->tname);
	fprintf("NAHEAD: %s", inactTmpHEAD->tname);
}*/