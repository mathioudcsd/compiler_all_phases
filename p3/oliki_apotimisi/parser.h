/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_PARSER_H_INCLUDED
# define YY_YY_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    IF = 258,
    ELSE = 259,
    WHILE = 260,
    FOR = 261,
    FUNCTION = 262,
    RETURN = 263,
    BREAK = 264,
    CONTINUE = 265,
    AND = 266,
    NOT = 267,
    OR = 268,
    LOCAL = 269,
    TRUE = 270,
    FALSE = 271,
    NIL = 272,
    ASSIGN = 273,
    PLUS = 274,
    MINUS = 275,
    MUL = 276,
    DIV = 277,
    MOD = 278,
    EQ = 279,
    NEQ = 280,
    PP = 281,
    MM = 282,
    GREATER = 283,
    LESS = 284,
    GEQ = 285,
    LEQ = 286,
    INTEGER = 287,
    REAL = 288,
    ID = 289,
    STR = 290,
    WHITESPACE = 291,
    BRKT_O = 292,
    BRKT_C = 293,
    SBRKT_O = 294,
    SBRKT_C = 295,
    PAR_O = 296,
    PAR_C = 297,
    SEMICOL = 298,
    COMMA = 299,
    COLON = 300,
    COLON2 = 301,
    DOT = 302,
    DOT2 = 303,
    LOWER_THAN_ELSE = 304,
    UMINUS = 305
  };
#endif
/* Tokens.  */
#define IF 258
#define ELSE 259
#define WHILE 260
#define FOR 261
#define FUNCTION 262
#define RETURN 263
#define BREAK 264
#define CONTINUE 265
#define AND 266
#define NOT 267
#define OR 268
#define LOCAL 269
#define TRUE 270
#define FALSE 271
#define NIL 272
#define ASSIGN 273
#define PLUS 274
#define MINUS 275
#define MUL 276
#define DIV 277
#define MOD 278
#define EQ 279
#define NEQ 280
#define PP 281
#define MM 282
#define GREATER 283
#define LESS 284
#define GEQ 285
#define LEQ 286
#define INTEGER 287
#define REAL 288
#define ID 289
#define STR 290
#define WHITESPACE 291
#define BRKT_O 292
#define BRKT_C 293
#define SBRKT_O 294
#define SBRKT_C 295
#define PAR_O 296
#define PAR_C 297
#define SEMICOL 298
#define COMMA 299
#define COLON 300
#define COLON2 301
#define DOT 302
#define DOT2 303
#define LOWER_THAN_ELSE 304
#define UMINUS 305

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 68 "parser.y" /* yacc.c:1909  */

	int integer;
	double real;
	char* string;
	struct expr_s* expression;
	unsigned unsigned_t;

	struct SymbolTableEntry* symboltableentry;

	struct label_carrier* carrier_t;


	//enum iopcode_e iopcode_t;

#line 169 "parser.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_PARSER_H_INCLUDED  */
