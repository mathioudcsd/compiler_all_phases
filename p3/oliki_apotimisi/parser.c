/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "parser.y" /* yacc.c:339  */


/*	
 *
 *	Project Name: Lexical Analysis cs340
 *	Authors: 
 *			Fotios Mathioudakis
 *			Leonidas Anagnostou
 *			Evaggelia Athanasaki
 */






/* ========== C  CODE (includes etc) =========== */
// #include "symtable.h"
//#include "function_stack.h"
#include "quadtools.h"
#include "tmpVars.h"
#include "offset_stack.h"



	int yyerror (char* yaccProvidedMessage);
//	int alpha_yylex(void* yylval);
	int yylex(void);



	extern int yylineno;
	extern char* yytext;
	extern FILE* yyin;
	//extern unsigned int scope=0;
	extern SymbolTable* table;
	//SymbolTable* table;
	SymbolTableEntry* entry;
	unsigned int funcCounter=0;
	char* tempname;
	struct f_stack* in_function=NULL;
	struct f_stack* f_names=NULL;
	struct f_stack* breaklist=NULL;
	struct f_stack* contlist=NULL;
	int inside_function=0;
	int inside_loop=0;
	int is_func = 0;
	//int func_lv= 0;
	int id_is_lib= 0;
	unsigned int varexists=0;
	unsigned int is_var=1;
	unsigned int is_call=0;
	extern param_expr* elist_paramets;
	extern indexed_expr* indexed_paramets;
	unsigned call_method = 0;
	char *call_method_name = NULL;

	int stop_parser(){
		fprintf(stderr, "\x1b[31mcompilation failed\x1b[0m\n");
		return 1;
	}

/* ========== YACC definitions etc =========== */

#line 131 "parser.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "parser.h".  */
#ifndef YY_YY_PARSER_H_INCLUDED
# define YY_YY_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    IF = 258,
    ELSE = 259,
    WHILE = 260,
    FOR = 261,
    FUNCTION = 262,
    RETURN = 263,
    BREAK = 264,
    CONTINUE = 265,
    AND = 266,
    NOT = 267,
    OR = 268,
    LOCAL = 269,
    TRUE = 270,
    FALSE = 271,
    NIL = 272,
    ASSIGN = 273,
    PLUS = 274,
    MINUS = 275,
    MUL = 276,
    DIV = 277,
    MOD = 278,
    EQ = 279,
    NEQ = 280,
    PP = 281,
    MM = 282,
    GREATER = 283,
    LESS = 284,
    GEQ = 285,
    LEQ = 286,
    INTEGER = 287,
    REAL = 288,
    ID = 289,
    STR = 290,
    WHITESPACE = 291,
    BRKT_O = 292,
    BRKT_C = 293,
    SBRKT_O = 294,
    SBRKT_C = 295,
    PAR_O = 296,
    PAR_C = 297,
    SEMICOL = 298,
    COMMA = 299,
    COLON = 300,
    COLON2 = 301,
    DOT = 302,
    DOT2 = 303,
    LOWER_THAN_ELSE = 304,
    UMINUS = 305
  };
#endif
/* Tokens.  */
#define IF 258
#define ELSE 259
#define WHILE 260
#define FOR 261
#define FUNCTION 262
#define RETURN 263
#define BREAK 264
#define CONTINUE 265
#define AND 266
#define NOT 267
#define OR 268
#define LOCAL 269
#define TRUE 270
#define FALSE 271
#define NIL 272
#define ASSIGN 273
#define PLUS 274
#define MINUS 275
#define MUL 276
#define DIV 277
#define MOD 278
#define EQ 279
#define NEQ 280
#define PP 281
#define MM 282
#define GREATER 283
#define LESS 284
#define GEQ 285
#define LEQ 286
#define INTEGER 287
#define REAL 288
#define ID 289
#define STR 290
#define WHITESPACE 291
#define BRKT_O 292
#define BRKT_C 293
#define SBRKT_O 294
#define SBRKT_C 295
#define PAR_O 296
#define PAR_C 297
#define SEMICOL 298
#define COMMA 299
#define COLON 300
#define COLON2 301
#define DOT 302
#define DOT2 303
#define LOWER_THAN_ELSE 304
#define UMINUS 305

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 68 "parser.y" /* yacc.c:355  */

	int integer;
	double real;
	char* string;
	struct expr_s* expression;
	unsigned unsigned_t;

	struct SymbolTableEntry* symboltableentry;

	struct label_carrier* carrier_t;


	//enum iopcode_e iopcode_t;

#line 286 "parser.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_PARSER_H_INCLUDED  */

/* Copy the second part of user declarations.  */
#line 138 "parser.y" /* yacc.c:358  */
/* TOP(low) -> BOTTOM(high) */
#line 146 "parser.y" /* yacc.c:358  */
/* warning!! check the MINUS CASE */

#line 307 "parser.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  65
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   598

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  51
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  48
/* YYNRULES -- Number of rules.  */
#define YYNRULES  107
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  194

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   305

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   183,   183,   184,   187,   188,   191,   192,   193,   194,
     195,   196,   208,   218,   219,   220,   225,   230,   234,   238,
     243,   251,   260,   265,   270,   275,   280,   285,   291,   304,
     318,   325,   331,   348,   361,   389,   420,   448,   479,   485,
     521,   525,   526,   529,   534,   539,   616,   655,   665,   668,
     678,   684,   688,   695,   695,   722,   732,   735,   740,   747,
     747,   755,   778,   803,   806,   807,   810,   815,   816,   816,
     824,   887,   899,   824,   926,   937,   938,   926,   988,   989,
     990,   991,   992,   993,   997,   997,  1013,  1017,  1017,  1041,
    1045,  1054,  1062,  1065,  1072,  1077,  1088,  1102,  1106,  1113,
    1123,  1134,  1146,  1154,  1191,  1194,  1197,  1200
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "IF", "ELSE", "WHILE", "FOR", "FUNCTION",
  "RETURN", "BREAK", "CONTINUE", "AND", "NOT", "OR", "LOCAL", "TRUE",
  "FALSE", "NIL", "ASSIGN", "PLUS", "MINUS", "MUL", "DIV", "MOD", "EQ",
  "NEQ", "PP", "MM", "GREATER", "LESS", "GEQ", "LEQ", "INTEGER", "REAL",
  "ID", "STR", "WHITESPACE", "BRKT_O", "BRKT_C", "SBRKT_O", "SBRKT_C",
  "PAR_O", "PAR_C", "SEMICOL", "COMMA", "COLON", "COLON2", "DOT", "DOT2",
  "LOWER_THAN_ELSE", "UMINUS", "$accept", "program", "programcont", "stmt",
  "expr", "term", "assignexpr", "primary", "lvalue", "member", "call",
  "$@1", "callsuffix", "normcall", "methodcall", "$@2", "tablemake",
  "indexed", "indexed2", "indexedelem", "block", "$@3", "funcdef", "$@4",
  "$@5", "$@6", "$@7", "$@8", "$@9", "const", "idlist", "$@10", "idlist2",
  "$@11", "ifprefix", "elseprefix", "ifstmt", "whilestart", "whilecont",
  "whilestmt", "FOR_M", "FOR_JMP", "forprefix", "formiddle", "forstmt",
  "returnstmt", "elist", "elist2", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305
};
# endif

#define YYPACT_NINF -158

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-158)))

#define YYTABLE_NINF -54

#define yytable_value_is_error(Yytable_value) \
  (!!((Yytable_value) == (-54)))

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     158,   -36,  -158,   -32,   -24,   219,   -17,   -10,   280,   -23,
    -158,  -158,  -158,   280,   -12,   -12,  -158,  -158,  -158,  -158,
     -11,   191,  -158,     1,    37,   158,  -158,   336,  -158,  -158,
    -158,    12,  -158,   -35,  -158,  -158,  -158,   158,  -158,     4,
    -158,  -158,   158,  -158,  -158,   280,   280,  -158,  -158,  -158,
     361,  -158,  -158,  -158,  -158,  -158,    45,   -27,   -35,   -27,
    -158,   158,   411,    19,  -158,  -158,  -158,   280,   280,   280,
     280,   280,   280,   280,   280,   280,   280,   280,   280,   280,
    -158,   280,  -158,  -158,   252,    44,   -16,  -158,   280,   280,
      52,    84,   280,   158,   280,  -158,   435,   125,    49,    54,
      59,  -158,    60,    91,  -158,    63,   554,   541,    21,    21,
    -158,  -158,  -158,   567,   567,    27,    27,    27,    27,   526,
     280,    69,    68,    73,  -158,   280,    80,  -158,  -158,  -158,
     483,    77,  -158,  -158,   158,   459,  -158,    79,  -158,  -158,
     280,  -158,  -158,  -158,  -158,  -158,    63,  -158,   280,   309,
    -158,    85,  -158,  -158,    88,  -158,  -158,  -158,  -158,  -158,
    -158,   125,   280,    59,    83,    89,    97,   280,    68,  -158,
      92,  -158,  -158,   386,  -158,   106,  -158,  -158,  -158,   505,
    -158,   280,  -158,    99,  -158,   105,  -158,   109,   105,    83,
    -158,  -158,  -158,  -158
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       3,     0,    94,     0,     0,     0,     0,     0,     0,     0,
      82,    83,    81,     0,     0,     0,    78,    79,    45,    80,
      68,     0,    15,     0,     0,     2,     4,     0,    30,    16,
      38,    40,    48,    41,    13,    14,    44,     0,     7,     0,
       8,    98,     0,     9,    10,     0,   105,    70,    74,   102,
       0,    11,    12,    33,    46,    32,     0,    34,     0,    36,
      67,     0,     0,     0,    47,     1,     5,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       6,     0,    35,    37,   105,     0,     0,    42,     0,   105,
       0,    92,     0,     0,   105,    98,     0,   107,     0,     0,
      86,   103,     0,     0,    31,    43,    28,    29,    17,    18,
      19,    20,    21,    26,    27,    22,    23,    24,    25,    39,
       0,     0,    65,     0,    49,   105,     0,    54,    56,    57,
       0,     0,    50,    91,     0,     0,    96,     0,   101,    90,
       0,   104,    97,    71,    84,    75,     0,    69,   105,     0,
      62,     0,    63,    61,     0,    59,    51,    52,    93,    95,
      98,   107,     0,    86,    89,     0,     0,     0,    65,    58,
       0,   100,   106,     0,    72,     0,    85,    76,    55,     0,
      64,   105,    99,     0,    87,     0,    66,     0,     0,    89,
      77,    60,    73,    88
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -158,  -158,    98,   -18,    -5,  -158,  -158,  -158,    67,  -158,
      76,  -158,  -158,  -158,  -158,  -158,  -158,  -158,    -8,     7,
    -157,  -158,   -20,  -158,  -158,  -158,  -158,  -158,  -158,  -158,
       8,  -158,   -37,  -158,  -158,  -158,  -158,  -158,  -158,  -158,
    -158,   -80,  -158,  -158,  -158,  -158,   -71,    16
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    86,   127,   128,   129,   170,    87,   121,   152,   122,
      34,    61,    35,    99,   163,   183,   100,   165,   185,    36,
     145,   164,   176,   189,    37,   134,    38,    39,    93,    40,
     162,    94,    41,    42,    43,    44,    98,   141
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      50,    63,     9,    53,    88,    45,    89,    66,    55,    46,
      47,    54,    90,   123,   -53,   138,    62,    48,   131,    91,
      85,   -53,    18,   137,    95,   125,    51,    60,   190,    56,
      81,   192,   126,    52,    23,    64,   102,    65,    82,    83,
      96,    97,    71,    72,    73,    92,    69,    70,    71,    72,
      73,    84,     4,   -53,   154,   -54,   -54,   -54,   -54,    85,
     -53,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   136,   119,   166,   124,    97,
     171,    57,    59,   130,    97,    66,   132,   135,   133,    97,
      58,    58,   142,   144,     1,   143,     2,     3,     4,     5,
       6,     7,   146,     8,   148,     9,    10,    11,    12,   150,
     187,    13,   151,   153,   155,   149,   158,    14,    15,   157,
      97,   160,   120,    16,    17,    18,    19,   175,    20,   147,
     169,   177,    21,   181,    22,   161,    67,    23,    68,   178,
     184,   188,    20,    97,    69,    70,    71,    72,    73,    74,
      75,   191,   193,    76,    77,    78,    79,   173,   168,   103,
     180,     1,   179,     2,     3,     4,     5,     6,     7,   140,
       8,   174,     9,    10,    11,    12,    97,   172,    13,     0,
       0,     0,     0,     0,    14,    15,     0,     0,     0,     0,
      16,    17,    18,    19,     0,    20,     0,     0,     4,    21,
       0,    22,     0,     8,    23,     9,    10,    11,    12,     0,
       0,    13,     0,     0,     0,     0,     0,    14,    15,     0,
       0,     0,     0,    16,    17,    18,    19,     0,     0,     0,
       0,     8,    21,     9,    10,    11,    12,    23,     0,    13,
       0,     0,     0,     0,     0,    14,    15,     0,     0,     0,
       0,    16,    17,    18,    19,     0,     0,     0,     0,     0,
      21,     0,    49,     0,     8,    23,     9,    10,    11,    12,
       0,     0,    13,     0,     0,     0,     0,     0,    14,    15,
       0,     0,     0,     0,    16,    17,    18,    19,     0,   120,
       0,     0,     8,    21,     9,    10,    11,    12,    23,     0,
      13,     0,     0,     0,     0,     0,    14,    15,     0,     0,
       0,     0,    16,    17,    18,    19,     0,     0,     0,     0,
      67,    21,    68,     0,     0,     0,    23,     0,    69,    70,
      71,    72,    73,    74,    75,     0,     0,    76,    77,    78,
      79,     0,     0,     0,     0,     0,     0,    67,     0,    68,
       0,     0,     0,     0,   167,    69,    70,    71,    72,    73,
      74,    75,     0,     0,    76,    77,    78,    79,     0,     0,
       0,     0,    67,     0,    68,     0,     0,     0,     0,    80,
      69,    70,    71,    72,    73,    74,    75,     0,     0,    76,
      77,    78,    79,     0,     0,     0,     0,    67,     0,    68,
       0,     0,     0,     0,   101,    69,    70,    71,    72,    73,
      74,    75,     0,     0,    76,    77,    78,    79,     0,     0,
       0,     0,    67,     0,    68,     0,     0,     0,     0,   182,
      69,    70,    71,    72,    73,    74,    75,     0,     0,    76,
      77,    78,    79,     0,     0,     0,    67,     0,    68,     0,
       0,     0,     0,   104,    69,    70,    71,    72,    73,    74,
      75,     0,     0,    76,    77,    78,    79,     0,     0,     0,
      67,     0,    68,     0,     0,     0,     0,   139,    69,    70,
      71,    72,    73,    74,    75,     0,     0,    76,    77,    78,
      79,     0,     0,     0,    67,     0,    68,     0,     0,     0,
       0,   159,    69,    70,    71,    72,    73,    74,    75,     0,
       0,    76,    77,    78,    79,     0,    67,     0,    68,     0,
       0,     0,     0,   156,    69,    70,    71,    72,    73,    74,
      75,     0,     0,    76,    77,    78,    79,    67,     0,    68,
       0,     0,     0,   186,     0,    69,    70,    71,    72,    73,
      74,    75,    67,     0,    76,    77,    78,    79,     0,     0,
      69,    70,    71,    72,    73,    74,    75,     0,     0,    76,
      77,    78,    79,    69,    70,    71,    72,    73,    74,    75,
       0,     0,    76,    77,    78,    79,    69,    70,    71,    72,
      73,   -54,   -54,     0,     0,    76,    77,    78,    79
};

static const yytype_int16 yycheck[] =
{
       5,    21,    14,     8,    39,    41,    41,    25,    13,    41,
      34,    34,    47,    84,    41,    95,    21,    41,    89,    37,
      47,    48,    34,    94,    42,    41,    43,    38,   185,    41,
      18,   188,    48,    43,    46,    34,    56,     0,    26,    27,
      45,    46,    21,    22,    23,    41,    19,    20,    21,    22,
      23,    39,     7,    41,   125,    28,    29,    30,    31,    47,
      48,    42,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    93,    81,   148,    34,    84,
     160,    14,    15,    88,    89,   103,    34,    92,     4,    94,
      14,    15,    43,    34,     3,    41,     5,     6,     7,     8,
       9,    10,    42,    12,    41,    14,    15,    16,    17,    40,
     181,    20,    44,    40,    34,   120,   134,    26,    27,    42,
     125,    42,    37,    32,    33,    34,    35,    44,    37,    38,
      42,    42,    41,    41,    43,   140,    11,    46,    13,    42,
      34,    42,    37,   148,    19,    20,    21,    22,    23,    24,
      25,    42,   189,    28,    29,    30,    31,   162,   151,    61,
     168,     3,   167,     5,     6,     7,     8,     9,    10,    44,
      12,   163,    14,    15,    16,    17,   181,   161,    20,    -1,
      -1,    -1,    -1,    -1,    26,    27,    -1,    -1,    -1,    -1,
      32,    33,    34,    35,    -1,    37,    -1,    -1,     7,    41,
      -1,    43,    -1,    12,    46,    14,    15,    16,    17,    -1,
      -1,    20,    -1,    -1,    -1,    -1,    -1,    26,    27,    -1,
      -1,    -1,    -1,    32,    33,    34,    35,    -1,    -1,    -1,
      -1,    12,    41,    14,    15,    16,    17,    46,    -1,    20,
      -1,    -1,    -1,    -1,    -1,    26,    27,    -1,    -1,    -1,
      -1,    32,    33,    34,    35,    -1,    -1,    -1,    -1,    -1,
      41,    -1,    43,    -1,    12,    46,    14,    15,    16,    17,
      -1,    -1,    20,    -1,    -1,    -1,    -1,    -1,    26,    27,
      -1,    -1,    -1,    -1,    32,    33,    34,    35,    -1,    37,
      -1,    -1,    12,    41,    14,    15,    16,    17,    46,    -1,
      20,    -1,    -1,    -1,    -1,    -1,    26,    27,    -1,    -1,
      -1,    -1,    32,    33,    34,    35,    -1,    -1,    -1,    -1,
      11,    41,    13,    -1,    -1,    -1,    46,    -1,    19,    20,
      21,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    -1,    -1,    -1,    -1,    -1,    -1,    11,    -1,    13,
      -1,    -1,    -1,    -1,    45,    19,    20,    21,    22,    23,
      24,    25,    -1,    -1,    28,    29,    30,    31,    -1,    -1,
      -1,    -1,    11,    -1,    13,    -1,    -1,    -1,    -1,    43,
      19,    20,    21,    22,    23,    24,    25,    -1,    -1,    28,
      29,    30,    31,    -1,    -1,    -1,    -1,    11,    -1,    13,
      -1,    -1,    -1,    -1,    43,    19,    20,    21,    22,    23,
      24,    25,    -1,    -1,    28,    29,    30,    31,    -1,    -1,
      -1,    -1,    11,    -1,    13,    -1,    -1,    -1,    -1,    43,
      19,    20,    21,    22,    23,    24,    25,    -1,    -1,    28,
      29,    30,    31,    -1,    -1,    -1,    11,    -1,    13,    -1,
      -1,    -1,    -1,    42,    19,    20,    21,    22,    23,    24,
      25,    -1,    -1,    28,    29,    30,    31,    -1,    -1,    -1,
      11,    -1,    13,    -1,    -1,    -1,    -1,    42,    19,    20,
      21,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    -1,    -1,    -1,    11,    -1,    13,    -1,    -1,    -1,
      -1,    42,    19,    20,    21,    22,    23,    24,    25,    -1,
      -1,    28,    29,    30,    31,    -1,    11,    -1,    13,    -1,
      -1,    -1,    -1,    40,    19,    20,    21,    22,    23,    24,
      25,    -1,    -1,    28,    29,    30,    31,    11,    -1,    13,
      -1,    -1,    -1,    38,    -1,    19,    20,    21,    22,    23,
      24,    25,    11,    -1,    28,    29,    30,    31,    -1,    -1,
      19,    20,    21,    22,    23,    24,    25,    -1,    -1,    28,
      29,    30,    31,    19,    20,    21,    22,    23,    24,    25,
      -1,    -1,    28,    29,    30,    31,    19,    20,    21,    22,
      23,    24,    25,    -1,    -1,    28,    29,    30,    31
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     5,     6,     7,     8,     9,    10,    12,    14,
      15,    16,    17,    20,    26,    27,    32,    33,    34,    35,
      37,    41,    43,    46,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    71,    73,    80,    85,    87,    88,
      90,    93,    94,    95,    96,    41,    41,    34,    41,    43,
      55,    43,    43,    55,    34,    55,    41,    59,    61,    59,
      38,    72,    55,    73,    34,     0,    54,    11,    13,    19,
      20,    21,    22,    23,    24,    25,    28,    29,    30,    31,
      43,    18,    26,    27,    39,    47,    62,    67,    39,    41,
      47,    54,    41,    89,    92,    54,    55,    55,    97,    74,
      77,    43,    73,    53,    42,    42,    55,    55,    55,    55,
      55,    55,    55,    55,    55,    55,    55,    55,    55,    55,
      37,    68,    70,    97,    34,    41,    48,    63,    64,    65,
      55,    97,    34,     4,    86,    55,    54,    97,    92,    42,
      44,    98,    43,    41,    34,    81,    42,    38,    41,    55,
      40,    44,    69,    40,    97,    34,    40,    42,    54,    42,
      42,    55,    91,    75,    82,    78,    97,    45,    70,    42,
      66,    92,    98,    55,    81,    44,    83,    42,    42,    55,
      69,    41,    43,    76,    34,    79,    38,    97,    42,    84,
      71,    42,    71,    83
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    51,    52,    52,    53,    53,    54,    54,    54,    54,
      54,    54,    54,    54,    54,    54,    55,    55,    55,    55,
      55,    55,    55,    55,    55,    55,    55,    55,    55,    55,
      55,    56,    56,    56,    56,    56,    56,    56,    56,    57,
      58,    58,    58,    58,    58,    59,    59,    59,    59,    60,
      60,    60,    61,    62,    61,    61,    63,    63,    64,    66,
      65,    67,    67,    68,    69,    69,    70,    71,    72,    71,
      74,    75,    76,    73,    77,    78,    79,    73,    80,    80,
      80,    80,    80,    80,    82,    81,    81,    84,    83,    83,
      85,    86,    87,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    96,    97,    97,    98,    98
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     0,     1,     2,     2,     1,     1,     1,
       1,     2,     2,     1,     1,     1,     1,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       1,     3,     2,     2,     2,     2,     2,     2,     1,     3,
       1,     1,     2,     3,     1,     1,     2,     2,     1,     3,
       3,     4,     4,     0,     3,     6,     1,     1,     3,     0,
       6,     3,     3,     2,     3,     0,     5,     2,     0,     4,
       0,     0,     0,     9,     0,     0,     0,     8,     1,     1,
       1,     1,     1,     1,     0,     3,     0,     0,     4,     0,
       4,     1,     2,     4,     1,     3,     3,     0,     0,     7,
       5,     3,     2,     3,     2,     0,     3,     0
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 183 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"program -> programcont\n");*/}
#line 1607 "parser.c" /* yacc.c:1646  */
    break;

  case 3:
#line 184 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"program -> \n");*/}
#line 1613 "parser.c" /* yacc.c:1646  */
    break;

  case 4:
#line 187 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"programcont -> stmt\n");*/}
#line 1619 "parser.c" /* yacc.c:1646  */
    break;

  case 5:
#line 188 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"programcont -> programcont stmt\n");*/}
#line 1625 "parser.c" /* yacc.c:1646  */
    break;

  case 6:
#line 191 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"stmt -> expr;\n");*/}
#line 1631 "parser.c" /* yacc.c:1646  */
    break;

  case 7:
#line 192 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"stmt -> ifstmt\n");*/}
#line 1637 "parser.c" /* yacc.c:1646  */
    break;

  case 8:
#line 193 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"stmt -> whilestmt\n");*/}
#line 1643 "parser.c" /* yacc.c:1646  */
    break;

  case 9:
#line 194 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"stmt -> forstmt\n");*/}
#line 1649 "parser.c" /* yacc.c:1646  */
    break;

  case 10:
#line 195 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"stmt -> returnstmt\n");*/}
#line 1655 "parser.c" /* yacc.c:1646  */
    break;

  case 11:
#line 196 "parser.y" /* yacc.c:1646  */
    {	/*fprintf(stdout,"stmt -> break;\n");*/
																				if(!inside_loop){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): Use of \'break\' while not in a loop\n",yylineno);
																					return stop_parser();
																				}

																				incrQuadNo();
																				emitJump(0, yylineno);
																				breaklist= push_bc(breaklist, inside_loop, getQuadNo());

																			}
#line 1671 "parser.c" /* yacc.c:1646  */
    break;

  case 12:
#line 208 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"stmt -> continue;\n");
																				if(!inside_loop){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): Use of \'continue\' while not in a loop\n",yylineno);
																					return stop_parser();
																				}

																				incrQuadNo();
																				emitJump(0, yylineno);
																				contlist= push_bc(contlist, inside_loop, getQuadNo());
																			}
#line 1686 "parser.c" /* yacc.c:1646  */
    break;

  case 13:
#line 218 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"stmt -> block\n");*/}
#line 1692 "parser.c" /* yacc.c:1646  */
    break;

  case 14:
#line 219 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"stmt -> funcdef\n");*/}
#line 1698 "parser.c" /* yacc.c:1646  */
    break;

  case 15:
#line 220 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"stmt -> \n");*/}
#line 1704 "parser.c" /* yacc.c:1646  */
    break;

  case 16:
#line 225 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"expr -> assignexpr\n");*/
																				 
																			}
#line 1712 "parser.c" /* yacc.c:1646  */
    break;

  case 17:
#line 230 "parser.y" /* yacc.c:1646  */
    {	/*fprintf(stdout,"expr -> expr arithop expr\n");*/
																										
																					(yyval.expression) = emitArithExpr(add, (yyvsp[-2].expression), (yyvsp[0].expression), scope, yylineno); 
																			}
#line 1721 "parser.c" /* yacc.c:1646  */
    break;

  case 18:
#line 234 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"expr -> expr arithop expr\n");
																										
																					(yyval.expression) = emitArithExpr(sub, (yyvsp[-2].expression), (yyvsp[0].expression), scope, yylineno); 
																			}
#line 1730 "parser.c" /* yacc.c:1646  */
    break;

  case 19:
#line 238 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"expr -> expr arithop expr\n");
																				
																										
																					(yyval.expression) = emitArithExpr(mul, (yyvsp[-2].expression), (yyvsp[0].expression), scope, yylineno); 
																			}
#line 1740 "parser.c" /* yacc.c:1646  */
    break;

  case 20:
#line 243 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"expr -> expr arithop expr\n");
																				if((yyvsp[0].expression)->type==constnum_e && (yyvsp[0].expression)->numConst==0 ){
																					fprintf(stderr,"\x1b[31mRUNTIME ERROR\x1b[0m line(%d): Cannot Divide by Zero!\n",yylineno);
																					return stop_parser();
																				}	
																				else						
																					(yyval.expression) = emitArithExpr(Div, (yyvsp[-2].expression), (yyvsp[0].expression), scope, yylineno); 
																			}
#line 1753 "parser.c" /* yacc.c:1646  */
    break;

  case 21:
#line 251 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"expr -> expr arithop expr\n");
																				if((yyvsp[0].expression)->type==constnum_e && (yyvsp[0].expression)->numConst==0 ){
																					fprintf(stderr,"\x1b[31mRUNTIME ERROR\x1b[0m line(%d): Cannot Divide by Zero!\n",yylineno);
																					return stop_parser();
																				}	
																				else						
																					(yyval.expression) = emitArithExpr(mod, (yyvsp[-2].expression), (yyvsp[0].expression), scope, yylineno); 
																			}
#line 1766 "parser.c" /* yacc.c:1646  */
    break;

  case 22:
#line 260 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"expr -> expr relop expr\n");
																				incrQuadNo();
																				(yyval.expression) = emitLogicOp(if_greater, (yyvsp[-2].expression), (yyvsp[0].expression), scope, yylineno, getQuadNo());
																			}
#line 1775 "parser.c" /* yacc.c:1646  */
    break;

  case 23:
#line 265 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"expr -> expr relop expr\n");
																				incrQuadNo();
																				(yyval.expression) = emitLogicOp(if_less, (yyvsp[-2].expression), (yyvsp[0].expression), scope, yylineno, getQuadNo());
																			}
#line 1784 "parser.c" /* yacc.c:1646  */
    break;

  case 24:
#line 270 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"expr -> expr relop expr\n");
																				incrQuadNo();
																				(yyval.expression) = emitLogicOp(if_greatereq, (yyvsp[-2].expression), (yyvsp[0].expression), scope, yylineno, getQuadNo());
																			}
#line 1793 "parser.c" /* yacc.c:1646  */
    break;

  case 25:
#line 275 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"expr -> expr relop expr\n");
																				incrQuadNo();
																				(yyval.expression) = emitLogicOp(if_lesseq, (yyvsp[-2].expression), (yyvsp[0].expression), scope, yylineno, getQuadNo());
																			}
#line 1802 "parser.c" /* yacc.c:1646  */
    break;

  case 26:
#line 280 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"expr -> expr relop expr\n");
																				incrQuadNo();
																				(yyval.expression) = emitLogicOp(if_eq, (yyvsp[-2].expression), (yyvsp[0].expression), scope, yylineno, getQuadNo());
																			}
#line 1811 "parser.c" /* yacc.c:1646  */
    break;

  case 27:
#line 285 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"expr -> expr relop expr\n");
																				incrQuadNo();
																				(yyval.expression) = emitLogicOp(if_noteq, (yyvsp[-2].expression), (yyvsp[0].expression), scope, yylineno, getQuadNo());
																			}
#line 1820 "parser.c" /* yacc.c:1646  */
    break;

  case 28:
#line 291 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"expr -> expr and expr\n");
																				expr* exp =checkBoolExp((yyvsp[-2].expression));
																				expr* exp2 =checkBoolExp((yyvsp[0].expression));
																				if( exp != NULL && exp2 != NULL){
																					if(exp->boolConst== 'T' && exp2->boolConst== 'T')
																						(yyval.expression) = emitBoolExpr(and, (yyvsp[-2].expression), (yyvsp[0].expression), scope, yylineno, 'T');
																					else
																						(yyval.expression) = emitBoolExpr(and, (yyvsp[-2].expression), (yyvsp[0].expression), scope, yylineno, 'F');
																				}else{
																					fprintf(stderr,"\x1b[31mRUNTIME ERROR\x1b[0m line(%d): Cannot cast to boolean type!\n",yylineno);
																					return stop_parser();
																				}
																			}
#line 1838 "parser.c" /* yacc.c:1646  */
    break;

  case 29:
#line 304 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"expr -> expr or expr\n");
																				expr* exp =checkBoolExp((yyvsp[-2].expression));
																				expr* exp2 =checkBoolExp((yyvsp[0].expression));
																				if( exp != NULL && exp2 != NULL){
																					if(exp->boolConst== 'F' && exp2->boolConst== 'F')
																						(yyval.expression) = emitBoolExpr(or, (yyvsp[-2].expression), (yyvsp[0].expression), scope, yylineno, 'F');
																					else
																						(yyval.expression) = emitBoolExpr(or, (yyvsp[-2].expression), (yyvsp[0].expression), scope, yylineno, 'T');
																				}else{
																					fprintf(stderr,"\x1b[31mRUNTIME ERROR\x1b[0m line(%d): Cannot cast to boolean type!\n",yylineno);
																					return stop_parser();
																				}
																			}
#line 1856 "parser.c" /* yacc.c:1646  */
    break;

  case 30:
#line 318 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"expr -> term\n");
																				(yyval.expression) = (yyvsp[0].expression);
																			}
#line 1864 "parser.c" /* yacc.c:1646  */
    break;

  case 31:
#line 325 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"term -> (expr)\n");
																				//$$ = emitArithExpr(mod, $1, $3, scope, yylineno);
																				//myEmit(add, $2, arg2, $2, getQuadNo() , yylineno);
																				
																				(yyval.expression) = (yyvsp[-1].expression);
																			}
#line 1875 "parser.c" /* yacc.c:1646  */
    break;

  case 32:
#line 331 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"term -> -expr\n");

																				if((yyvsp[0].expression)->type == arithexpr_e || (yyvsp[0].expression)->type == constnum_e || (yyvsp[0].expression)->type == var_e ){
																					(yyval.expression) = emitArithExpr(uminus, (yyvsp[0].expression), NULL, scope, yylineno);
																				}
																				else{
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): cannot have \n",yylineno );
																					return stop_parser();
																				}
																				// if($2->type == arithexpr_e || $2->type == constnum_e || $2->type == var_e ){
																				// 	incrQuadNo();
																				// 	myEmit(uminus, $2, NULL , $2 , getQuadNo(), yylineno);

																				// 	$$ = $2;
																				// }       
																				//error?
																			}
#line 1897 "parser.c" /* yacc.c:1646  */
    break;

  case 33:
#line 348 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"term -> not expr\n");
																				expr* exp =checkBoolExp((yyvsp[0].expression));
																				if( exp != NULL ){
																					if(exp->boolConst == 'T')
																						(yyval.expression)=emitBoolExpr(not, (yyvsp[0].expression), NULL, scope, yylineno, 'F');
																					else
																						(yyval.expression)=emitBoolExpr(not, (yyvsp[0].expression), NULL, scope, yylineno, 'T');
																				}else{
																					fprintf(stderr,"\x1b[31mRUNTIME ERROR\x1b[0m line(%d): Cannot cast to boolean type!\n",yylineno);
																					return stop_parser();
																				}
																				
																			}
#line 1915 "parser.c" /* yacc.c:1646  */
    break;

  case 34:
#line 361 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"term -> ++lvalue\n");
																				if(is_func){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																					return stop_parser();
																				}
																				expr* tmp = newexpr(arithexpr_e);
																				tmp->sym= CreateNode(bringTemp()->tname, scope, yylineno, LOC); //tmpVar as LOC
																				tmp->sym->sym_type = var_s;
																				tmp->sym->space=currScopeSpace();
																				tmp->sym->offset=currScopeOffset();
																				incCurrScopeOffset();
																				Insert(table, table->scopeHead,tmp->sym);
																				if((yyvsp[0].expression)->type == tableitem_e){
																					tmp = emit_ifTableItem((yyvsp[0].expression), scope, yylineno);
																					incrQuadNo();
																					myEmit(add, tmp, newexpr_constnum(1), tmp, getQuadNo(), yylineno);
																					incrQuadNo();
																					myEmit(tablesetelement,(yyvsp[0].expression),(yyvsp[0].expression)->index, tmp,getQuadNo(), yylineno);
																				}else{
																					incrQuadNo();
																					myEmit(add, (yyvsp[0].expression), newexpr_constnum(1), (yyvsp[0].expression), getQuadNo() , yylineno);

																					incrQuadNo();
																					myEmit(assign, (yyvsp[0].expression), NULL, tmp, getQuadNo() , yylineno);
																				}
																				(yyval.expression) = tmp; //mallon ksana

																			}
#line 1948 "parser.c" /* yacc.c:1646  */
    break;

  case 35:
#line 389 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"term -> lvalue++\n");
																				if(is_func){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																					return stop_parser();
																				}

																				expr* tmp = newexpr(var_e);
																				tmp->sym= CreateNode(bringTemp()->tname, scope, yylineno, LOC); //tmpVar as LOC
																				tmp->sym->sym_type = var_s;
																				tmp->sym->space=currScopeSpace();
																				tmp->sym->offset=currScopeOffset();
																				incCurrScopeOffset();
																				Insert(table, table->scopeHead,tmp->sym);
																				if((yyvsp[-1].expression)->type == tableitem_e){
																					expr *value = emit_ifTableItem((yyvsp[-1].expression), scope, yylineno);
																					incrQuadNo();	
																					myEmit(assign, value, NULL ,tmp, getQuadNo(), yylineno);
																					incrQuadNo();
																					myEmit(add, value, newexpr_constnum(1), value, getQuadNo(), yylineno);
																					incrQuadNo();
																					myEmit(tablesetelement,(yyvsp[-1].expression),(yyvsp[-1].expression)->index, value,getQuadNo(), yylineno);
																				}else{
																					incrQuadNo();
																					myEmit(assign, (yyvsp[-1].expression), NULL, tmp, getQuadNo() , yylineno);
																					
																					incrQuadNo();
																					myEmit(add,(yyvsp[-1].expression), newexpr_constnum(1), (yyvsp[-1].expression), getQuadNo() ,yylineno);
																				}
																				(yyval.expression)= tmp; //mallon ayto edw

																			}
#line 1984 "parser.c" /* yacc.c:1646  */
    break;

  case 36:
#line 420 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"term -> --lvalue\n");
																				if(is_func){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																					return stop_parser();
																				}
																				expr* tmp = newexpr(arithexpr_e);
																				tmp->sym= CreateNode(bringTemp()->tname, scope, yylineno, LOC); //tmpVar as LOC
																				tmp->sym->sym_type = var_s;
																				tmp->sym->space=currScopeSpace();
																				tmp->sym->offset=currScopeOffset();
																				incCurrScopeOffset();
																				Insert(table, table->scopeHead,tmp->sym);
																				if((yyvsp[0].expression)->type == tableitem_e){
																					tmp = emit_ifTableItem((yyvsp[0].expression), scope, yylineno);
																					incrQuadNo();
																					myEmit(sub, tmp, newexpr_constnum(1), tmp, getQuadNo(), yylineno);
																					incrQuadNo();
																					myEmit(tablesetelement,(yyvsp[0].expression),(yyvsp[0].expression)->index, tmp,getQuadNo(), yylineno);
																				}else{
																					incrQuadNo();
																					myEmit(sub, (yyvsp[0].expression), newexpr_constnum(1), (yyvsp[0].expression), getQuadNo() , yylineno);

																					incrQuadNo();
																					myEmit(assign, (yyvsp[0].expression), NULL, tmp, getQuadNo() , yylineno);
																				}
																				(yyval.expression) = tmp; //mallon ksana

																			}
#line 2017 "parser.c" /* yacc.c:1646  */
    break;

  case 37:
#line 448 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"term -> lvalue--\n");
																				if(is_func){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																					return stop_parser();
																				}

																				expr* tmp = newexpr(var_e);
																				tmp->sym= CreateNode(bringTemp()->tname, scope, yylineno, LOC); //tmpVar as LOC
																				tmp->sym->sym_type = var_s;
																				tmp->sym->space=currScopeSpace();
																				tmp->sym->offset=currScopeOffset();
																				incCurrScopeOffset();
																				Insert(table, table->scopeHead,tmp->sym);
																				if((yyvsp[-1].expression)->type == tableitem_e){
																					expr *value = emit_ifTableItem((yyvsp[-1].expression), scope, yylineno);
																					incrQuadNo();	
																					myEmit(assign, value, NULL ,tmp, getQuadNo(), yylineno);
																					incrQuadNo();
																					myEmit(sub, value, newexpr_constnum(1), value, getQuadNo(), yylineno);
																					incrQuadNo();
																					myEmit(tablesetelement,(yyvsp[-1].expression),(yyvsp[-1].expression)->index, value,getQuadNo(), yylineno);
																				}else{
																					incrQuadNo();
																					myEmit(assign, (yyvsp[-1].expression), NULL, tmp, getQuadNo() , yylineno);
																					
																					incrQuadNo();
																					myEmit(sub,(yyvsp[-1].expression), newexpr_constnum(1), (yyvsp[-1].expression), getQuadNo() ,yylineno);
																				}
																				(yyval.expression)= tmp; //mallon ayto edw

																			}
#line 2053 "parser.c" /* yacc.c:1646  */
    break;

  case 38:
#line 479 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"term -> primary\n");
																				(yyval.expression) = (yyvsp[0].expression);
																			}
#line 2061 "parser.c" /* yacc.c:1646  */
    break;

  case 39:
#line 485 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"assignexpr -> lvalue = expr\n");
																				expr *exp = malloc(sizeof(expr));
																				if((yyvsp[-2].expression)->type==programfunc_e || (yyvsp[-2].expression)->type==libraryfunc_e){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																					return stop_parser();
																				}else{
																					if((yyvsp[-2].expression)->type == tableitem_e){
																						incrQuadNo();
																						myEmit(tablesetelement,(yyvsp[-2].expression), (yyvsp[-2].expression)->index, (yyvsp[0].expression), getQuadNo() ,yylineno);
																						exp = emit_ifTableItem((yyvsp[-2].expression), scope, yylineno); //h incrQuadNo ginetai eswterika
																						exp->type = assignexpr_e;
																					}else{

																						incrQuadNo();
																						myEmit(assign,(yyvsp[0].expression), NULL, (yyvsp[-2].expression), getQuadNo() ,yylineno);
																						exp = newexpr(assignexpr_e);
																						exp->sym =  CreateNode(bringTemp()->tname, scope, yylineno, LOC);
																						exp->sym->sym_type = var_s;
																						exp->sym->space=currScopeSpace();
																						exp->sym->offset=currScopeOffset();
																						incCurrScopeOffset();
																						Insert(table, table->scopeHead,exp->sym);
																						incrQuadNo();
																						myEmit(assign,(yyvsp[-2].expression), NULL, exp, getQuadNo() ,yylineno);


																					//fprintf(stderr, "doulepse to emit\n");
																					}
																				}
																				//resetTemps();
																				// char *c = $3->sym->name;
																				// removeFromActiveTempList_n(c);
																				(yyval.expression) = exp;
																			}
#line 2100 "parser.c" /* yacc.c:1646  */
    break;

  case 40:
#line 521 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"primary -> lvalue\n");
																				(yyval.expression) = emit_ifTableItem((yyvsp[0].expression), scope, yylineno);
																				//$$ = $1; //ayto edw antikatastathike logw ton members, ginetai eswterika
																			}
#line 2109 "parser.c" /* yacc.c:1646  */
    break;

  case 41:
#line 525 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"primary -> call\n"); $$ = $1;*/}
#line 2115 "parser.c" /* yacc.c:1646  */
    break;

  case 42:
#line 526 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"primary -> tablemake\n");
																				(yyval.expression)=(yyvsp[-1].expression);
																			}
#line 2123 "parser.c" /* yacc.c:1646  */
    break;

  case 43:
#line 529 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"primary -> (funcdef)\n");
																				expr* exp = newexpr(programfunc_e);
																				exp->sym= (yyvsp[-1].symboltableentry); //tmpVar as LOC
																				(yyval.expression) = exp;
																			}
#line 2133 "parser.c" /* yacc.c:1646  */
    break;

  case 44:
#line 534 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"primary -> const\n");
																				(yyval.expression) = (yyvsp[0].expression);
																			}
#line 2141 "parser.c" /* yacc.c:1646  */
    break;

  case 45:
#line 539 "parser.y" /* yacc.c:1646  */
    {	
																				varexists=0;
																				is_var=1;
																				unsigned int tempscope=scope;
																				id_is_lib=0;
																				SymbolTableEntry * tentry;
																				is_func=0;
																				//fprintf(stdout,"lvalue -> ID (%s)\n", yytext);
																				if(isLibFunc(table,yytext)){
																					id_is_lib=1;
																					is_func = 1;
																					tentry= ScopeLookup(table, (yyvsp[0].string), tempscope);
																					expr* exp = newexpr(libraryfunc_e);
																					exp->sym = tentry ;
																					exp->sym->sym_type = libraryfunc_s;
																					(yyval.expression) = exp;
																					
																				}else{
																					while(1){
																						tentry= ScopeLookup(table, yytext, tempscope);
																						if(tentry!=NULL){
																							varexists=1;
																							if(tentry->type==3 || tentry->type==4){
																								is_var=0;
																								//is_func = 1;
																								id_is_lib=1; //na tsekaroume giati einai edw 
																							}
																							expr* exp = newexpr(programfunc_e);
																							exp->sym= tentry;
																							(yyval.expression) = exp;
																							break;
																						}	
																						if(!tempscope)
																							break;
																						
																						tempscope--;
																						}	
																					
																						if(is_var){																					
																							if(varexists){
																								if(tentry->scope!=0 && tentry->scope!=top(in_function)){
																									fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): cannot access \'%s\' inside the function\n",yylineno, yytext);
																									return stop_parser();
																								}
																								expr* exp = newexpr(var_e);
																								exp->sym= tentry;
																								(yyval.expression) = exp;
																							}
																							else{
																								expr* exp = newexpr(var_e);

																								if(scope){
																									entry=CreateNode(yytext, scope, yylineno, LOC);																									
																									exp->sym= entry;
																									exp->sym->sym_type = var_s;
																									exp->sym->space=currScopeSpace();
																									exp->sym->offset=currScopeOffset();
																									incCurrScopeOffset();
																								}
																								else{
																									entry=CreateNode(yytext, scope, yylineno, GLOBAL);
																									exp->sym= entry;
																									exp->sym->sym_type = var_s;
																									exp->sym->space=currScopeSpace();
																									exp->sym->offset=currScopeOffset();
																									incCurrScopeOffset();
																								}
																								Insert(table, table->scopeHead, entry);

																								increase_data(f_names);
																								(yyval.expression) = exp;
																							}
																						}
																					
																					}
																				}
#line 2222 "parser.c" /* yacc.c:1646  */
    break;

  case 46:
#line 616 "parser.y" /* yacc.c:1646  */
    { 	varexists=0;
																			 	unsigned int tempscope=scope;
																				
																				if(isLibFunc(table,yytext)){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): \'%s\' shadows a library function\n",yylineno,yytext);
																					return stop_parser();
																					break;
																				}
																				else{
																					expr* exp = newexpr(var_e);
																					if(scope==0){
																						entry=CreateNode(yytext, scope, yylineno, GLOBAL);
																							exp->sym= entry;
																							exp->sym->sym_type = var_s;
																							exp->sym->space=currScopeSpace();
																							exp->sym->offset=currScopeOffset();
																							incCurrScopeOffset();
																						Insert(table, table->scopeHead, entry);
																						increase_data(f_names);
																						(yyval.expression) = exp;
																					}
																					else{
																						if(ScopeLookup2(table, yytext, scope)==0){
																							entry=CreateNode(yytext, scope, yylineno, LOC);
																								exp->sym= entry;
																								exp->sym->sym_type = var_s;
																								exp->sym->space=currScopeSpace();
																								exp->sym->offset=currScopeOffset();
																								incCurrScopeOffset();
																							Insert(table, table->scopeHead, entry);
																							increase_data(f_names);
																							(yyval.expression) = exp;
																						}
																					}
																					
																					//fprintf(stdout,"lvalue -> LOCAL ID (%s)\n", yytext);
																				}
																			}
#line 2265 "parser.c" /* yacc.c:1646  */
    break;

  case 47:
#line 655 "parser.y" /* yacc.c:1646  */
    {
																				if(ScopeLookup2(table, yytext, 0)==0){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): No global variable \'::%s\' exists\n",yylineno, yytext);
																					return stop_parser();
																				}
																			//	entry=CreateNode(yytext, 0, yylineno, GLOBAL);
																			//	Insert(table, table->scopeHead, entry);
																				//fprintf(stdout,"lvalue -> :: ID (%s)\n", yytext);
																			}
#line 2279 "parser.c" /* yacc.c:1646  */
    break;

  case 48:
#line 665 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"lvalue from a \'member\'\n");*/}
#line 2285 "parser.c" /* yacc.c:1646  */
    break;

  case 49:
#line 668 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"member -> lvalue . ID\n");
																				// if(ScopeLookup2(table, yytext, scope)==0){
																				// 	entry=CreateNode(yytext, scope, yylineno, GLOBAL);
																				// 	Insert(table, table->scopeHead, entry);
																				// 	increase_data(f_names);
																				// }
																				(yyval.expression) = member_item((yyvsp[-2].expression), (yyvsp[0].string) , scope, yylineno); //incrQuadNO eswterika

																			}
#line 2299 "parser.c" /* yacc.c:1646  */
    break;

  case 50:
#line 678 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"member -> call . ID\n");
																				if(ScopeLookup2(table, (yyvsp[0].string), scope)==0){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): No member \'.%s\' exists\n",yylineno, (yyvsp[0].string));
																					return stop_parser();
																				}
																			}
#line 2310 "parser.c" /* yacc.c:1646  */
    break;

  case 51:
#line 684 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"member -> call [ EXPR ]\n");*/}
#line 2316 "parser.c" /* yacc.c:1646  */
    break;

  case 52:
#line 688 "parser.y" /* yacc.c:1646  */
    {	//if(!is_func){fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): can not use function \'%s\' as a variable\n",yylineno,yytext); return stop_parser();}
																			//fprintf(stdout,"call -> (  elist ) \n");
																			(yyval.expression) = emitCall((yyvsp[-3].expression), elist_paramets, scope ,yylineno); //kanei eswterika to incrQuadNo();
																			if(elist_paramets != NULL)
																				free_param();
																			elist_paramets = NULL;
																		}
#line 2328 "parser.c" /* yacc.c:1646  */
    break;

  case 53:
#line 695 "parser.y" /* yacc.c:1646  */
    {	//xali
																			//if(!id_is_lib){
																				// if(!varexists){
																			 // 		fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): function  \'%s\' is undeclared\n",yylineno,$1->sym->name);
																			 // 		return stop_parser();
																			 // 	}else if(!is_func){
																				// 	fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): can not use variable \'%s\' as a function\n",yylineno,$1->sym->name);
																			  		//return stop_parser();
																			 // 	}
																		 	// }
																		}
#line 2344 "parser.c" /* yacc.c:1646  */
    break;

  case 54:
#line 706 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"call -> lvalue callsuffix\n");
																				if(call_method){
																				 	expr *self = (yyvsp[-2].expression);
																				 	(yyvsp[-2].expression) = emit_ifTableItem(member_item(self, call_method_name, scope, yylineno),scope, yylineno);
																				 	insert_param(self);
																				}
																				
																				(yyval.expression) = emitCall((yyvsp[-2].expression),elist_paramets, scope ,yylineno);
																				if(elist_paramets != NULL)
																					free_param();
																				elist_paramets = NULL;


																				}
#line 2363 "parser.c" /* yacc.c:1646  */
    break;

  case 55:
#line 722 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"call -> (funcdef) (elist)\n");
																				expr* exp = newexpr(programfunc_e);
																				exp->sym= (yyvsp[-4].symboltableentry); 
																				(yyval.expression) = emitCall(exp, elist_paramets, scope ,yylineno);
																				if(elist_paramets != NULL)
																					free_param();
																				elist_paramets = NULL;
																			}
#line 2376 "parser.c" /* yacc.c:1646  */
    break;

  case 56:
#line 732 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"callsuffix -> normcall\n");
																				(yyval.expression) = (yyvsp[0].expression);
																			}
#line 2384 "parser.c" /* yacc.c:1646  */
    break;

  case 57:
#line 735 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"callsuffix -> methodcall\n");
																				(yyval.expression) = (yyvsp[0].expression);
																			}
#line 2392 "parser.c" /* yacc.c:1646  */
    break;

  case 58:
#line 740 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"normcall -> (elist)\n");
																				call_method = 0; 
																				call_method_name = NULL;
																			}
#line 2401 "parser.c" /* yacc.c:1646  */
    break;

  case 59:
#line 747 "parser.y" /* yacc.c:1646  */
    {call_method_name = strdup((yyvsp[0].string));}
#line 2407 "parser.c" /* yacc.c:1646  */
    break;

  case 60:
#line 748 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"methodcall -> ::ID (elist)\n");
																				call_method = 1; //simainei oti exoume ..f()
																		 	}
#line 2415 "parser.c" /* yacc.c:1646  */
    break;

  case 61:
#line 755 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"tablemake -> [elist]\n");
																				expr *exp = newexpr(newtable_e); 
																				exp->sym = CreateNode(bringTemp()->tname, scope, yylineno, LOC); //tmpVar as LOC
																				exp->sym->sym_type = var_s;
																				exp->sym->space=currScopeSpace();
																				exp->sym->offset=currScopeOffset();
																				incCurrScopeOffset();
																				Insert(table, table->scopeHead,exp->sym);
																				incrQuadNo();
																				myEmit(tablecreate, NULL, NULL, exp,getQuadNo(), yylineno);
																				int i =0;
																				param_expr *tmp = elist_paramets;
																				while(tmp != NULL){
																					incrQuadNo();
																					myEmit(tablesetelement, exp, newexpr_constnum(i++), tmp->exp,getQuadNo(), yylineno);
																					tmp = tmp->right;
																				}
																				(yyval.expression) = exp;
																				if(elist_paramets != NULL)
																					free_param();
																				elist_paramets = NULL;

																			}
#line 2443 "parser.c" /* yacc.c:1646  */
    break;

  case 62:
#line 778 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"tablemake -> [indexed]\n");
																				expr *exp = newexpr(newtable_e); 
																				exp->sym = CreateNode(bringTemp()->tname, scope, yylineno, LOC); //tmpVar as LOC
																				exp->sym->sym_type = var_s;
																				exp->sym->space=currScopeSpace();
																				exp->sym->offset=currScopeOffset();
																				incCurrScopeOffset();
																				Insert(table, table->scopeHead,exp->sym);
																				incrQuadNo();
																				myEmit(tablecreate, NULL, NULL, exp,getQuadNo(), yylineno);

																				indexed_expr *tmp = indexed_paramets;
																				while(tmp != NULL){
																					incrQuadNo();
																					myEmit(tablesetelement, exp, tmp->exp1, tmp->exp2,getQuadNo(), yylineno);
																					tmp = tmp->next;
																				}
																				(yyval.expression) = exp;
																				if(indexed_paramets != NULL)
																					free_indexed();
																				indexed_paramets = NULL;

																			}
#line 2471 "parser.c" /* yacc.c:1646  */
    break;

  case 63:
#line 803 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"indexed -> indexedelem\n");*/}
#line 2477 "parser.c" /* yacc.c:1646  */
    break;

  case 64:
#line 806 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"indexed -> , indexedelem\n");*/}
#line 2483 "parser.c" /* yacc.c:1646  */
    break;

  case 65:
#line 807 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"indexed -> empty\n");*/}
#line 2489 "parser.c" /* yacc.c:1646  */
    break;

  case 66:
#line 810 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"indexedelem -> {expr : expr}  \n");*/
																				insert_indexed((yyvsp[-3].expression),(yyvsp[-1].expression));
																			}
#line 2497 "parser.c" /* yacc.c:1646  */
    break;

  case 67:
#line 815 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"block -> {}\n");*/}
#line 2503 "parser.c" /* yacc.c:1646  */
    break;

  case 68:
#line 816 "parser.y" /* yacc.c:1646  */
    {scope++; addScope(table->scopeHead, scope); enterScopeSpace();}
#line 2509 "parser.c" /* yacc.c:1646  */
    break;

  case 69:
#line 817 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"block -> {stmt*}\n");
																				Hide(table, scope);
																				scope--;
																				exitScopeSpace();
																			}
#line 2519 "parser.c" /* yacc.c:1646  */
    break;

  case 70:
#line 824 "parser.y" /* yacc.c:1646  */
    {	tempname = (char *) malloc(strlen((yyvsp[0].string))+1); 
																				strcpy(tempname, (yyvsp[0].string)); 
																				unsigned int funcexists=0, tempscope=scope, flag=0, func_err=0;
																				if(isLibFunc(table,tempname)){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): %s shadows a library function\n",yylineno, tempname);
																					return stop_parser();
																				}
																				else{
																					SymbolTableEntry*  tentry;
																						tentry= ScopeLookup(table, tempname, tempscope);
																						
																					if(tentry!=NULL){
																						funcexists=1;
																						if(tentry->type==0){
																							flag=1;
																							fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): \'%s\' already defined as GLOBAL variable \n",yylineno, yytext);
																							return stop_parser();
																						}else if(tentry->type==1){
																							flag=1;
																							fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): \'%s\' already defined as LOCAL variable \n",yylineno, yytext);
																							return stop_parser();
																						}else if(tentry->type==2){
																							flag=1;
																							fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): \'%s\' already defined as FORMAL variable \n",yylineno, yytext);
																							return stop_parser();
																						}else if(tentry->type==3 && tentry->scope==scope){
																							func_err=1;
																							fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): \'%s\' already defined as USER function in scope: %d\n",yylineno, yytext, scope);
																							return stop_parser();
																						}

																					}
																					if(!func_err){
																						if(!flag){																			
																							if(funcexists){
																								entry=CreateNode(tempname, tempscope, yylineno, USERFUNC);
																								//fprintf(stdout, "just added Function %s, in scope %d \n", tempname, tempscope);
																							}
																							else{
																								entry=CreateNode(tempname, scope, yylineno, USERFUNC);
																								//fprintf(stdout, "just added Function %s, in scope %d \n", tempname, scope);
																							}
																							//{fprintf(stdout,"funcdef -> function ID  (idlist) block\n");}

																							Insert(table, table->scopeHead, entry);

																							f_names= push_name(f_names, (yyvsp[0].string));

																							expr* exp = newexpr(programfunc_e);
																							exp->sym = entry;
																							exp->sym->sym_type = programfunc_s;
																							exp->sym->name= malloc(strlen((yyvsp[0].string)));
																							strcpy(exp->sym->name, (yyvsp[0].string));
																							incrQuadNo();
																							myEmit(funcstart, NULL, NULL, exp, getQuadNo(),yylineno);
																							entry->label = getQuadNo();
																							
																						}
																					}
																					
																				}
																				
																			}
#line 2587 "parser.c" /* yacc.c:1646  */
    break;

  case 71:
#line 887 "parser.y" /* yacc.c:1646  */
    {scope++; 
																				addScope(table->scopeHead, scope); 
																				inside_function++; 
																				in_function=push(in_function, scope); 
																				enterScopeSpace();
																				if(inside_function != 0){
																					addOffsetNode(get_functionLocalOffset());
																					reset_formalArgsOffset();
																					reset_functionLocalOffset();
																				}

																			}
#line 2604 "parser.c" /* yacc.c:1646  */
    break;

  case 72:
#line 899 "parser.y" /* yacc.c:1646  */
    {scope--;}
#line 2610 "parser.c" /* yacc.c:1646  */
    break;

  case 73:
#line 900 "parser.y" /* yacc.c:1646  */
    {	inside_function--;
																									
																				in_function = pop(in_function);
																				
																				struct f_stack* top_f= top_struct(f_names);

																				SymbolTableEntry*  tentry;
																				tentry= ScopeLookup(table, top_f->f_name, scope);
																				tentry->totalArgs= top_f->data;
																				 
																				expr* exp = newexpr(programfunc_e);
																				exp->sym = tentry;
																				exp->sym->name= malloc(strlen(top_f->f_name));
																				strcpy(exp->sym->name, top_f->f_name);
																				
																				incrQuadNo();
																				myEmit(funcend, NULL, NULL, exp, getQuadNo(),yylineno);
																				
																				f_names= pop(f_names);
																				exitScopeSpace();

																				set_functionLocalOffset(popOffsetNode()); //ksan kanoume to offset gia ta locals iso me oso itan prin 

																				(yyval.symboltableentry) = tentry;
																			}
#line 2640 "parser.c" /* yacc.c:1646  */
    break;

  case 74:
#line 926 "parser.y" /* yacc.c:1646  */
    {scope++; 
																				addScope(table->scopeHead, scope); 
																				inside_function++; 
																				in_function=push(in_function, scope); 
																				enterScopeSpace(); 
																				if(inside_function != 0){
																					addOffsetNode(get_functionLocalOffset());
																					reset_formalArgsOffset();
																					reset_functionLocalOffset();
																				}
																			}
#line 2656 "parser.c" /* yacc.c:1646  */
    break;

  case 75:
#line 937 "parser.y" /* yacc.c:1646  */
    {scope--;}
#line 2662 "parser.c" /* yacc.c:1646  */
    break;

  case 76:
#line 938 "parser.y" /* yacc.c:1646  */
    { 	char * buf = (char *) malloc(sizeof(int));
																				char* funcName = (char*) malloc(strlen(buf)+3);

																				funcCounter++;
																				itoa(funcCounter,buf);
																				strcpy(funcName, "_f");
																				funcName=strcat(funcName,buf);
																				
																				entry=CreateNode(funcName, scope, yylineno, USERFUNC);
																				entry->sym_type = programfunc_s;
																				
																				Insert(table, table->scopeHead, entry);

																				f_names= push_name(f_names, funcName);
																				expr* exp = newexpr(programfunc_e);
																				exp->sym = entry;
																				exp->sym->name= malloc(strlen(funcName));
																				strcpy(exp->sym->name, funcName);
																				
																				incrQuadNo();
																				myEmit(funcstart, NULL, NULL, exp, getQuadNo(),yylineno);
																				entry->label = getQuadNo();
																				
																			}
#line 2691 "parser.c" /* yacc.c:1646  */
    break;

  case 77:
#line 962 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"funcdef -> function (idlist) block\n");
																				inside_function--;
																				
																				in_function = pop(in_function);

																				struct f_stack* top_f= top_struct(f_names);

																				SymbolTableEntry*  tentry;
																				tentry= ScopeLookup(table, f_names->f_name, scope);
																				tentry->totalArgs= f_names->data;

																				expr* exp = newexpr(programfunc_e);
																				exp->sym = tentry;
																				exp->sym->name= malloc(strlen(top_f->f_name));
																				strcpy(exp->sym->name, top_f->f_name);
																				
																				incrQuadNo();
																				myEmit(funcend, NULL, NULL, exp, getQuadNo(),yylineno);

																				f_names= pop(f_names);
																				exitScopeSpace();
																				set_functionLocalOffset(popOffsetNode());
																				(yyval.symboltableentry) = entry;
																			}
#line 2720 "parser.c" /* yacc.c:1646  */
    break;

  case 78:
#line 988 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"const -> integer '%d'\n", $1);*/ (yyval.expression)=newexpr_constnum((double)(yyvsp[0].integer));}
#line 2726 "parser.c" /* yacc.c:1646  */
    break;

  case 79:
#line 989 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"const -> real\n");*/ (yyval.expression)=newexpr_constnum((yyvsp[0].real)); }
#line 2732 "parser.c" /* yacc.c:1646  */
    break;

  case 80:
#line 990 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"const -> string '%s'\n", $1);*/ (yyval.expression)=newexpr_conststring((yyvsp[0].string)); }
#line 2738 "parser.c" /* yacc.c:1646  */
    break;

  case 81:
#line 991 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"const -> nil\n");*/ (yyval.expression)=newexpr_constnil();}
#line 2744 "parser.c" /* yacc.c:1646  */
    break;

  case 82:
#line 992 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"const -> true\n"); */(yyval.expression)=newexpr_constbool('T');}
#line 2750 "parser.c" /* yacc.c:1646  */
    break;

  case 83:
#line 993 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"const -> false\n");*/ (yyval.expression)=newexpr_constbool('F');}
#line 2756 "parser.c" /* yacc.c:1646  */
    break;

  case 84:
#line 997 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"idlist -> ID (1)\n");
																				if(isLibFunc(table,yytext)){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): \'%s\' shadows a library function\n",yylineno,yytext);
																					return stop_parser();
																				}
																				else{

																					entry=CreateNode(yytext, scope, yylineno, FORMAL);
																					entry->sym_type = var_s;
																					entry->space = currScopeSpace();
																					entry->offset = currScopeOffset();
																					incCurrScopeOffset();
																					Insert(table,table->scopeHead, entry);
																				}
																			}
#line 2776 "parser.c" /* yacc.c:1646  */
    break;

  case 85:
#line 1012 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"idlist -> ID idlist2\n");*/}
#line 2782 "parser.c" /* yacc.c:1646  */
    break;

  case 86:
#line 1013 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"idlist -> empty\n");*/}
#line 2788 "parser.c" /* yacc.c:1646  */
    break;

  case 87:
#line 1017 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"idlist -> ID (2)\n");
																				if(isLibFunc(table,yytext)){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): \'%s\' shadows a library function\n",yylineno,yytext);
																					return stop_parser();
																				}
																				else{
																					SymbolTableEntry*  tentry;
																					tentry= ScopeLookup(table, yytext, scope);
																						
																					if(tentry!=NULL){
																						fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): variable \'%s\' already defined in scope: %d\n",yylineno, yytext, scope);
																						return stop_parser();
																					}
																					else{
																						entry=CreateNode(yytext, scope, yylineno, FORMAL);
																						entry->sym_type = var_s;
																						entry->space = currScopeSpace();
																						entry->offset = currScopeOffset();
																						incCurrScopeOffset();
																						Insert(table,table->scopeHead, entry);
																					}
																				}
																			}
#line 2816 "parser.c" /* yacc.c:1646  */
    break;

  case 88:
#line 1040 "parser.y" /* yacc.c:1646  */
    {	/*fprintf(stdout,"idlist2 -> , ID idlist2\n"); */}
#line 2822 "parser.c" /* yacc.c:1646  */
    break;

  case 89:
#line 1041 "parser.y" /* yacc.c:1646  */
    {	/*fprintf(stdout,"idlist -> empty\n");*/}
#line 2828 "parser.c" /* yacc.c:1646  */
    break;

  case 90:
#line 1045 "parser.y" /* yacc.c:1646  */
    {	/*fprintf(stdout,"ifstmt -> if(expr) stmt \n");*/
																				
																				incrQuadNo();
																				emitIf((yyvsp[-1].expression), yylineno );
																				
																				(yyval.unsigned_t)= getQuadNo();
																			}
#line 2840 "parser.c" /* yacc.c:1646  */
    break;

  case 91:
#line 1054 "parser.y" /* yacc.c:1646  */
    {
																				incrQuadNo();
																				emitJump(0, yylineno);

																				(yyval.unsigned_t)= getQuadNo();
																			}
#line 2851 "parser.c" /* yacc.c:1646  */
    break;

  case 92:
#line 1062 "parser.y" /* yacc.c:1646  */
    {	/*fprintf(stdout,"ifstmt -> if(expr) stmt \n");*/
																				patchlabel((yyvsp[-1].unsigned_t), nextQuadNo());
																			}
#line 2859 "parser.c" /* yacc.c:1646  */
    break;

  case 93:
#line 1065 "parser.y" /* yacc.c:1646  */
    {	/*fprintf(stdout,"ifstmt -> if(expr) stmt else stmt\n");*/
																				patchlabel((yyvsp[-3].unsigned_t), (yyvsp[-1].unsigned_t)+1);
																				patchlabel((yyvsp[-1].unsigned_t), nextQuadNo());
																			}
#line 2868 "parser.c" /* yacc.c:1646  */
    break;

  case 94:
#line 1072 "parser.y" /* yacc.c:1646  */
    {
																				(yyval.unsigned_t)= nextQuadNo();
																			}
#line 2876 "parser.c" /* yacc.c:1646  */
    break;

  case 95:
#line 1077 "parser.y" /* yacc.c:1646  */
    {
																				incrQuadNo();
																				emitIf((yyvsp[-1].expression), yylineno );

																				(yyval.unsigned_t)= getQuadNo();

																				inside_loop++;
																			}
#line 2889 "parser.c" /* yacc.c:1646  */
    break;

  case 96:
#line 1088 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"whilestmt -> while(expr) stmt\n");

																				incrQuadNo();
																				emitJump((yyvsp[-2].unsigned_t), yylineno);
																				patchlabel((yyvsp[-1].unsigned_t), nextQuadNo());

																				breaklist = patchlabel_bc(breaklist, nextQuadNo(), inside_loop);
																				contlist = patchlabel_bc(contlist, (yyvsp[-2].unsigned_t), inside_loop);
																				
																				inside_loop--;
																			}
#line 2905 "parser.c" /* yacc.c:1646  */
    break;

  case 97:
#line 1102 "parser.y" /* yacc.c:1646  */
    {	(yyval.unsigned_t)= nextQuadNo();	}
#line 2911 "parser.c" /* yacc.c:1646  */
    break;

  case 98:
#line 1106 "parser.y" /* yacc.c:1646  */
    {	incrQuadNo();
																				emitJump(0, yylineno);
																				(yyval.unsigned_t)= getQuadNo();
																			}
#line 2920 "parser.c" /* yacc.c:1646  */
    break;

  case 99:
#line 1113 "parser.y" /* yacc.c:1646  */
    {	struct label_carrier* ret_carrier= malloc(sizeof(struct label_carrier));
																				ret_carrier->test= (yyvsp[-2].unsigned_t);
																				incrQuadNo();
																				myEmit(if_eq, (yyvsp[-1].expression), newexpr_constbool('T'), NULL, 0, yylineno);
																				ret_carrier->enter= getQuadNo();
																				(yyval.carrier_t)= ret_carrier;
																			}
#line 2932 "parser.c" /* yacc.c:1646  */
    break;

  case 100:
#line 1123 "parser.y" /* yacc.c:1646  */
    {
																				patchlabel((yyvsp[-4].carrier_t)->enter, (yyvsp[0].unsigned_t)+1);
																				patchlabel((yyvsp[0].unsigned_t), (yyvsp[-4].carrier_t)->test);
																				inside_loop++;

																				(yyval.unsigned_t)= (yyvsp[-3].unsigned_t);
																			}
#line 2944 "parser.c" /* yacc.c:1646  */
    break;

  case 101:
#line 1134 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"forstmt -> for(elist;expr;elist) stmt\n");
																				
																				patchlabel((yyvsp[-2].unsigned_t), nextQuadNo());
																				patchlabel((yyvsp[0].unsigned_t), (yyvsp[-2].unsigned_t)+1);

																				breaklist = patchlabel_bc(breaklist, nextQuadNo(), inside_loop);
																				contlist = patchlabel_bc(contlist, (yyvsp[-2].unsigned_t)+1, inside_loop);
																				
																				inside_loop--;
																			}
#line 2959 "parser.c" /* yacc.c:1646  */
    break;

  case 102:
#line 1146 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"returnstmt -> return;\n");
																				if(!inside_function){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): Use of \'return\' while not in a function\n", yylineno);
																					return stop_parser();
																				}
																				myEmit(ret, NULL, NULL, NULL, getQuadNo(), yylineno);

																			}
#line 2972 "parser.c" /* yacc.c:1646  */
    break;

  case 103:
#line 1154 "parser.y" /* yacc.c:1646  */
    {	//fprintf(stdout,"returnstmt -> return expr;\n");
																				if(!inside_function){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): Use of \'return\' while not in a function\n", yylineno);
																					return stop_parser();
																				}

																				expr* tmp;
																				int const_val=0;
																				if((yyvsp[-1].expression)->type == constnum_e){
																					const_val=1;
																					tmp = newexpr_constnum((yyvsp[-1].expression)->numConst);
																				}else if((yyvsp[-1].expression)->type == conststring_e){
																					const_val=1;
																					tmp = newexpr_conststring((yyvsp[-1].expression)->strConst);
																				}else if((yyvsp[-1].expression)->type == constbool_e){
																					const_val=1;
																					tmp = newexpr_constbool((yyvsp[-1].expression)->boolConst);
																				}else{
																					tmp = (yyvsp[-1].expression);
																				}

																				if(const_val){
																					tmp->type =  var_e;
																					tmp->sym= CreateNode(bringTemp()->tname, scope, yylineno, LOC); //tmpVar as LOC
																					tmp->sym->sym_type = var_s;
																					tmp->sym->space = currScopeSpace();
																					tmp->sym->offset = currScopeOffset();
																					incCurrScopeOffset();
																					Insert(table, table->scopeHead,tmp->sym);
																					incrQuadNo();
																					myEmit(assign, (yyvsp[-1].expression), NULL, tmp, getQuadNo(), yylineno);
																				}
																				incrQuadNo();
																				myEmit(ret, NULL, NULL, tmp, getQuadNo(), yylineno);
																			}
#line 3012 "parser.c" /* yacc.c:1646  */
    break;

  case 104:
#line 1191 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"elist -> expr\n");
																				insert_param((yyvsp[-1].expression));
																			}
#line 3020 "parser.c" /* yacc.c:1646  */
    break;

  case 106:
#line 1197 "parser.y" /* yacc.c:1646  */
    {//fprintf(stdout,"elist -> , expr\n");
																				insert_param((yyvsp[-1].expression));
																			}
#line 3028 "parser.c" /* yacc.c:1646  */
    break;

  case 107:
#line 1200 "parser.y" /* yacc.c:1646  */
    {/*fprintf(stdout,"elist -> empty\n");*/}
#line 3034 "parser.c" /* yacc.c:1646  */
    break;


#line 3038 "parser.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 1204 "parser.y" /* yacc.c:1906  */


int yyerror (char* yaccProvidedMessage)
{
	fprintf(stderr, "\t\x1b[31mSYNTAX ERROR\x1b[0m\n%s: at line %d, before token:  <%s> \n", yaccProvidedMessage, yylineno, yytext);
}


int main(int argc,char** argv){
	table=CreateTable();
    if(argc>1){
        if(!(yyin = fopen(argv[1],"r"))){
            fprintf(stderr,"Cannot read file: %s\n",argv[1]);
            return 1;
        }
    }
    else{
        yyin = stdin;
    }

    elist_paramets = NULL;
	indexed_paramets = NULL;
    

    SymbolTableEntry * newEntry;
    newEntry= CreateNode("print", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

  	newEntry= CreateNode("input", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("objectmemberkeys", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("objecttotalmembers", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("objectcopy", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("totalarguments", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("argument", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("typeof", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("strtonum", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("sqrt", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("cos", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

	newEntry= CreateNode("sin", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

	
	if(yyparse())
		return 1;
	fprintf(stderr, "\x1b[32mcompilation succeded\x1b[0m\n");

	//printTable(table);

	printQuads();
	printQuads_to_file();
	
    return 0;
}
