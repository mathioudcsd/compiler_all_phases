 /*	
 *	Project Name: Grammatic Analysis cs340
 *	Authors: 
 *			Fotios Mathioudakis
 *			Leonidas Anagnostou
 *			Evaggelia Athanasaki
 */

/*  Variables, DataTypes, #defines and Functions Naming Contract:
 *	-------------------------------------------------------------
 *	Functions: First letter Capital, continues with lowercase,
 *	until we meet first letter of real English word,
 *	or changing word in general.
 *	e.g. int* FindTheNumber(int n);
 *	
 *	DataTypes: Same as Functions
 * 	e.g. typedef struct S{..}ThisIsANewStruct;
 *
 *	Variables: First letter, lowercase, continues with lowercase
 *	until we meet first letter of real English word or changing 
 * 	word in general.
 *	e.g. int thisIsAVar; char* whatsUpBro;
*/

//symtable.h
#ifndef _symtable_h
#define _symtable_h



#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#define	HASHSIZE 20




/*
 *	Definitions of Types and Enums
 *	------------------------------
*/
enum SymbolTableType {
	GLOBAL,
	LOC,
	FORMAL,
	USERFUNC,
	LIBFUNC
};


typedef enum scopespace_e{
	programvar,
	functionLocal,
	formalarg
}scopespace_t;


typedef enum symbol_e {var_s, programfunc_s, libraryfunc_s}symbol_t;


unsigned int scope;


// unsigned programVarOffset = 0;
// unsigned functionLocalOffset = 0;
// unsigned formalArgOffset = 0;
// unsigned scopeSpaceCounter =1;

/*
 *	when we create a node, we cannot pass as an arguement 
 *	whether it is a var or a func
 */
//enum ValueType{VARIABLE, FUNCTION}; 

// typedef struct Arguments{
// 	const char* name;
// 	struct Arguments* next; 
// } Arguments;

									
// typedef struct Variable {
// 	const char* name;
// 	unsigned int scope;
// 	unsigned int line;
// } Variable;


// typedef struct Function{
// 	const char* name;
// 	unsigned int scope;
// 	unsigned int line;
// 	//Arguments* args;
// } Function;

// typedef struct SymbolTableEntry {
// 	int isActive;
// 	union {
// 		Variable* varVal;
// 		Function* funcVal;
// 	} value;
// 	enum SymbolTableType type; //GLOBAL, LOC, FORMAL, USERFUNC, LIBFUNC
// 	struct SymbolTableEntry* nextInTable; //next horizontally
// 	struct SymbolTableEntry* nextInScope; //next verically (scopewise)
// } SymbolTableEntry;

typedef struct SymbolTableEntry {
	scopespace_t space;
	int totalArgs;
	unsigned offset;
	int isActive;
	char* name;
	unsigned int scope;
	unsigned int line;
	enum SymbolTableType type; //GLOBAL, LOC, FORMAL, USERFUNC, LIBFUNC
	symbol_t sym_type; // var_s, programfunc_s, libraryfunc_s
	struct SymbolTableEntry* nextInTable; //next horizontally
	struct SymbolTableEntry* nextInScope; //next verically (scopewise)
	unsigned label;
} SymbolTableEntry;

typedef struct ScopeList{
	SymbolTableEntry* symTableNode; // crossing vertically the SymbolTable 
									//	among nodes with same scope upwards ^
	struct ScopeList* next;			// crossing horizontally the first row of scopelist
									// incrementing in scope by +1.
}ScopeList;

typedef struct SymbolTable{
	SymbolTableEntry* hashTable[HASHSIZE];
	ScopeList* scopeHead;
} SymbolTable;




//SymbolTabint functionCounteru=0;
//ScopeList* scopelist;



/*
 *	Definitions of Functions
 *	------------------------
 */


/*
 *	Creates a symbol table
 */
SymbolTable* CreateTable();

/* 
 *	Our Hash Function deciding about the 
 * 	placement of the new nodes(entries) 
 */
unsigned int HashFunction(const char* x);


SymbolTableEntry* CreateNode(const char* name,unsigned int scope, unsigned int line, enum SymbolTableType type );

/* 
 *	Insert new node to the SymbolTable + ScopeList
 */
int Insert(SymbolTable* tbl, ScopeList* scopeList, SymbolTableEntry* newNode);

/*
 *	Hides (inactive) nodes of a certain scope level
 */
void Hide(SymbolTable* tbl, unsigned int scope);

/*
 *	Looks for a node in any Scope
 */
SymbolTableEntry* Lookup(SymbolTable* tbl, const char* name);

/*
 *	Looks for a node in a specific Scope
 */
SymbolTableEntry* ScopeLookup(SymbolTable* tbl, const char* name, unsigned int scope);

int ScopeLookup2(SymbolTable* tbl, const char* name, unsigned int scope);

int addScope(ScopeList* scopeList, unsigned int scope);

//Arguments* CreateArgsList(Arguments* args,const char* a);

void printTable(SymbolTable* s);

void printScopeTable(ScopeList* s);

int isLibFunc(SymbolTable* s,const char* text);

void reverse(char* s);

void itoa(int n, char* s);

void report_error(int, char*);




scopespace_t currScopeSpace(void);

unsigned currScopeOffset(void);

void incCurrScopeOffset(void);

void enterScopeSpace(void);

void exitScopeSpace(void);

void reset_formalArgsOffset(void);

void reset_functionLocalOffset(void);

unsigned get_functionLocalOffset(void);

void set_functionLocalOffset(unsigned  offset);

SymbolTable* table;

















/*


typedef struct scope_list{
	SymbolTableEntry_node* symtnode;
	struct scope_list *next;
} scope_list;


typedef struct Bucket{
	SymbolTableEntry_node* symbol;
} Bucket;


typedef struct Hashtable{
	Bucket buckets[Hash_size];
} Hashtable;


*/


#endif