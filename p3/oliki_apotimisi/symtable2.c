 //test
#include "symtable.h"
#include "tmpVars.h"

unsigned programVarOffset = 0;
unsigned functionLocalOffset = 0;
unsigned formalArgOffset = 0;
unsigned scopeSpaceCounter =1;

unsigned int HashFunction(const char* x){
	unsigned int i=0;  
	unsigned int hashNum=0;
	return	hashNum=(hashNum*314+(unsigned int)x[0])%HASHSIZE;
}

SymbolTable* CreateTable(){
	int i;
	SymbolTable* new;
	new=(SymbolTable*)malloc(sizeof(SymbolTable));
	
	if(new==NULL){
		fprintf(stderr, "Malloc Error: new Symbol table\n");
		return NULL;
	}
	
	new->scopeHead=(ScopeList*) malloc(sizeof(ScopeList) ); 
	new->scopeHead->symTableNode = NULL; 
	new->scopeHead->next=NULL; 

	for(i=0;i<HASHSIZE;i++)
		new->hashTable[i]=NULL;
	return new;
}

int addScope(ScopeList* scopeList, unsigned int scope){
	ScopeList* scopePtr=scopeList;
	unsigned int hop=0;

	//uparxei
	if(scopePtr!= NULL && scope==0){
		return 1;
	}
	

	while(scopePtr->next != NULL ){
		if(hop == scope){
			return 1;
		}
		hop++;
		scopePtr = scopePtr->next;
	}

	ScopeList* newscope= (ScopeList*) malloc(sizeof(ScopeList) );
	newscope->symTableNode= NULL;
	newscope->next= NULL;	
	scopePtr->next=newscope;
	return 0;
}




SymbolTableEntry* CreateNode(const char* name, unsigned int scope, unsigned int line, enum SymbolTableType type){
	char* nameBuff;

	SymbolTableEntry* buff = (SymbolTableEntry*)malloc(sizeof(SymbolTableEntry));

	if(buff==NULL){
		fprintf(stderr, "Malloc Error: new Node creation\n");
		return NULL;
	}

	nameBuff=(char*)malloc(strlen(name));
	nameBuff=strdup(name);

	buff->name=nameBuff;
	buff->scope=scope;
	buff->line=line;
		
	buff->isActive=1;
	buff->type=type; 
	buff->nextInTable=NULL; 
	buff->nextInScope=NULL;
	
	return buff;
}


int Insert(SymbolTable* tbl, ScopeList* scopeList, SymbolTableEntry* newNode){
	unsigned int hash;
	unsigned int scope;
	unsigned int i=0;
	ScopeList* scopePtr=scopeList;
	SymbolTableEntry* symEntry;
	SymbolTableEntry* buffer;
	SymbolTableEntry* curr;

	hash=HashFunction(newNode->name);
	scope=newNode->scope;

	if(tbl->hashTable[hash]==NULL){
		tbl->hashTable[hash]=newNode;
		tbl->hashTable[hash]->nextInTable=NULL;
	}
	else{ 
		curr=tbl->hashTable[hash];
		while(curr->nextInTable!=NULL){
			curr=curr->nextInTable;
		}
		curr->nextInTable=newNode;
		curr=newNode;
		curr->nextInTable=NULL;
	}

	addScope(scopeList, scope);
	/* add sto scopeList */
	scopePtr = scopeList;
	while(i<scope){
		scopePtr=scopePtr->next;
		i++;
	}
	if(scopePtr->symTableNode==NULL){
		scopePtr->symTableNode=newNode;
		scopePtr->next=NULL;
	}
	else{
		symEntry=scopePtr->symTableNode;
		while(symEntry->nextInScope!=NULL){
			symEntry=symEntry->nextInScope;
		}
		symEntry->nextInScope=newNode;
	}
	return 0;
}


void Hide(SymbolTable* tbl, unsigned int scope){
	ScopeList* slist= tbl->scopeHead;
	unsigned int num= 0;

	while(num<scope){
		if(slist->next==NULL){
			fprintf(stderr, "Hide Error: scope does not exist\n");
			return;
		}
		slist= slist->next;
		num++;
	}
	
	SymbolTableEntry * buffer= slist->symTableNode;

	if(num>0){
		while(buffer){
		if(buffer->isActive==1){
			if(buffer->type<3)
				if(strncmp(buffer->name, "_", 1)==0){
					fprintf(stdout, "De-Activating _t var : %s\n",buffer->name);
					removeFromActiveTempList_n(buffer->name);
				}
				else
					fprintf(stdout, "De-Activating variable : %s\n",buffer->name);
			else
				fprintf(stdout, "De-Activating function : %s\n",buffer->name);
		}
		buffer->isActive= 0;
		buffer= buffer->nextInScope;
		}
	}
	

	return;
}




SymbolTableEntry* Lookup(SymbolTable* tbl, const char* name){
	unsigned int key= HashFunction(name);
	SymbolTableEntry * buffer= tbl->hashTable[key];

	while(buffer){
		if( strcmp(buffer->name, name )==0 )
			return buffer;

		buffer= buffer->nextInTable;
	}

	return NULL;
}


SymbolTableEntry* ScopeLookup(SymbolTable* tbl, const char* name, unsigned int scope){
	ScopeList* slist= tbl->scopeHead;
	unsigned int num= 0;

	while(num<scope){
		if(slist->next==NULL){
			fprintf(stderr, "ScopeLookup Error: scope does not exist\n");
			return NULL;
		}
		slist= slist->next;
		num++;
	}
	
	SymbolTableEntry * buffer= slist->symTableNode;

	while(buffer){
		if( strcmp(buffer->name, name )==0 ){
			if(buffer->isActive)
				return buffer;
		}

		buffer= buffer->nextInScope;
	}

	return NULL;
}


int ScopeLookup2(SymbolTable* tbl, const char* name, unsigned int scope){
	ScopeList* slist= tbl->scopeHead;
	unsigned int num= 0;

	while(num<scope){
		if(slist->next==NULL){
			return 0;
		}
		slist= slist->next;
		num++;
	}
	
	SymbolTableEntry * buffer= slist->symTableNode;

	while(buffer){
		if( strcmp(buffer->name, name )==0 ){
			if(buffer->isActive)
				return 1;
		}
			
		buffer= buffer->nextInScope;
	}

	return 0;
}




void printTable(SymbolTable* s){
	int i = 0;
	SymbolTableEntry* ptr=s->hashTable[0];
	int type;
	fprintf(stdout, "===================================\n" );
	fprintf(stdout, "LINE \t SCOPE \t TYPE \t\t\t NAME\n");
	fprintf(stdout, "______________________________________________________\n");
	while(i<HASHSIZE){
		ptr=s->hashTable[i];
		while(ptr!=NULL){
			type=(int)ptr->type;
			switch (ptr->type){
				case 0:
					fprintf(stdout, "%d \t (%d) \t GLOBAL VARIABLE \t %s\n", ptr->line, ptr->scope, ptr->name);
					break;
				case 1:
					fprintf(stdout, "%d \t (%d) \t LOCAL VARIABLE \t %s\n", ptr->line, ptr->scope, ptr->name);
					break;
				case 2:
					fprintf(stdout, "%d \t (%d) \t FORMAL VARIABLE \t %s\n", ptr->line, ptr->scope, ptr->name);
					break;
				case 3:
					fprintf(stdout, "%d \t (%d) \t USERFUNC \t\t %s\n", ptr->line, ptr->scope, ptr->name);
					break;
				case 4:
					//fprintf(stdout, "%d \t (%d) \t LIBFUNC \t\t %s\n", ptr->line, ptr->scope, ptr->name);
					break;	
			}
			ptr=ptr->nextInTable;
		}
		i++;
	}
	fprintf(stdout, "______________________________________________________\n");
}

void printScopeTable(ScopeList* scopeList){
	ScopeList* scopePtr =scopeList;
	SymbolTableEntry* ptr=scopePtr->symTableNode;
	int type;
	while(scopePtr!=NULL){
		while(ptr!=NULL){
			fprintf(stdout, "%d ", ptr->scope);
			fprintf(stdout, "%s \n",ptr->name);
			
			ptr=ptr->nextInScope;
		}
		scopePtr=scopePtr->next;
		if(scopePtr==NULL)
			break;
		ptr=scopePtr->symTableNode;
	}
}

// Arguments* CreateArgsList(Arguments* args,const char* a){
// 	 Arguments* head = args;

// 	if (head==NULL){
// 		Arguments *node = (Arguments*) malloc(sizeof(Arguments));
// 		node -> name = a;
// 		node -> next = NULL;
// 		head = node;
// 		return head;
// 	}
// 	else{
// 		Arguments *temp = head;
// 		while(temp -> next!=NULL){
// 			temp = temp -> next;
// 		}
// 		Arguments *node = (Arguments*) malloc(sizeof(Arguments));
// 		node ->name = a;
// 		node -> next = NULL;		
// 		temp -> next = node;
// 		return head;
// 	}
	
// }

 void reverse(char* s)
 {
     int i, j;
     char c;
 
     for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
         c = s[i];
         s[i] = s[j];
         s[j] = c;
     }
 }

void itoa(int n, char* s)
 {
     int i, sign;
 
     if ((sign = n) < 0)  /* record sign */
         n = -n;          /* make n positive */
     i = 0;
     do {       /* generate digits in reverse order */
         s[i++] = n % 10 + '0';   /* get next digit */
     } while ((n /= 10) > 0);     /* delete it */
     if (sign < 0)
         s[i++] = '-';
     s[i] = '\0';
     reverse(s);
 }

int isLibFunc(SymbolTable* s,const char* text){
	ScopeList* slist= s->scopeHead;
	unsigned int num= 0;

	SymbolTableEntry * buffer= slist->symTableNode;

	while(num<12){
		
		if( strcmp(buffer->name, text )==0 ){
			return 1;
		}
		num++;
		buffer= buffer->nextInScope;
	}
	return 0;
}

void report_error(int lineno, char* text){
	fprintf(stderr,"\x1b[31mERROR line(%d): \'%s\' shadows a library function\n\x1b[0m", lineno, text);
}




scopespace_t currScopeSpace(void){
	if(scopeSpaceCounter == 1)
		return programvar;
	else if(scopeSpaceCounter%2 == 0)
		return formalarg;
	else 
		return functionLocal;
}

unsigned currScopeOffset(void){
	switch(currScopeSpace()){
		case programvar:
			return programVarOffset;
		case functionLocal:
			return functionLocalOffset;
		case formalarg:
			return formalArgOffset;
		default:
			assert(0);
	}
}

void incCurrScopeOffset(void){
	switch(currScopeSpace()){
		case programvar:
			++programVarOffset;
			break;
		case functionLocal:
			++functionLocalOffset;
			break;
		case formalarg:
			++formalArgOffset;
			break;
		default:
			assert(0);

	}
}

void enterScopeSpace(void){
	++scopeSpaceCounter;
}

void exitScopeSpace(void){
	assert(scopeSpaceCounter>1);
	--scopeSpaceCounter;
}

void reset_formalArgsOffset(void){
	formalArgOffset = 0;
}

void reset_functionLocalOffset(void){
	functionLocalOffset = 0;
}

unsigned get_functionLocalOffset(void){
	return functionLocalOffset;
}

void set_functionLocalOffset(unsigned  offset){
	functionLocalOffset = offset;
}

/*
int main(void){
			int i=0;
	SymbolTable* table = CreateTable();
	
	SymbolTableEntry* a = CreateNode("ohh", 0, 22, 2, NULL);
	Insert(table, table->scopeHead, a);


	a = CreateNode("darling", 1, 20, 2, NULL);
	Insert(table, table->scopeHead, a);

	a = CreateNode("what", 2, 20, 2, NULL);
	Insert(table, table->scopeHead, a);

	addScope(table->scopeHead, 3);
	a = CreateNode("have", 4, 20, 2, NULL);
	Insert(table, table->scopeHead, a);


	printTable(table);
	return 1;
}
/*
	a = CreateNode("i", 4, 20, 2, NULL);
	Insert(table, table->scopeHead, a);


	a = CreateNode("done", 0, 20, 2, NULL);
	Insert(table, table->scopeHead, a);

	a = CreateNode("done", 4, 20, 2, NULL);
	Insert(table, table->scopeHead, a);

	a = CreateNode("done", 5, 20, 2, NULL);
	Insert(table, table->scopeHead, a);

	a = CreateNode("done", 6, 20, 2, NULL);
	Insert(table, table->scopeHead, a);

	a = CreateNode("done", 7, 20, 2, NULL);
	Insert(table, table->scopeHead, a);

 	printScopeTable(table->scopeHead);
	

	printTable(table);

	//----------LOOKUPS tests---------------------*/

/*	if(ScopeLookup(table, "page", 1))
		fprintf(stdout, "page found in scope 1\n");
	else
		fprintf(stdout, "page NOT found in scope 1\n");
	

	if(ScopeLookup(table, "page", 30))
		fprintf(stdout, "page found in scope 30\n");
	else
		fprintf(stdout, "page NOT found in scope 30\n");

	if(ScopeLookup(table, "darling",1))
		fprintf(stdout, "darling found\n");
	else
		fprintf(stdout, "darling NOT found\n");


	return 0;


}
*/