#include "offset_stack.h"

void addOffsetNode(unsigned offset){
	offsetNode *tmp = malloc(sizeof(offsetNode));
	if(topHead==NULL){
		tmp->offset=offset;
		tmp->next=NULL;
		topHead= tmp;
	}
	else{
		tmp->offset=offset;
		tmp->next=topHead;
		topHead=tmp;
	}
}

unsigned popOffsetNode(){
	unsigned result;
	offsetNode *tmp;
	if(topHead==NULL){
		return 99999;
	}
	else{
		tmp=topHead;
		topHead=topHead->next;
		result = tmp->offset;
		free(tmp);
		return result;
	}
}

void printOffsets(){
	offsetNode *tmp = topHead;
	while(tmp!=NULL){
		fprintf(stderr, "%d\n", tmp->offset);
		tmp=tmp->next;
	}
	fprintf(stderr, "\n\n");
}