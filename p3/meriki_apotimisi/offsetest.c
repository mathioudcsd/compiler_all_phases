#include "offset_stack.h"

int main(){

	addOffsetNode(4);
	addOffsetNode(3);
	addOffsetNode(2);

	printOffsets();

	popOffsetNode();

	printOffsets();

	return 0;
}