#include <stdlib.h>
#include <stdio.h>

typedef struct bc_stack_s{
    unsigned label;
    struct bc_stack_s* prevLoop;
    struct bc_stack_s* nextbc;
}bc_stack;



bc_stack* top(bc_stack* head){
	return head;
}

unsigned top_label(bc_stack* head){
	if(head!=NULL)
		return head->nextbc->label;
	return 0;
}

void push(bc_stack* head){
	// printf("LOL\n");
	bc_stack* tmp;
	tmp=malloc(sizeof(bc_stack));
	if(tmp == NULL)
        exit(0);
	tmp->prevLoop = head;
	tmp->nextbc=NULL;
	tmp->label = 0;
	head=tmp;
}

void pop(bc_stack* head){
	if(head!=NULL)
		head = head->prevLoop;
	else
		printf("NO LOOPS\n");
}

void add(unsigned data, bc_stack* head){
	bc_stack* tmp = malloc(sizeof(bc_stack));
	tmp->nextbc = head->nextbc;
	head->nextbc=tmp;
	printf("LOL\n");
}

void printBC(bc_stack* head){
	// printf("LOL\n");
	bc_stack* tmpHead = head;
	bc_stack* curr;
	while(tmpHead!=NULL){
		curr = tmpHead->nextbc;
		while(curr!=NULL){
			printf("%d ", curr->label );
			curr=curr->nextbc;
		}
		printf("\n");
		tmpHead=tmpHead->prevLoop;
	}
}