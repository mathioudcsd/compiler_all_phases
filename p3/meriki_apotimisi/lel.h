#include <stdlib.h>
#include <stdio.h>

typedef struct bc_stack_s{
    unsigned label;
    struct bc_stack_s* prevLoop;
    struct bc_stack_s* nextbc;
}bc_stack;

void push(bc_stack* head, unsigned label);

void pop(bc_stack* head);

void add(bc_stack* head, unsigned data);

bc_stack* top(bc_stack* head);

unsigned top_label(bc_stack* head);

void printBC(bc_stack* head);