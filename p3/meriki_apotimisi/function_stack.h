#include <stdio.h>
#include <stdlib.h>
#include <string.h>
 
struct f_stack
{
    int data;
    char *f_name;
    struct f_stack* next;
    unsigned label;
};
 
/*
    init the stack
*/
void init(struct f_stack* head);
 
/*
    push an element into stack
*/
struct f_stack* push(struct f_stack* head,int data);

struct f_stack* push_bc(struct f_stack* head, int data, unsigned l);

struct f_stack* push_name(struct f_stack* head,char * name);

/*
    pop an element from the stack
*/
struct f_stack* pop(struct f_stack *head);
/*
    returns 1 if the stack is empty, otherwise returns 0
*/
int empty(struct f_stack* head);
 
/*
    display the stack content
*/
void display(struct f_stack* head);

int top(struct f_stack* head);

struct f_stack* top_struct(struct f_stack* head);

void increase_data(struct f_stack* head);