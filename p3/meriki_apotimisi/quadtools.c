#include "quadtools.h"
#include "tmpVars.h"
// #include "symtable.h"
//extern SymbolTable* table;
unsigned quadNo = 0;

// extern char* yytext;
/*
void emit(iopcode op, expr*	arg1, expr*	arg2,expr* result, unsigned	label, unsigned	line){
	if(currQuad == total)
		expand();

	quad* p	= quads*currQuad++;
	p->arg1	= arg1;
	p->arg2 = arg2;
	p->result = result;
	p->label =label;
	p->line = line;
}
*/

// void expand(void){
// 	assert (total==currQuad);
// 	quad* p=(quad*) malloc(NEW_SIZE);
// 	if(quads){
// 		memcpy(p, quads, CURR_SIZE);
// 		free(quads);
// 	}
// 	quads = p;
// 	total += EXPAND_SIZE;
// }

expr* newexpr(expr_t t){
	expr* e= (expr*) malloc(sizeof(expr));
	memset(e, 0, sizeof(expr));
	e->type= t;
	e->truelist= NULL;
	e->falselist= NULL;
	e->is_not_value= -1;
	// fprintf("YYTEXT: %s\n", yytext);
	// e->sym->name = malloc(20);

	return e;
}

expr* newexpr_conststring(char* s){
	expr* e= newexpr(conststring_e);
	e->strConst= strdup(s);
	return e;
}

expr* newexpr_constnum(double i){
	expr* e= newexpr(constnum_e);
	e->numConst=i;
	return e;

}

expr* newexpr_constbool(unsigned char c){
	expr* e= newexpr(constbool_e);
	e->boolConst=c;
	return e;
}

expr* newexpr_constnil(){
	expr* e= newexpr(nil_e);
	return e;
}


void myEmit(iopcode op, expr* arg1, expr* arg2,expr* result, unsigned jlabel, unsigned line){

	quad *curr;
	quad *tmp = malloc(sizeof(quad));
	tmp-> op = op;
	tmp->arg1 = arg1;
	tmp->arg2 = arg2;
	tmp->result = result; //ok
	tmp->label = getQuadNo();
	tmp->jLabel = jlabel;
	tmp->line = line;
	
	tmp->next = NULL;

	curr = quadHEAD;
	if(quadHEAD==NULL){
		quadHEAD=tmp;
	}
	else{
		while(curr->next!=NULL){
			curr=curr->next;
		}
		curr->next = tmp;
	}
}

quad* findQuad(unsigned label){
	quad *curr=quadHEAD;
	while(curr!=NULL){
		if(curr->label!=label){
			curr=curr->next;
		}
		else
			return curr;
	}
	return NULL;
}


unsigned getQuadNo(){
	return quadNo;
}

unsigned nextQuadNo(){
	return quadNo+1;
}

unsigned incrQuadNo(){
	quadNo +=1;
	return quadNo;
}

expr* emitArithExpr(iopcode op, expr* arg1, expr* arg2, int scope, int line){
	
	expr* exp = newexpr(var_e);
	exp->sym= CreateNode(bringTemp()->tname, scope, line, LOC); //tmpVar as LOC
	exp->sym->sym_type = var_s;
	exp->sym->space=currScopeSpace();
	exp->sym->offset=currScopeOffset();
	incCurrScopeOffset();
	Insert(table, table->scopeHead,exp->sym);
	exp->type= arithexpr_e;
	
	incrQuadNo();
	myEmit(op, arg1, arg2, exp, getQuadNo() ,line);
	return exp;
}

expr* emitBoolExpr(iopcode op, expr* arg1, expr* arg2, int scope, int line, unsigned char c){
	
	expr* exp = newexpr(boolexpr_e);
	exp->sym= CreateNode(bringTemp()->tname, scope, line, LOC); //tmpVar as LOC
	exp->sym->sym_type = var_s;
	exp->sym->space=currScopeSpace();
	exp->sym->offset=currScopeOffset();
	incCurrScopeOffset();
	Insert(table, table->scopeHead,exp->sym);
	exp->boolConst= c;
	
	incrQuadNo();
	myEmit(op, arg1, arg2, exp, getQuadNo() ,line);
	return exp;
}


expr* emitLogicOp(iopcode op, expr* arg1, expr* arg2, int scope, int line, unsigned jlabel){

	// 1st emit-> logic operation (if T, jump after 3rd emit)
	myEmit(op, arg1, arg2, NULL, jlabel+3, line);

	expr* exp = newexpr(boolexpr_e);
	exp->sym =  CreateNode(bringTemp()->tname, scope, line, LOC);
	exp->sym->sym_type = var_s;
	exp->sym->space=currScopeSpace();
	exp->sym->offset=currScopeOffset();
	incCurrScopeOffset();
	Insert(table, table->scopeHead,exp->sym);
	
	// 2nd emit-> assign F to temp
	incrQuadNo();
	myEmit(assign, newexpr_constbool('F') ,NULL, exp, getQuadNo() ,line);

	// 3rd emit-> jump after 4th emit
	incrQuadNo();
	emitJump(jlabel+4, line);

	// 4th emit-> assign T to temp
	incrQuadNo();
	myEmit(assign, newexpr_constbool('T') ,NULL, exp, getQuadNo() ,line);

	return exp;
}


expr* emitShortCircuitAssignment(expr* arg, int scope, int line){
	// emitLogicOp produces 3 standard emits.
//printf("SHORTCIRCUITASSIGNMENT\n");
	if(arg->falselist==NULL && arg->truelist==NULL){
		return arg;
	}

	expr* exp = newexpr(var_e);
	//expr* exp = newexpr(boolexpr_e);
	exp->sym =  CreateNode(bringTemp()->tname, scope, line, LOC);
	Insert(table, table->scopeHead,exp->sym);
	
	// 1st emit-> assign F to temp
	incrQuadNo();
	myEmit(assign, newexpr_constbool('T') ,NULL, exp, getQuadNo() ,line);

	backpatch(arg->truelist, getQuadNo());

	// 2nd emit-> jump after 3rd emit
	incrQuadNo();
	emitJump(getQuadNo()+2, line);

	// 3rd emit-> assign T to temp
	incrQuadNo();
	myEmit(assign, newexpr_constbool('F') ,NULL, exp, getQuadNo() ,line);

	backpatch(arg->falselist, getQuadNo());

	return exp;
}



unsigned emitIf(expr* arg, int line){
	// emitIf produces 2 standard emits.

	// 1st emit-> logic operation (if T, jump after 2rd emit)
	myEmit(if_eq, arg, newexpr_constbool('T'), NULL, getQuadNo()+2, line);

	// 2nd emit-> jump with invalid jlabel, to be patched
	incrQuadNo();
	emitJump(0, line);

	return getQuadNo();
}


void emitJump(unsigned jumpLabel, int line){
	myEmit(jump, NULL ,NULL, NULL, jumpLabel ,line);
}

expr* checkBoolExp(expr* exp){
	expr* tmp = newexpr(boolexpr_e);
	unsigned char boolean;
	if(exp->type==programfunc_e || exp->type==libraryfunc_e || exp->type==newtable_e ||  exp->type==tableitem_e){
		boolean='T';
	}
	else if(exp->type == constnum_e){
		if(exp->numConst==0)
			boolean ='F';
		else
			boolean ='T';
	}
	else if(exp->type == nil_e)
		boolean='T';
	else if(exp->type == conststring_e){
		if(strcmp(exp->strConst, "") == 0)
			boolean ='F';
		else
			boolean ='T';
	}else if(exp->type == var_e || exp->type == boolexpr_e || exp->type == arithexpr_e){
		tmp = exp;
		return tmp;
	}
	else 
		return NULL;

	tmp->boolConst = boolean;
	return tmp;

}



void patchlabel(unsigned quad_num, unsigned label){
	quad *q = findQuad(quad_num);
	
	if(q!=NULL)
		q->jLabel= label;
}



expr* emit_ifTableItem(expr *exp, int scope, int line){
	expr *res;
	if(exp->type != tableitem_e){
		return exp;
	}else{
		res = newexpr(var_e);
		res->sym =  CreateNode(bringTemp()->tname, scope, line, LOC);
		exp->sym->sym_type = var_s;
		exp->sym->space=currScopeSpace();
		exp->sym->offset=currScopeOffset();
		incCurrScopeOffset();
		Insert(table, table->scopeHead,res->sym);
		incrQuadNo();
		myEmit(tablegetelement, exp, exp->index, res, getQuadNo(), line);
	}

	return res;
}

expr* emitCall(expr* fun_name, param_expr* param_list, int scope, int line){
	expr* func = emit_ifTableItem(fun_name, scope, line );
	param_expr *curr;
	param_expr *rightmost;

	curr=param_list;
	while(curr->right!=NULL)
		curr=curr->right;
	
	while(curr!=NULL){
		incrQuadNo();
		myEmit(param, NULL, NULL, curr->exp,getQuadNo(), line);
		curr=curr->left;
	}

	incrQuadNo();	
	myEmit(call, NULL, NULL, func, getQuadNo(), line);

	expr* exp = newexpr(var_e);
	exp->sym= CreateNode(bringTemp()->tname, scope, line, LOC); //tmpVar as LOC
	exp->sym->sym_type = var_s;
	exp->sym->space=currScopeSpace();
	exp->sym->offset=currScopeOffset();
	incCurrScopeOffset();
	Insert(table, table->scopeHead,exp->sym);

	incrQuadNo();
	myEmit(getretval, NULL, NULL, exp, getQuadNo(), line);

	return exp;
}



expr* member_item(expr *lvalue, char *name , int scope, int line){
	lvalue = emit_ifTableItem(lvalue,scope, line);
	expr *item = newexpr(tableitem_e);
	item->sym = lvalue->sym;
	item->index = newexpr_conststring(name);
	return item;
}

struct f_stack* patchlabel_bc(struct f_stack* list, unsigned label, int loopnum){
	struct f_stack *current;
    current = list;
    if(current!= NULL){
        do{
        	if(current->data!= loopnum)
        		break;
            else{
            	patchlabel(current->label, label);
            	list= pop(list);
            }
            	
            current = current->next;
        }
        while (current!= NULL);
    }
    return list;
}

/* TA KAINOURIA STHN ARXH TIS LISTAS */
void insert_param(expr* this){
	param_expr* tmp = malloc(sizeof(param_expr));
	tmp->exp = this;

	if(elist_paramets==NULL){
		tmp->left=NULL;
		tmp->right = NULL;
		elist_paramets = tmp;
	}else{
		tmp->left=NULL;
		tmp->right = elist_paramets;
		elist_paramets->left=tmp;
		elist_paramets = tmp;
	}
}


void free_param(){
	param_expr* tmp = elist_paramets;
	param_expr* curr = elist_paramets;
	while(curr->right!= NULL){
		curr = curr->right;
		free(tmp);
		tmp = curr;
	}
	free(curr);
}

void insert_indexed(expr* exp1, expr* exp2){
	indexed_expr* curr;
	indexed_expr* tmp = malloc(sizeof(indexed_expr));
	tmp->exp1 = exp1;
	tmp->exp2 = exp2;
	if(indexed_paramets==NULL){
		tmp->next=NULL;
		indexed_paramets=tmp;
	}
	else{
		curr=indexed_paramets;
		while(curr->next!=NULL)
			curr=curr->next;
		curr->next=tmp;
		tmp->next = NULL;
		// indexed_paramets = tmp;	
	}

}

void free_indexed(){
	indexed_expr* tmp = indexed_paramets;
	indexed_expr* curr = indexed_paramets;
	while(curr->next!= NULL){
		curr = curr->next;
		free(tmp);
		tmp = curr;
	}
	free(curr);
}


ft_list* add_ft_node(ft_list* list, unsigned label){
	ft_list* node= malloc(sizeof(ft_list));
	node->label= label;
	node->next= list;

	return node;
}


ft_list* merge_ft(ft_list* l1, ft_list* l2){
	ft_list* iter= l1;

	if(iter==NULL)
		return l2;

	while(iter->next!=NULL){
		iter= iter->next;
	}
	iter->next= l2;

	return l1;
}


void print_ft(ft_list* list){
	ft_list* iter= list;

	while(iter!=NULL){
		printf("%d. ", iter->label);
		iter= iter->next;
	}	
	printf("\n");
}


void backpatch(ft_list* list, unsigned jlabel){
	while(list!=NULL){
		patchlabel(list->label, jlabel);
		list= list->next;
	}
}


expr* apply_not(expr* exp){
	ft_list* tmp;

	tmp= exp->truelist;
	exp->truelist= exp->falselist;
	exp->falselist= tmp;	
//printf("APPLY_NOT_FUNTION\n");
	return exp;
}


unsigned is_not_expr(expr* exp){
	if(exp->is_not_value==1)
		return 1;
	else
		return 0;

}


expr* toggle_not_expr(expr* exp){
	exp->is_not_value *= -1;

	return exp;
}



void print_helper(expr* exp){
	
	if(exp->type == constnum_e)
		printf(" %f", exp->numConst);

	else if(exp->type == constbool_e)
		printf(" %c", exp->boolConst);

	else if(exp->type == conststring_e)
		printf(" %s", exp->strConst);

	else if( exp->type == assignexpr_e || exp->type == var_e || exp->type == boolexpr_e || exp->type == tableitem_e || exp->type == programfunc_e || exp->type == libraryfunc_e || exp->type == arithexpr_e || exp->type == newtable_e)
		printf(" %s", exp->sym->name);

	else if(exp->type == nil_e)
		printf(" NULL");

	

}




void printQuads(){
	quad *curr=quadHEAD;
	while(curr!=NULL){
		switch(curr->op){
			case 0:
				printf("%d: ASSIGN", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->result);
				printf("\n");
				break;
			case 1:
				printf("%d: ADD", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->arg2);
				print_helper(curr->result);
				printf("\n");
				break;
			case 2:
				printf("%d: SUB", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->arg2);
				print_helper(curr->result);
				printf("\n");
				break;
			case 3:
				printf("%d: MUL", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->arg2);
				print_helper(curr->result);
				printf("\n");
				break;
			case 4:
				printf("%d: DIV", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->arg2);
				print_helper(curr->result);
				printf("\n");
				break;
			case 5:
				printf("%d: MOD", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->arg2);
				print_helper(curr->result);
				printf("\n");
				break;
			case 6:
			printf("%d: UMINUS", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->result);
				printf("\n");
				break;
			case 7:
				printf("%d: AND", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->arg2);
				print_helper(curr->result);
				printf("\n");
				break;
			case 8:
				printf("%d: OR", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->arg2);
				print_helper(curr->result);
				printf("\n");
				break;
			case 9: //not
				printf("%d: NOT", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->result);
				printf("\n");
				break;
			case 10:
				printf("%d: IF_EQ", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->arg2);
				printf(" %d\n", curr->jLabel);
				break;
			case 11:
				printf("%d: IF_NOTEQ", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->arg2);
				printf(" %d\n", curr->jLabel);
				break;
			case 12:
				printf("%d: IF_LESSEQ", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->arg2);
				printf(" %d\n", curr->jLabel);
				break;
			case 13:
				printf("%d: IF_GREATEREQ", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->arg2);
				printf(" %d\n", curr->jLabel);
				break;
			case 14:
				printf("%d: IF_LESS", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->arg2);
				printf(" %d\n", curr->jLabel);
				break;
			case 15:
				printf("%d: IF_GREATER", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->arg2);
				printf(" %d\n", curr->jLabel);
				break;
			case 16: /* call */
				printf("%d: CALL", curr->label);
				print_helper(curr->result);
				printf("\n");
				break;
			case 17: /* param */
				printf("%d: PARAM", curr->label);
				print_helper(curr->result);
				printf("\n");
				break;
			case 18: /* ret */

				printf("%d: RET", curr->label);
				if(curr->result != NULL)
					print_helper(curr->result);
				printf("\n");
				break;
			case 19: /* getretval */
				printf("%d: GETRETVAL", curr->label);
				print_helper(curr->result);
				printf("\n");
				break;
			case 20: /* funcstart */
				printf("%d: FUNCSTART", curr->label);
				print_helper(curr->result);
				printf("\n");
				break;
			case 21: /* funcstart */
				printf("%d: FUNCEND", curr->label);
				print_helper(curr->result);
				printf("\n");
				break;
			case 22: /* tablecreate*/
				printf("%d: TABLECREATE", curr->label);
				print_helper(curr->result);
				printf("\n");
				break;
			case 23: /* tablegetelement */
				printf("%d: TABLEGETELEMENT", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->arg2);
				print_helper(curr->result);
				printf("\n");
				break;
			case 24: /* tablesetelement */
				printf("%d: TABLESETELEMENT", curr->label);
				print_helper(curr->arg1);
				print_helper(curr->arg2);
				print_helper(curr->result);
				printf("\n");
				break;
			case 25: 	/* Jmp */
				printf("%d: JUMP", curr->label);
				printf(" %d\n", curr->jLabel);
				break;
		}
		curr=curr->next;
	}
}





void f_print_helper(expr* exp, FILE *fd){
	char a =' ';
	if(exp==NULL)
		fprintf(fd, "\t-");

	else if(exp->type == constnum_e)
		fprintf(fd, "\t%f", exp->numConst);

	else if(exp->type == constbool_e)
		fprintf(fd, "\t%c", exp->boolConst);

	else if(exp->type == conststring_e)
		fprintf(fd, "\t%s", exp->strConst);

	else if( exp->type == assignexpr_e || exp->type == var_e || exp->type == boolexpr_e || exp->type == tableitem_e || exp->type == programfunc_e || exp->type == libraryfunc_e || exp->type == arithexpr_e || exp->type == newtable_e)
		fprintf(fd, "\t%s", exp->sym->name);

	else if(exp->type == nil_e)
		fprintf(fd, "\tNULL");

}



/* ALLAGES */


void printQuads_to_file(){
	quad *curr=quadHEAD;
	FILE* fd= fopen("output.icode", "w");

	if(fd==NULL)
		return;

	fprintf(fd, "LABEL: OPCODE\t ARG1\t ARG2\t RESULT\t JUMPLABEL\n");

	while(curr!=NULL){
		switch(curr->op){
			case 0:
				fprintf(fd, "%d:\tASSIGN", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 1:
				fprintf(fd, "%d:\tADD", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 2:
				fprintf(fd, "%d:\tSUB", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 3:
				fprintf(fd, "%d:\tMUL", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 4:
				fprintf(fd, "%d:\tDIV", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 5:
				fprintf(fd, "%d:\tMOD", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 6:
				fprintf(fd, "%d:\tUMINUS", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 7:
				fprintf(fd, "%d:\tAND", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 8:
				fprintf(fd, "%d:\tOR", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 9: //not
				fprintf(fd, "%d:\tNOT", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 10:
				fprintf(fd, "%d:\tIF_EQ", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 11:
				fprintf(fd, "%d:\t\tIF_NOTEQ", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 12:
				fprintf(fd, "%d:\tIF_LESSEQ", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 13:
				fprintf(fd, "%d:\tIF_GREATEREQ", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 14:
				fprintf(fd, "%d:\tIF_LESS", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 15:
				fprintf(fd, "%d:\tIF_GREATER", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 16: /* call */
				fprintf(fd, "%d:\tCALL", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 17: /* param */
				fprintf(fd, "%d:\tPARAM", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 18: /* ret */
				fprintf(fd, "%d:\tRET", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 19: /* getretval */
				fprintf(fd, "%d:\tGETRETVAL", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 20: /* funcstart */
				fprintf(fd, "%d:\tFUNCSTART", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 21: /* funcstart */
				fprintf(fd, "%d:\tFUNCEND", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 22: /* tablecreate*/
				fprintf(fd, "%d:\tTABLECREATE", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 23: /* tablegetelement */
				fprintf(fd, "%d:\tTABLEGETELEMENT", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 24: /* tablesetelement */
				fprintf(fd, "%d:\tTABLESETELEMENT", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
			case 25: 	/* Jmp */
				fprintf(fd, "%d:\tJUMP", curr->label);
				f_print_helper(curr->arg1, fd);
				f_print_helper(curr->arg2, fd);
				f_print_helper(curr->result, fd);
				fprintf(fd, "\t%d\n", curr->jLabel);
				break;
		}
		curr=curr->next;
	}

	fclose(fd);
}