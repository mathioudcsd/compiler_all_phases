/* tmpVars.h */
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

typedef struct tmpVar_s{
	char *tname;
	struct tmpVar_s *next;
}tmpVar_t;


int tempNameCounter;

tmpVar_t *activeTmpHEAD;
tmpVar_t *inactTmpHEAD;

tmpVar_t* newTempName();

void addToActiveTempList( tmpVar_t* t);

void removeFromActiveTempList(tmpVar_t* t);

void removeFromActiveTempList_n(char* s);

tmpVar_t* bringTemp();

void printTmpLists();

void resetTemps();

void printHEADS();

void initHeads();

void popActive();