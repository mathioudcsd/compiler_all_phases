#include "function_stack.h"

/*
    init the stack
*/
void init(struct f_stack* head)
{
    head = NULL;
}
 
/*
    push an element into stack
*/
struct f_stack* push(struct f_stack* head,int data)
{
    struct f_stack* tmp = (struct f_stack*)malloc(sizeof(struct f_stack));
    if(tmp == NULL)
    {   
        exit(0);
    }
    tmp->data = data;
    tmp->next = head;
    head = tmp;
    return head;
}


struct f_stack* push_bc(struct f_stack* head, int data, unsigned l){
    struct f_stack* tmp = (struct f_stack*)malloc(sizeof(struct f_stack));
    if(tmp == NULL)
    {   
        exit(0);
    }
    tmp->data = data;
    tmp->next = head;
    tmp->label = l;
    head = tmp;
    return head;
}


struct f_stack* push_name(struct f_stack* head,char * name){
    struct f_stack* tmp = (struct f_stack*)malloc(sizeof(struct f_stack));
    if(tmp == NULL)
    {   
        exit(0);
    }
    tmp->f_name = malloc( strlen(name));
    strcpy(tmp->f_name ,name);

    tmp->next = head;
    head = tmp;
    return head;
}


/*
    pop an element from the stack
*/
struct f_stack* pop(struct f_stack *head)
{
    struct f_stack* tmp = head;
    if(tmp==NULL){
        return NULL;
    }
    head = head->next;
    free(tmp);
    return head;
}
/*
    returns 1 if the stack is empty, otherwise returns 0
*/
int empty(struct f_stack* head)
{
    return head == NULL ? 1 : 0;
}
 
/*
    display the stack content
*/
void display(struct f_stack* head)
{
    struct f_stack *current;
    current = head;
    if(current!= NULL)
    {
        do
        {
            printf("%d -",current->data);
            printf("%d -",current->label);
            printf("%s \n",current->f_name);
            current = current->next;
        }
        while (current!= NULL);
        printf("\n");
    }
    else
        printf("EMPTY STACK!!!!!!!!!!!!!!!!\n");
}

int top(struct f_stack* head){
	if(empty(head))
		return -1;

	return head->data;
}

struct f_stack* top_struct(struct f_stack* head){
    if(empty(head))
        return NULL;

    return head;
}

void increase_data(struct f_stack* head){
    if(!empty(head))
        head->data++;
}
