#include "quadtools.h"
#include "tmpVars.h"

int main(){

	initHeads();
	tmpVar_t *use;
	tmpVar_t *t0;
	tmpVar_t *t1;
	tmpVar_t *t2;
	tmpVar_t *t3;
	tmpVar_t *t4;

	/*
	t0=newTempName();
	printTmpLists();
	t1=newTempName();
	printTmpLists();
	t2=newTempName();
	printTmpLists();
	t3=newTempName();
	printTmpLists();
	t4=newTempName();
	printTmpLists();
	*/
	//printHEADS();



	use = bringTemp();
	use = bringTemp();
	use = bringTemp();
	use = bringTemp();
	use = bringTemp();
	use = bringTemp();
	use = bringTemp();
	use = bringTemp();
	use = bringTemp();
	
	printTmpLists();
	popActive();
	printTmpLists();
	popActive();
	printTmpLists();
	popActive();
	printTmpLists();
	bringTemp();
	printTmpLists();
	

	resetTemps();
	printTmpLists();
	bringTemp();
	printTmpLists();
	return 0;
}
