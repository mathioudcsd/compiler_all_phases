%{

/*	
 *
 *	Project Name: Lexical Analysis cs340
 *	Authors: 
 *			Fotios Mathioudakis
 *			Leonidas Anagnostou
 *			Evaggelia Athanasaki
 */






/* ========== C  CODE (includes etc) =========== */
// #include "symtable.h"
//#include "function_stack.h"
#include "quadtools.h"
#include "tmpVars.h"
#include "offset_stack.h"



	int yyerror (char* yaccProvidedMessage);
//	int alpha_yylex(void* yylval);
	int yylex(void);



	extern int yylineno;
	extern char* yytext;
	extern FILE* yyin;
	//extern unsigned int scope=0;
	extern SymbolTable* table;
	//SymbolTable* table;
	SymbolTableEntry* entry;
	unsigned int funcCounter=0;
	char* tempname;
	struct f_stack* in_function=NULL;
	struct f_stack* f_names=NULL;
	struct f_stack* breaklist=NULL;
	struct f_stack* contlist=NULL;
	int inside_function=0;
	int inside_loop=0;
	int is_func = 0;
	//int func_lv= 0;
	int id_is_lib= 0;
	unsigned int varexists=0;
	unsigned int is_var=1;
	unsigned int is_call=0;
	extern param_expr* elist_paramets;
	extern indexed_expr* indexed_paramets;
	unsigned call_method = 0;
	char *call_method_name = NULL;

	int stop_parser(){
		fprintf(stderr, "\x1b[31mcompilation failed\x1b[0m\n");
		return 1;
	}

/* ========== YACC definitions etc =========== */
%}

%start program

%union {
	int integer;
	double real;
	char* string;
	struct expr_s* expression;
	unsigned unsigned_t;

	struct SymbolTableEntry* symboltableentry;

	struct label_carrier* carrier_t;


	//enum iopcode_e iopcode_t;
}

%token IF
%token ELSE
%token WHILE
%token FOR
%token FUNCTION
%token RETURN
%token BREAK
%token CONTINUE
%token AND
%token NOT
%token OR
%token LOCAL
%token TRUE
%token FALSE
%token NIL

%token ASSIGN
%token PLUS
%token MINUS
%token MUL
%token DIV
%token MOD
%token EQ
%token NEQ
%token PP
%token MM
%token GREATER
%token LESS
%token GEQ
%token LEQ

%token <integer> INTEGER
%token <real> REAL
%token <string> ID
%token <string> STR

%token WHITESPACE

%token BRKT_O
%token BRKT_C
%token SBRKT_O
%token SBRKT_C
%token PAR_O
%token PAR_C
%token SEMICOL
%token COMMA
%token COLON
%token COLON2
%token DOT
%token DOT2

%nonassoc LOWER_THAN_ELSE
%nonassoc ELSE


%{/* TOP(low) -> BOTTOM(high) */%}
%right ASSIGN
%left OR
%left AND
%nonassoc EQ NEQ
%nonassoc GREATER LESS GEQ LEQ
%left PLUS MINUS
%left MUL DIV MOD
%{/* warning!! check the MINUS CASE */%}
%right NOT PP MM UMINUS
%left DOT DOT2
%left SBRKT_O SBRKT_C
%left PAR_O PAR_C


%type <expression> lvalue 
%type <expression> member
%type <expression> primary
%type <expression> assignexpr
%type <expression> call
%type <expression> term
%type <expression> tablemake
%type <expression> const
%type <expression> expr
%type <expression> callsuffix
%type <expression> normcall
%type <expression> methodcall

%type <symboltableentry> funcdef

%type <unsigned_t> ifprefix

%type <unsigned_t> elseprefix
%type <unsigned_t> whilestart
%type <unsigned_t> whilecont
%type <unsigned_t> FOR_M
%type <unsigned_t> FOR_JMP
%type <unsigned_t> formiddle
%type <unsigned_t> M_BOOL

%type <carrier_t> forprefix

%%



program:			programcont												{/*fprintf(stdout,"program -> programcont\n");*/}
					| 														{/*fprintf(stdout,"program -> \n");*/}
					;

programcont:			stmt												{/*fprintf(stdout,"programcont -> stmt\n");*/}
					| programcont stmt 										{/*fprintf(stdout,"programcont -> programcont stmt\n");*/}
					;


stmt:				expr SEMICOL											{/*fprintf(stdout,"stmt -> expr;\n");*/}
					| ifstmt												{/*fprintf(stdout,"stmt -> ifstmt\n");*/}
					| whilestmt												{/*fprintf(stdout,"stmt -> whilestmt\n");*/}
					| forstmt												{/*fprintf(stdout,"stmt -> forstmt\n");*/}
					| returnstmt											{/*fprintf(stdout,"stmt -> returnstmt\n");*/}
					| BREAK SEMICOL											{	/*fprintf(stdout,"stmt -> break;\n");*/

																				if(!inside_loop){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): Use of \'break\' while not in a loop\n",yylineno);
																					return stop_parser();
																				}

																				incrQuadNo();
																				emitJump(0, yylineno);
																				breaklist= push_bc(breaklist, inside_loop, getQuadNo());

																			}

					| CONTINUE SEMICOL										{	//fprintf(stdout,"stmt -> continue;\n");
																				if(!inside_loop){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): Use of \'continue\' while not in a loop\n",yylineno);
																					return stop_parser();
																				}

																				incrQuadNo();
																				emitJump(0, yylineno);
																				contlist= push_bc(contlist, inside_loop, getQuadNo());
																			}
					| block													{/*fprintf(stdout,"stmt -> block\n");*/}
					| funcdef												{/*fprintf(stdout,"stmt -> funcdef\n");*/}
					| SEMICOL												{/*fprintf(stdout,"stmt -> \n");*/}
					;


M_BOOL:																		{	$$= nextQuadNo();	}
					;


expr:				assignexpr												{/*fprintf(stdout,"expr -> assignexpr\n");*/
																				 
																			}

					
					| expr PLUS expr										{	/*fprintf(stdout,"expr -> expr arithop expr\n");*/
																				if($3->type==constnum_e && $3->numConst==0 ){
																					fprintf(stderr,"\x1b[31mRUNTIME ERROR\x1b[0m line(%d): Cannot Divide by Zero!\n",yylineno);
																					return stop_parser();
																				}	
																				else						
																					$$ = emitArithExpr(add, $1, $3, scope, yylineno); 
																			}
					| expr MINUS expr										{	//fprintf(stdout,"expr -> expr arithop expr\n");
																				if($3->type==constnum_e && $3->numConst==0 ){
																					fprintf(stderr,"\x1b[31mRUNTIME ERROR\x1b[0m line(%d): Cannot Divide by Zero!\n",yylineno);
																					return stop_parser();
																				}	
																				else						
																					$$ = emitArithExpr(sub, $1, $3, scope, yylineno); 
																			}
					| expr MUL expr										{	//fprintf(stdout,"expr -> expr arithop expr\n");
																				if($3->type==constnum_e && $3->numConst==0 ){
																					fprintf(stderr,"\x1b[31mRUNTIME ERROR\x1b[0m line(%d): Cannot Divide by Zero!\n",yylineno);
																					return stop_parser();
																				}	
																				else						
																					$$ = emitArithExpr(mul, $1, $3, scope, yylineno); 
																			}
					| expr DIV expr										{	//fprintf(stdout,"expr -> expr arithop expr\n");
																				if($3->type==constnum_e && $3->numConst==0 ){
																					fprintf(stderr,"\x1b[31mRUNTIME ERROR\x1b[0m line(%d): Cannot Divide by Zero!\n",yylineno);
																					return stop_parser();
																				}	
																				else						
																					$$ = emitArithExpr(Div, $1, $3, scope, yylineno); 
																			}
					| expr MOD expr										{	//fprintf(stdout,"expr -> expr arithop expr\n");
																				if($3->type==constnum_e && $3->numConst==0 ){
																					fprintf(stderr,"\x1b[31mRUNTIME ERROR\x1b[0m line(%d): Cannot Divide by Zero!\n",yylineno);
																					return stop_parser();
																				}	
																				else						
																					$$ = emitArithExpr(mod, $1, $3, scope, yylineno); 
																			}


					| expr GREATER expr 									{	//fprintf(stdout,"expr -> expr relop expr\n");

																				expr* exp= newexpr(boolexpr_e);

																				incrQuadNo();
																				myEmit(if_greater, $1, $3, NULL, 0, yylineno);
																				exp->truelist= add_ft_node(NULL, getQuadNo());

																				incrQuadNo();
																				emitJump(0, yylineno);
																				exp->falselist= add_ft_node(NULL, getQuadNo());
																				$$= exp;

																				// printf("GREATER--------\n");
																				// print_ft(exp->truelist);
																				// print_ft(exp->falselist);
																				// printf("END-GREATER----\n");
																			}


					| expr LESS expr 										{	//fprintf(stdout,"expr -> expr relop expr\n");
																				expr* exp= newexpr(boolexpr_e);

																				incrQuadNo();
																				myEmit(if_less, $1, $3, NULL, 0, yylineno);
																				exp->truelist= add_ft_node(NULL, getQuadNo());

																				incrQuadNo();
																				emitJump(0, yylineno);
																				exp->falselist= add_ft_node(NULL, getQuadNo());
																				$$= exp;
																			}


					| expr GEQ expr 										{	//fprintf(stdout,"expr -> expr relop expr\n");
																				expr* exp= newexpr(boolexpr_e);

																				incrQuadNo();
																				myEmit(if_greatereq, $1, $3, NULL, 0, yylineno);
																				exp->truelist= add_ft_node(NULL, getQuadNo());

																				incrQuadNo();
																				emitJump(0, yylineno);
																				exp->falselist= add_ft_node(NULL, getQuadNo());
																				$$= exp;
																			}


					| expr LEQ expr 										{	//fprintf(stdout,"expr -> expr relop expr\n");
																				expr* exp= newexpr(boolexpr_e);

																				incrQuadNo();
																				myEmit(if_lesseq, $1, $3, NULL, 0, yylineno);
																				exp->truelist= add_ft_node(NULL, getQuadNo());

																				incrQuadNo();
																				emitJump(0, yylineno);
																				exp->falselist= add_ft_node(NULL, getQuadNo());
																				$$= exp;
																			}


					| expr EQ expr 											{	//fprintf(stdout,"expr -> expr relop expr\n");
																				expr* exp= newexpr(boolexpr_e);

																				incrQuadNo();
																				myEmit(if_eq, $1, $3, NULL, 0, yylineno);
																				exp->truelist= add_ft_node(NULL, getQuadNo());

																				incrQuadNo();
																				emitJump(0, yylineno);
																				exp->falselist= add_ft_node(NULL, getQuadNo());
																				$$= exp;
																			}


					| expr NEQ expr 										{	//fprintf(stdout,"expr -> expr relop expr\n");
																				expr* exp= newexpr(boolexpr_e);

																				incrQuadNo();
																				myEmit(if_noteq, $1, $3, NULL, 0, yylineno);
																				exp->truelist= add_ft_node(NULL, getQuadNo());

																				incrQuadNo();
																				emitJump(0, yylineno);
																				exp->falselist= add_ft_node(NULL, getQuadNo());
																				$$= exp;
																			}



					| expr AND M_BOOL expr 									{	//fprintf(stdout,"expr -> expr and expr\n");
																				
																				expr* exp1 = $1;
																				expr* exp2 = $4;
																				unsigned jlabel = $3;

																				if(exp1->type!=boolexpr_e){
																					exp1= newexpr(boolexpr_e);

																					incrQuadNo();
																					myEmit(if_eq, $1, newexpr_constbool('T'), NULL, 0, yylineno);
																					exp1->truelist= add_ft_node(NULL, getQuadNo());

																					incrQuadNo();
																					emitJump(0, yylineno);
																					exp1->falselist= add_ft_node(NULL, getQuadNo());

																					if(is_not_expr($1) ){
																						//printf("1st EXPRESSION---------\n");
																						exp1= apply_not(exp1);
																					}

																					// increase by 2, because we manually added 2 emits
																					jlabel+= 2;
																				}

																				if(exp2->type!=boolexpr_e){
																					exp2= newexpr(boolexpr_e);

																					incrQuadNo();
																					myEmit(if_eq, $4, newexpr_constbool('T'), NULL, 0, yylineno);
																					exp2->truelist= add_ft_node(NULL, getQuadNo());

																					incrQuadNo();
																					emitJump(0, yylineno);
																					exp2->falselist= add_ft_node(NULL, getQuadNo());

																					if(is_not_expr($4) ){
																						//printf("2nd EXPRESSION---------\n");
																						exp2= apply_not(exp2);
																					}
																				}

																				expr* exp= newexpr(boolexpr_e);

																				backpatch(exp1->truelist, jlabel);
																				exp->truelist= exp2->truelist;
																				exp->falselist= merge_ft(exp1->falselist, exp2->falselist);
																				$$= exp;

																			}

					| expr OR M_BOOL expr 									{	//fprintf(stdout,"expr -> expr or expr\n");

																				expr* exp1 = $1;
																				expr* exp2 = $4;
																				unsigned jlabel = $3;

																				if(exp1->type==var_e){
																					exp1= newexpr(boolexpr_e);

																					incrQuadNo();
																					myEmit(if_eq, $1, newexpr_constbool('T'), NULL, 0, yylineno);
																					exp1->truelist= add_ft_node(NULL, getQuadNo());

																					incrQuadNo();
																					emitJump(0, yylineno);
																					exp1->falselist= add_ft_node(NULL, getQuadNo());

																					if(is_not_expr($1) ){
																						//printf("1st EXPRESSION---------\n");
																						exp1= apply_not(exp1);
																					}

																					// increase by 2, because we manually added 2 emits
																					jlabel+= 2;
																				}

																				if(exp2->type==var_e){
																					exp2= newexpr(boolexpr_e);

																					incrQuadNo();
																					myEmit(if_eq, $4, newexpr_constbool('T'), NULL, 0, yylineno);
																					exp2->truelist= add_ft_node(NULL, getQuadNo());

																					incrQuadNo();
																					emitJump(0, yylineno);
																					exp2->falselist= add_ft_node(NULL, getQuadNo());

																					if(is_not_expr($4)){
																						//printf("2nd EXPRESSION---------\n");
																						exp2= apply_not(exp2);
																					}
																				}
																				
																				expr* exp= newexpr(boolexpr_e);

																				backpatch(exp1->falselist, jlabel);
																				exp->falselist= exp2->falselist;
																				exp->truelist= merge_ft(exp1->truelist, exp2->truelist);
																				$$= exp;

																			}

					| term													{//fprintf(stdout,"expr -> term\n");
																				$$ = $1;
																			}
					;



term:				PAR_O expr PAR_C										{//fprintf(stdout,"term -> (expr)\n");
																				//$$ = emitArithExpr(mod, $1, $3, scope, yylineno);
																				//myEmit(add, $2, arg2, $2, getQuadNo() , yylineno);
																				//$$=$2;
																				$$ = emitShortCircuitAssignment($2, scope, yylineno);;
																			}
					| MINUS expr %prec UMINUS								{//fprintf(stdout,"term -> -expr\n");

																				if($2->type == arithexpr_e || $2->type == constnum_e || $2->type == var_e ){
																					$$ = emitArithExpr(uminus, $2, NULL, scope, yylineno);
																				}
																				else{
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): cannot have \n",yylineno );
																					return stop_parser();
																				}
																				// if($2->type == arithexpr_e || $2->type == constnum_e || $2->type == var_e ){
																				// 	incrQuadNo();
																				// 	myEmit(uminus, $2, NULL , $2 , getQuadNo(), yylineno);

																				// 	$$ = $2;
																				// }       
																				//error?
																			}

					| NOT expr 												{	//fprintf(stdout,"term -> not expr\n");
																				if($2->type!=boolexpr_e){
																					$2=toggle_not_expr($2);
																					$$= $2;
																				}else{
																					$$= apply_not($2);		
																				}			
																			}
					| PP lvalue 											{//fprintf(stdout,"term -> ++lvalue\n");
																				if(is_func){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																					return stop_parser();
																				}
																				expr* tmp = newexpr(arithexpr_e);
																				tmp->sym= CreateNode(bringTemp()->tname, scope, yylineno, LOC); //tmpVar as LOC
																				tmp->sym->sym_type = var_s;
																				tmp->sym->space=currScopeSpace();
																				tmp->sym->offset=currScopeOffset();
																				incCurrScopeOffset();
																				Insert(table, table->scopeHead,tmp->sym);
																				if($2->type == tableitem_e){
																					tmp = emit_ifTableItem($2, scope, yylineno);
																					incrQuadNo();
																					myEmit(add, tmp, newexpr_constnum(1), tmp, getQuadNo(), yylineno);
																					incrQuadNo();
																					myEmit(tablesetelement,$2,$2->index, tmp,getQuadNo(), yylineno);
																				}else{
																					incrQuadNo();
																					myEmit(add, $2, newexpr_constnum(1), $2, getQuadNo() , yylineno);

																					incrQuadNo();
																					myEmit(assign, $2, NULL, tmp, getQuadNo() , yylineno);
																				}
																				$$ = tmp; //mallon ksana

																			}
					| lvalue PP 											{//fprintf(stdout,"term -> lvalue++\n");
																				if(is_func){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																					return stop_parser();
																				}

																				expr* tmp = newexpr(var_e);
																				tmp->sym= CreateNode(bringTemp()->tname, scope, yylineno, LOC); //tmpVar as LOC
																				tmp->sym->sym_type = var_s;
																				tmp->sym->space=currScopeSpace();
																				tmp->sym->offset=currScopeOffset();
																				incCurrScopeOffset();
																				Insert(table, table->scopeHead,tmp->sym);
																				if($1->type == tableitem_e){
																					expr *value = emit_ifTableItem($1, scope, yylineno);
																					incrQuadNo();	
																					myEmit(assign, value, NULL ,tmp, getQuadNo(), yylineno);
																					incrQuadNo();
																					myEmit(add, value, newexpr_constnum(1), value, getQuadNo(), yylineno);
																					incrQuadNo();
																					myEmit(tablesetelement,$1,$1->index, value,getQuadNo(), yylineno);
																				}else{
																					incrQuadNo();
																					myEmit(assign, $1, NULL, tmp, getQuadNo() , yylineno);
																					
																					incrQuadNo();
																					myEmit(add,$1, newexpr_constnum(1), $1, getQuadNo() ,yylineno);
																				}
																				$$= tmp; //mallon ayto edw

																			}
					| MM lvalue												{//fprintf(stdout,"term -> --lvalue\n");
																				if(is_func){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																					return stop_parser();
																				}
																				expr* tmp = newexpr(arithexpr_e);
																				tmp->sym= CreateNode(bringTemp()->tname, scope, yylineno, LOC); //tmpVar as LOC
																				tmp->sym->sym_type = var_s;
																				tmp->sym->space=currScopeSpace();
																				tmp->sym->offset=currScopeOffset();
																				incCurrScopeOffset();
																				Insert(table, table->scopeHead,tmp->sym);
																				if($2->type == tableitem_e){
																					tmp = emit_ifTableItem($2, scope, yylineno);
																					incrQuadNo();
																					myEmit(sub, tmp, newexpr_constnum(1), tmp, getQuadNo(), yylineno);
																					incrQuadNo();
																					myEmit(tablesetelement,$2,$2->index, tmp,getQuadNo(), yylineno);
																				}else{
																					incrQuadNo();
																					myEmit(sub, $2, newexpr_constnum(1), $2, getQuadNo() , yylineno);

																					incrQuadNo();
																					myEmit(assign, $2, NULL, tmp, getQuadNo() , yylineno);
																				}
																				$$ = tmp; //mallon ksana

																			}
					| lvalue MM 											{//fprintf(stdout,"term -> lvalue--\n");
																				if(is_func){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																					return stop_parser();
																				}

																				expr* tmp = newexpr(var_e);
																				tmp->sym= CreateNode(bringTemp()->tname, scope, yylineno, LOC); //tmpVar as LOC
																				tmp->sym->sym_type = var_s;
																				tmp->sym->space=currScopeSpace();
																				tmp->sym->offset=currScopeOffset();
																				incCurrScopeOffset();
																				Insert(table, table->scopeHead,tmp->sym);
																				if($1->type == tableitem_e){
																					expr *value = emit_ifTableItem($1, scope, yylineno);
																					incrQuadNo();	
																					myEmit(assign, value, NULL ,tmp, getQuadNo(), yylineno);
																					incrQuadNo();
																					myEmit(sub, value, newexpr_constnum(1), value, getQuadNo(), yylineno);
																					incrQuadNo();
																					myEmit(tablesetelement,$1,$1->index, value,getQuadNo(), yylineno);
																				}else{
																					incrQuadNo();
																					myEmit(assign, $1, NULL, tmp, getQuadNo() , yylineno);
																					
																					incrQuadNo();
																					myEmit(sub,$1, newexpr_constnum(1), $1, getQuadNo() ,yylineno);
																				}
																				$$= tmp; //mallon ayto edw

																			}
					| primary 												{//fprintf(stdout,"term -> primary\n");
																				$$ = $1;
																			}
					;



assignexpr:			lvalue ASSIGN expr 										{	//fprintf(stdout,"assignexpr -> lvalue = expr\n");
																				expr* input_exp= emitShortCircuitAssignment($3, scope, yylineno);

																				expr *exp = malloc(sizeof(expr));
																				if($1->type==programfunc_e || $1->type==libraryfunc_e){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																					return stop_parser();
																				}else{
																					if($1->type == tableitem_e){
																						incrQuadNo();
																						myEmit(tablesetelement,$1, $1->index, input_exp, getQuadNo() ,yylineno);
																						exp = emit_ifTableItem($1, scope, yylineno); //h incrQuadNo ginetai eswterika
																						exp->type = assignexpr_e;
																					}else{

																						incrQuadNo();
																						myEmit(assign,input_exp, NULL, $1, getQuadNo() ,yylineno);
																						exp = newexpr(assignexpr_e);
																						exp->sym =  CreateNode(bringTemp()->tname, scope, yylineno, LOC);
																						exp->sym->sym_type = var_s;
																						exp->sym->space=currScopeSpace();
																						exp->sym->offset=currScopeOffset();
																						incCurrScopeOffset();
																						Insert(table, table->scopeHead,exp->sym);
																						incrQuadNo();
																						myEmit(assign,$1, NULL, exp, getQuadNo() ,yylineno);


																					//fprintf(stderr, "doulepse to emit\n");
																					}
																				}
																				//resetTemps();
																				// char *c = $3->sym->name;
																				// removeFromActiveTempList_n(c);
																				$$ = exp;
																			}
					;

primary:			lvalue													{//fprintf(stdout,"primary -> lvalue\n");
																				$$ = emit_ifTableItem($1, scope, yylineno);
																				//$$ = $1; //ayto edw antikatastathike logw ton members, ginetai eswterika
																			}												
					| call 													{/*fprintf(stdout,"primary -> call\n"); $$ = $1;*/}
					| tablemake 											{//fprintf(stdout,"primary -> tablemake\n");
																				$$=$1;
																			}
					| PAR_O funcdef PAR_C 									{//fprintf(stdout,"primary -> (funcdef)\n");
																				expr* exp = newexpr(programfunc_e);
																				exp->sym= $2; //tmpVar as LOC
																				$$ = exp;
																			}
					| const 												{//fprintf(stdout,"primary -> const\n");
																				$$ = $1;
																			}
					;

lvalue: 			ID 														{	
																				varexists=0;
																				is_var=1;
																				unsigned int tempscope=scope;
																				id_is_lib=0;
																				SymbolTableEntry * tentry;
																				is_func=0;
																				//fprintf(stdout,"lvalue -> ID (%s)\n", yytext);
																				if(isLibFunc(table,yytext)){
																					id_is_lib=1;
																					is_func = 1;
																					tentry= ScopeLookup(table, $1, tempscope);
																					expr* exp = newexpr(libraryfunc_e);
																					exp->sym = tentry ;
																					exp->sym->sym_type = libraryfunc_s;
																					$$ = exp;
																					
																				}else{
																					while(1){
																						tentry= ScopeLookup(table, yytext, tempscope);
																						if(tentry!=NULL){
																							varexists=1;
																							if(tentry->type==3 || tentry->type==4){
																								is_var=0;
																								//is_func = 1;
																								id_is_lib=1; //na tsekaroume giati einai edw 
																							}
																							expr* exp = newexpr(programfunc_e);
																							exp->sym= tentry;
																							$$ = exp;
																							break;
																						}	
																						if(!tempscope)
																							break;
																						
																						tempscope--;
																						}	
																					
																						if(is_var){																					
																							if(varexists){
																								if(tentry->scope!=0 && tentry->scope!=top(in_function)){
																									fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): cannot access \'%s\' inside the function\n",yylineno, yytext);
																									return stop_parser();
																								}
																								expr* exp = newexpr(var_e);
																								exp->sym= tentry;
																								$$ = exp;
																							}
																							else{
																								expr* exp = newexpr(var_e);

																								if(scope){
																									entry=CreateNode(yytext, scope, yylineno, LOC);																									
																									exp->sym= entry;
																									exp->sym->sym_type = var_s;
																									exp->sym->space=currScopeSpace();
																									exp->sym->offset=currScopeOffset();
																									incCurrScopeOffset();
																								}
																								else{
																									entry=CreateNode(yytext, scope, yylineno, GLOBAL);
																									exp->sym= entry;
																									exp->sym->sym_type = var_s;
																									exp->sym->space=currScopeSpace();
																									exp->sym->offset=currScopeOffset();
																									incCurrScopeOffset();
																								}
																								Insert(table, table->scopeHead, entry);

																								increase_data(f_names);
																								$$ = exp;
																							}
																						}
																					
																					}
																				}

					| LOCAL ID 												{ 	varexists=0;
																			 	unsigned int tempscope=scope;
																				
																				if(isLibFunc(table,yytext)){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): \'%s\' shadows a library function\n",yylineno,yytext);
																					return stop_parser();
																					break;
																				}
																				else{
																					expr* exp = newexpr(var_e);
																					if(scope==0){
																						entry=CreateNode(yytext, scope, yylineno, GLOBAL);
																							exp->sym= entry;
																							exp->sym->sym_type = var_s;
																							exp->sym->space=currScopeSpace();
																							exp->sym->offset=currScopeOffset();
																							incCurrScopeOffset();
																						Insert(table, table->scopeHead, entry);
																						increase_data(f_names);
																						$$ = exp;
																					}
																					else{
																						if(ScopeLookup2(table, yytext, scope)==0){
																							entry=CreateNode(yytext, scope, yylineno, LOC);
																								exp->sym= entry;
																								exp->sym->sym_type = var_s;
																								exp->sym->space=currScopeSpace();
																								exp->sym->offset=currScopeOffset();
																								incCurrScopeOffset();
																							Insert(table, table->scopeHead, entry);
																							increase_data(f_names);
																							$$ = exp;
																						}
																					}
																					
																					//fprintf(stdout,"lvalue -> LOCAL ID (%s)\n", yytext);
																				}
																			}

					| COLON2 ID 											{
																				if(ScopeLookup2(table, yytext, 0)==0){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): No global variable \'::%s\' exists\n",yylineno, yytext);
																					return stop_parser();
																				}
																			//	entry=CreateNode(yytext, 0, yylineno, GLOBAL);
																			//	Insert(table, table->scopeHead, entry);
																				//fprintf(stdout,"lvalue -> :: ID (%s)\n", yytext);
																			}

					| member												{/*fprintf(stdout,"lvalue from a \'member\'\n");*/}
					;

member:				lvalue DOT ID 											{	//fprintf(stdout,"member -> lvalue . ID\n");
																				// if(ScopeLookup2(table, yytext, scope)==0){
																				// 	entry=CreateNode(yytext, scope, yylineno, GLOBAL);
																				// 	Insert(table, table->scopeHead, entry);
																				// 	increase_data(f_names);
																				// }
																				$$ = member_item($1, $3 , scope, yylineno); //incrQuadNO eswterika

																			}															
					 
					| call DOT ID 											{//fprintf(stdout,"member -> call . ID\n");
																				if(ScopeLookup2(table, $3, scope)==0){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): No member \'.%s\' exists\n",yylineno, $3);
																					return stop_parser();
																				}
																			}
					| call SBRKT_O expr SBRKT_C 							{/*fprintf(stdout,"member -> call [ EXPR ]\n");*/}
					;
											

call:				call  PAR_O elist PAR_C 							{	//if(!is_func){fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): can not use function \'%s\' as a variable\n",yylineno,yytext); return stop_parser();}
																			//fprintf(stdout,"call -> (  elist ) \n");
																			$$ = emitCall($1, elist_paramets, scope ,yylineno); //kanei eswterika to incrQuadNo();
																			if(elist_paramets != NULL)
																				free_param();
																			elist_paramets = NULL;
																		}							
						| lvalue										{	//xali
																			//if(!id_is_lib){
																				// if(!varexists){
																			 // 		fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): function  \'%s\' is undeclared\n",yylineno,$1->sym->name);
																			 // 		return stop_parser();
																			 // 	}else if(!is_func){
																				// 	fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): can not use variable \'%s\' as a function\n",yylineno,$1->sym->name);
																			  		//return stop_parser();
																			 // 	}
																		 	// }
																		} 
					callsuffix 												{//fprintf(stdout,"call -> lvalue callsuffix\n");
																				if(call_method){
																				 	expr *self = $1;
																				 	$1 = emit_ifTableItem(member_item(self, call_method_name, scope, yylineno),scope, yylineno);
																				 	insert_param(self);
																				}
																				
																				$$ = emitCall($1,elist_paramets, scope ,yylineno);
																				if(elist_paramets != NULL)
																					free_param();
																				elist_paramets = NULL;

																				}

					
					| PAR_O funcdef PAR_C PAR_O elist PAR_C 				{//fprintf(stdout,"call -> (funcdef) (elist)\n");
																				expr* exp = newexpr(programfunc_e);
																				exp->sym= $2; 
																				$$ = emitCall(exp, elist_paramets, scope ,yylineno);
																				if(elist_paramets != NULL)
																					free_param();
																				elist_paramets = NULL;
																			}
					;

callsuffix:			normcall 												{//fprintf(stdout,"callsuffix -> normcall\n");
																				$$ = $1;
																			}
					| methodcall											{//fprintf(stdout,"callsuffix -> methodcall\n");
																				$$ = $1;
																			}
					;

normcall:			PAR_O  elist  PAR_C 									{//fprintf(stdout,"normcall -> (elist)\n");
																				call_method = 0; 
																				call_method_name = NULL;
																			}
					;


methodcall:			DOT2 ID 												{call_method_name = strdup($2);} 				
					PAR_O   elist  PAR_C  									{//fprintf(stdout,"methodcall -> ::ID (elist)\n");
																				call_method = 1; //simainei oti exoume ..f()
																		 	}
					;



tablemake:			SBRKT_O elist SBRKT_C									{//fprintf(stdout,"tablemake -> [elist]\n");
																				expr *exp = newexpr(newtable_e); 
																				exp->sym = CreateNode(bringTemp()->tname, scope, yylineno, LOC); //tmpVar as LOC
																				exp->sym->sym_type = var_s;
																				exp->sym->space=currScopeSpace();
																				exp->sym->offset=currScopeOffset();
																				incCurrScopeOffset();
																				Insert(table, table->scopeHead,exp->sym);
																				incrQuadNo();
																				myEmit(tablecreate, NULL, NULL, exp,getQuadNo(), yylineno);
																				int i =0;
																				param_expr *tmp = elist_paramets;
																				while(tmp != NULL){
																					incrQuadNo();
																					myEmit(tablesetelement, exp, newexpr_constnum(i++), tmp->exp,getQuadNo(), yylineno);
																					tmp = tmp->right;
																				}
																				$$ = exp;
																				if(elist_paramets != NULL)
																					free_param();
																				elist_paramets = NULL;

																			}
					| SBRKT_O indexed SBRKT_C								{//fprintf(stdout,"tablemake -> [indexed]\n");
																				expr *exp = newexpr(newtable_e); 
																				exp->sym = CreateNode(bringTemp()->tname, scope, yylineno, LOC); //tmpVar as LOC
																				exp->sym->sym_type = var_s;
																				exp->sym->space=currScopeSpace();
																				exp->sym->offset=currScopeOffset();
																				incCurrScopeOffset();
																				Insert(table, table->scopeHead,exp->sym);
																				incrQuadNo();
																				myEmit(tablecreate, NULL, NULL, exp,getQuadNo(), yylineno);

																				indexed_expr *tmp = indexed_paramets;
																				while(tmp != NULL){
																					incrQuadNo();
																					myEmit(tablesetelement, exp, tmp->exp1, tmp->exp2,getQuadNo(), yylineno);
																					tmp = tmp->next;
																				}
																				$$ = exp;
																				if(indexed_paramets != NULL)
																					free_indexed();
																				indexed_paramets = NULL;

																			}
					;

indexed:			indexedelem	indexed2									{/*fprintf(stdout,"indexed -> indexedelem\n");*/}
					;

indexed2:			COMMA indexedelem indexed2 								{/*fprintf(stdout,"indexed -> , indexedelem\n");*/}
 					|														{/*fprintf(stdout,"indexed -> empty\n");*/}
					;

indexedelem:		BRKT_O expr COLON expr BRKT_C 							{/*fprintf(stdout,"indexedelem -> {expr : expr}  \n");*/
																				insert_indexed($2,$4);
																			}
					;   

block:				BRKT_O BRKT_C											{/*fprintf(stdout,"block -> {}\n");*/}
					| BRKT_O 												{scope++; addScope(table->scopeHead, scope); enterScopeSpace();} 
					programcont BRKT_C 										{	//fprintf(stdout,"block -> {stmt*}\n");
																				Hide(table, scope);
																				scope--;
																				exitScopeSpace();
																			}
					;

funcdef:			FUNCTION ID 											{	tempname = (char *) malloc(strlen($2)+1); 
																				strcpy(tempname, $2); 
																				unsigned int funcexists=0, tempscope=scope, flag=0, func_err=0;
																				if(isLibFunc(table,tempname)){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): %s shadows a library function\n",yylineno, tempname);
																					return stop_parser();
																				}
																				else{
																					SymbolTableEntry*  tentry;
																						tentry= ScopeLookup(table, tempname, tempscope);
																						
																					if(tentry!=NULL){
																						funcexists=1;
																						if(tentry->type==0){
																							flag=1;
																							fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): \'%s\' already defined as GLOBAL variable \n",yylineno, yytext);
																							return stop_parser();
																						}else if(tentry->type==1){
																							flag=1;
																							fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): \'%s\' already defined as LOCAL variable \n",yylineno, yytext);
																							return stop_parser();
																						}else if(tentry->type==2){
																							flag=1;
																							fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): \'%s\' already defined as FORMAL variable \n",yylineno, yytext);
																							return stop_parser();
																						}else if(tentry->type==3 && tentry->scope==scope){
																							func_err=1;
																							fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): \'%s\' already defined as USER function in scope: %d\n",yylineno, yytext, scope);
																							return stop_parser();
																						}

																					}
																					if(!func_err){
																						if(!flag){																			
																							if(funcexists){
																								entry=CreateNode(tempname, tempscope, yylineno, USERFUNC);
																								//fprintf(stdout, "just added Function %s, in scope %d \n", tempname, tempscope);
																							}
																							else{
																								entry=CreateNode(tempname, scope, yylineno, USERFUNC);
																								//fprintf(stdout, "just added Function %s, in scope %d \n", tempname, scope);
																							}
																							//{fprintf(stdout,"funcdef -> function ID  (idlist) block\n");}

																							Insert(table, table->scopeHead, entry);

																							f_names= push_name(f_names, $2);

																							expr* exp = newexpr(programfunc_e);
																							exp->sym = entry;
																							exp->sym->sym_type = programfunc_s;
																							exp->sym->name= malloc(strlen($2));
																							strcpy(exp->sym->name, $2);
																							incrQuadNo();
																							myEmit(funcstart, NULL, NULL, exp, getQuadNo(),yylineno);
																							entry->label = getQuadNo();
																							
																						}
																					}
																					
																				}
																				
																			}
					PAR_O													{scope++; 
																				addScope(table->scopeHead, scope); 
																				inside_function++; 
																				in_function=push(in_function, scope); 
																				enterScopeSpace();
																				if(inside_function != 0){
																					addOffsetNode(get_functionLocalOffset());
																					reset_formalArgsOffset();
																					reset_functionLocalOffset();
																				}

																			}
					idlist													{scope--;}
					PAR_C block												{	inside_function--;
																									
																				in_function = pop(in_function);
																				
																				struct f_stack* top_f= top_struct(f_names);

																				SymbolTableEntry*  tentry;
																				tentry= ScopeLookup(table, top_f->f_name, scope);
																				tentry->totalArgs= top_f->data;
																				 
																				expr* exp = newexpr(programfunc_e);
																				exp->sym = tentry;
																				exp->sym->name= malloc(strlen(top_f->f_name));
																				strcpy(exp->sym->name, top_f->f_name);
																				
																				incrQuadNo();
																				myEmit(funcend, NULL, NULL, exp, getQuadNo(),yylineno);
																				
																				f_names= pop(f_names);
																				exitScopeSpace();

																				set_functionLocalOffset(popOffsetNode()); //ksan kanoume to offset gia ta locals iso me oso itan prin 

																				$$ = tentry;
																			}	
																																												
					| FUNCTION PAR_O 										{scope++; 
																				addScope(table->scopeHead, scope); 
																				inside_function++; 
																				in_function=push(in_function, scope); 
																				enterScopeSpace(); 
																				if(inside_function != 0){
																					addOffsetNode(get_functionLocalOffset());
																					reset_formalArgsOffset();
																					reset_functionLocalOffset();
																				}
																			}
					idlist 													{scope--;}
					PAR_C  													{ 	char * buf = (char *) malloc(sizeof(int));
																				char* funcName = (char*) malloc(strlen(buf)+3);

																				funcCounter++;
																				itoa(funcCounter,buf);
																				strcpy(funcName, "_f");
																				funcName=strcat(funcName,buf);
																				
																				entry=CreateNode(funcName, scope, yylineno, USERFUNC);
																				entry->sym_type = programfunc_s;
																				
																				Insert(table, table->scopeHead, entry);

																				f_names= push_name(f_names, funcName);
																				expr* exp = newexpr(programfunc_e);
																				exp->sym = entry;
																				exp->sym->name= malloc(strlen(funcName));
																				strcpy(exp->sym->name, funcName);
																				
																				incrQuadNo();
																				myEmit(funcstart, NULL, NULL, exp, getQuadNo(),yylineno);
																				entry->label = getQuadNo();
																				
																			}
					block													{	//fprintf(stdout,"funcdef -> function (idlist) block\n");
																				inside_function--;
																				
																				in_function = pop(in_function);

																				struct f_stack* top_f= top_struct(f_names);

																				SymbolTableEntry*  tentry;
																				tentry= ScopeLookup(table, f_names->f_name, scope);
																				tentry->totalArgs= f_names->data;

																				expr* exp = newexpr(programfunc_e);
																				exp->sym = tentry;
																				exp->sym->name= malloc(strlen(top_f->f_name));
																				strcpy(exp->sym->name, top_f->f_name);
																				
																				incrQuadNo();
																				myEmit(funcend, NULL, NULL, exp, getQuadNo(),yylineno);

																				f_names= pop(f_names);
																				exitScopeSpace();
																				set_functionLocalOffset(popOffsetNode());
																				$$ = entry;
																			}
					;

const:				INTEGER 												{/*fprintf(stdout,"const -> integer '%d'\n", $1);*/ $$=newexpr_constnum((double)$1);}
					| REAL 													{/*fprintf(stdout,"const -> real\n");*/ $$=newexpr_constnum($1); }
					| STR 													{/*fprintf(stdout,"const -> string '%s'\n", $1);*/ $$=newexpr_conststring($1); }
					| NIL 													{/*fprintf(stdout,"const -> nil\n");*/ $$=newexpr_constnil();}
					| TRUE 													{/*fprintf(stdout,"const -> true\n"); */$$=newexpr_constbool('T');}
					| FALSE 												{/*fprintf(stdout,"const -> false\n");*/ $$=newexpr_constbool('F');}
					;

idlist:																		
					ID														{	//fprintf(stdout,"idlist -> ID (1)\n");
																				if(isLibFunc(table,yytext)){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): \'%s\' shadows a library function\n",yylineno,yytext);
																					return stop_parser();
																				}
																				else{

																					entry=CreateNode(yytext, scope, yylineno, FORMAL);
																					entry->sym_type = var_s;
																					entry->space = currScopeSpace();
																					entry->offset = currScopeOffset();
																					incCurrScopeOffset();
																					Insert(table,table->scopeHead, entry);
																				}
																			}
					idlist2 												{/*fprintf(stdout,"idlist -> ID idlist2\n");*/}	
					|														{/*fprintf(stdout,"idlist -> empty\n");*/}
					;

idlist2:																		
					COMMA ID												{	//fprintf(stdout,"idlist -> ID (2)\n");
																				if(isLibFunc(table,yytext)){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): \'%s\' shadows a library function\n",yylineno,yytext);
																					return stop_parser();
																				}
																				else{
																					SymbolTableEntry*  tentry;
																					tentry= ScopeLookup(table, yytext, scope);
																						
																					if(tentry!=NULL){
																						fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): variable \'%s\' already defined in scope: %d\n",yylineno, yytext, scope);
																						return stop_parser();
																					}
																					else{
																						entry=CreateNode(yytext, scope, yylineno, FORMAL);
																						entry->sym_type = var_s;
																						entry->space = currScopeSpace();
																						entry->offset = currScopeOffset();
																						incCurrScopeOffset();
																						Insert(table,table->scopeHead, entry);
																					}
																				}
																			}
					idlist2 												{	/*fprintf(stdout,"idlist2 -> , ID idlist2\n"); */}						
					|														{	/*fprintf(stdout,"idlist -> empty\n");*/}
					;


ifprefix:			IF PAR_O expr PAR_C										{	/*fprintf(stdout,"ifstmt -> if(expr) stmt \n");*/
																				
																				expr* exp= emitShortCircuitAssignment($3, scope, yylineno);

																				incrQuadNo();
																				emitIf(exp, yylineno );
																				
																				$$= getQuadNo();
																			}
					;

elseprefix:			ELSE													{
																				incrQuadNo();
																				emitJump(0, yylineno);

																				$$= getQuadNo();
																			}
					;

ifstmt:				ifprefix stmt %prec LOWER_THAN_ELSE						{	/*fprintf(stdout,"ifstmt -> if(expr) stmt \n");*/
																				patchlabel($1, nextQuadNo());
																			}
					|ifprefix stmt elseprefix stmt 							{	/*fprintf(stdout,"ifstmt -> if(expr) stmt else stmt\n");*/
																				patchlabel($1, $3+1);
																				patchlabel($3, nextQuadNo());
																			}
					;


whilestart:			WHILE													{
																				$$= nextQuadNo();
																			}
					;

whilecont:			PAR_O expr PAR_C										{	
																				expr* exp= emitShortCircuitAssignment($2, scope, yylineno);

																				incrQuadNo();
																				emitIf(exp, yylineno );

																				$$= getQuadNo();

																				inside_loop++;
																			}
					;


whilestmt:			whilestart whilecont stmt 								{	//fprintf(stdout,"whilestmt -> while(expr) stmt\n");

																				incrQuadNo();
																				emitJump($1, yylineno);
																				patchlabel($2, nextQuadNo());

																				breaklist = patchlabel_bc(breaklist, nextQuadNo(), inside_loop);
																				contlist = patchlabel_bc(contlist, $1, inside_loop);
																				
																				inside_loop--;
																			}
					;


FOR_M:																		{	$$= nextQuadNo();	}
					;


FOR_JMP:																	{	incrQuadNo();
																				emitJump(0, yylineno);
																				$$= getQuadNo();
																			}
					;


forprefix:			FOR PAR_O elist SEMICOL FOR_M expr SEMICOL				{	
																				expr* exp= emitShortCircuitAssignment($6, scope, yylineno);

																				struct label_carrier* ret_carrier= malloc(sizeof(struct label_carrier));
																				ret_carrier->test= $5;
																				incrQuadNo();
																				myEmit(if_eq, exp, newexpr_constbool('T'), NULL, 0, yylineno);
																				ret_carrier->enter= getQuadNo();
																				$$= ret_carrier;
																			}
					;


formiddle:			forprefix FOR_JMP elist PAR_C FOR_JMP					{
																				patchlabel($1->enter, $5+1);
																				patchlabel($5, $1->test);
																				inside_loop++;

																				$$= $2;
																			}
					;



forstmt:			formiddle stmt FOR_JMP									{	//fprintf(stdout,"forstmt -> for(elist;expr;elist) stmt\n");
																				
																				patchlabel($1, nextQuadNo());
																				patchlabel($3, $1+1);

																				breaklist = patchlabel_bc(breaklist, nextQuadNo(), inside_loop);
																				contlist = patchlabel_bc(contlist, $1+1, inside_loop);
																				
																				inside_loop--;
																			}
					;

returnstmt:			RETURN SEMICOL 											{	//fprintf(stdout,"returnstmt -> return;\n");
																				if(!inside_function){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): Use of \'return\' while not in a function\n", yylineno);
																					return stop_parser();
																				}
																				myEmit(ret, NULL, NULL, NULL, getQuadNo(), yylineno);

																			}
					| RETURN expr SEMICOL 									{	//fprintf(stdout,"returnstmt -> return expr;\n");
																				if(!inside_function){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): Use of \'return\' while not in a function\n", yylineno);
																					return stop_parser();
																				}

																				expr* exp= emitShortCircuitAssignment($2, scope, yylineno);

																				expr* tmp;
																				int const_val=0;
																				if(exp->type == constnum_e){
																					const_val=1;
																					tmp = newexpr_constnum(exp->numConst);
																				}else if(exp->type == conststring_e){
																					const_val=1;
																					tmp = newexpr_conststring(exp->strConst);
																				}else if(exp->type == constbool_e){
																					const_val=1;
																					tmp = newexpr_constbool(exp->boolConst);
																				}else{
																					tmp = exp;
																				}

																				if(const_val){
																					tmp->type =  var_e;
																					tmp->sym= CreateNode(bringTemp()->tname, scope, yylineno, LOC); //tmpVar as LOC
																					tmp->sym->sym_type = var_s;
																					tmp->sym->space = currScopeSpace();
																					tmp->sym->offset = currScopeOffset();
																					incCurrScopeOffset();
																					Insert(table, table->scopeHead,tmp->sym);
																					incrQuadNo();
																					myEmit(assign, exp, NULL, tmp, getQuadNo(), yylineno);
																				}
																				incrQuadNo();
																				myEmit(ret, NULL, NULL, tmp, getQuadNo(), yylineno);
																			}
					;

elist:				expr elist2												{//fprintf(stdout,"elist -> expr\n");
																				insert_param($1);
																			}
					|														
					;

elist2:				COMMA expr elist2 										{//fprintf(stdout,"elist -> , expr\n");
																				insert_param($2);
																			}
					|														{/*fprintf(stdout,"elist -> empty\n");*/}
					;


%%

int yyerror (char* yaccProvidedMessage)
{
	fprintf(stderr, "\t\x1b[31mSYNTAX ERROR\x1b[0m\n%s: at line %d, before token:  <%s> \n", yaccProvidedMessage, yylineno, yytext);
}


int main(int argc,char** argv){
	table=CreateTable();
    if(argc>1){
        if(!(yyin = fopen(argv[1],"r"))){
            fprintf(stderr,"Cannot read file: %s\n",argv[1]);
            return 1;
        }
    }
    else{
        yyin = stdin;
    }

    elist_paramets = NULL;
	indexed_paramets = NULL;
    

    SymbolTableEntry * newEntry;
    newEntry= CreateNode("print", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

  	newEntry= CreateNode("input", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("objectmemberkeys", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("objecttotalmembers", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("objectcopy", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("totalarguments", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("argument", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("typeof", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("strtonum", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("sqrt", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("cos", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

	newEntry= CreateNode("sin", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

	
	if(yyparse())
		return 1;
	fprintf(stderr, "\x1b[32mcompilation succeded\x1b[0m\n");

	//printTable(table);

	printQuads();
	printQuads_to_file();
	
    return 0;
}