#include "function_stack.h"
#include <stdlib.h>

/*
    init the stack
*/
void init(struct f_stack* head)
{
    head = NULL;
}
 
/*
    push an element into stack
*/
struct f_stack* push(struct f_stack* head,int data)
{
    struct f_stack* tmp = (struct f_stack*)malloc(sizeof(struct f_stack));
    if(tmp == NULL)
    {   
        printf("PRINT GIA EXIT STO PUSH\n");
        exit(0);
    }
    tmp->data = data;
    printf("\x1b[32m%d\x1b[0m\n", tmp->data);
    tmp->next = head;
    printf("tmp->next = head;\n");
    head = tmp;
    printf("\x1b[38m%d\x1b[0m\n", head->data);
    return head;
}
/*
    pop an element from the stack
*/
struct f_stack* pop(struct f_stack *head)
{
    struct f_stack* tmp = head;
    if(tmp==NULL){
        // printf("ADEIASEE\n");
        return NULL;
    }
    
    //*element = head->data;
    head = head->next;
    free(tmp);
    return head;
}
/*
    returns 1 if the stack is empty, otherwise returns 0
*/
int empty(struct f_stack* head)
{
    return head == NULL ? 1 : 0;
}
 
/*
    display the stack content
*/
void display(struct f_stack* head)
{
    struct f_stack *current;
    current = head;
    if(current!= NULL)
    {
        printf("----------------Stack: ");
        do
        {
            printf("%d ",current->data);
            current = current->next;
        }
        while (current!= NULL);
        printf("\n");
    }
    else
    {
        printf("The Stack is empty\n");
    }
 
}

int top(struct f_stack* head){
	if(empty(head))
		return -1;

	return head->data;
}