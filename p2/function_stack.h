#include <stdio.h>
 
struct f_stack
{
    int data;
    struct f_stack* next;
};
 
/*
    init the stack
*/
void init(struct f_stack* head);
 
/*
    push an element into stack
*/
struct f_stack* push(struct f_stack* head,int data);
/*
    pop an element from the stack
*/
struct f_stack* pop(struct f_stack *head);
/*
    returns 1 if the stack is empty, otherwise returns 0
*/
int empty(struct f_stack* head);
 
/*
    display the stack content
*/
void display(struct f_stack* head);

int top(struct f_stack* head);