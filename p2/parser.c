/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "parser.y" /* yacc.c:339  */


/*	
 *
 *	Project Name: Lexical Analysis cs340
 *	Authors: 
 *			Fotios Mathioudakis
 *			Leonidas Anagnostou
 *			Evaggelia Athanasaki
 */






/* ========== C  CODE (includes etc) =========== */
#include "symtable.h"
#include "function_stack.h"

	int yyerror (char* yaccProvidedMessage);
//	int alpha_yylex(void* yylval);
	int yylex(void);

	extern int yylineno;
	extern char* yytext;
	extern FILE* yyin;
	//extern unsigned int scope=0;
	SymbolTable* table;
	SymbolTableEntry* entry;
	unsigned int funcCounter=0;
	char* tempname;
	struct f_stack* in_function=NULL;
	int inside_function=0;
	int inside_loop=0;
	int is_func = 0;
	int func_lv= 0;
	int id_is_lib= 0;
	int f_call = 0;
	int f_call2 = 0;


/* ========== YACC definitions etc =========== */

#line 111 "parser.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "parser.h".  */
#ifndef YY_YY_PARSER_H_INCLUDED
# define YY_YY_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    IF = 258,
    ELSE = 259,
    WHILE = 260,
    FOR = 261,
    FUNCTION = 262,
    RETURN = 263,
    BREAK = 264,
    CONTINUE = 265,
    AND = 266,
    NOT = 267,
    OR = 268,
    LOCAL = 269,
    TRUE = 270,
    FALSE = 271,
    NIL = 272,
    ASSIGN = 273,
    PLUS = 274,
    MINUS = 275,
    MUL = 276,
    DIV = 277,
    MOD = 278,
    EQ = 279,
    NEQ = 280,
    PP = 281,
    MM = 282,
    GREATER = 283,
    LESS = 284,
    GEQ = 285,
    LEQ = 286,
    INTEGER = 287,
    REAL = 288,
    ID = 289,
    STR = 290,
    WHITESPACE = 291,
    BRKT_O = 292,
    BRKT_C = 293,
    SBRKT_O = 294,
    SBRKT_C = 295,
    PAR_O = 296,
    PAR_C = 297,
    SEMICOL = 298,
    COMMA = 299,
    COLON = 300,
    COLON2 = 301,
    DOT = 302,
    DOT2 = 303,
    LOWER_THAN_ELSE = 304,
    UMINUS = 305
  };
#endif
/* Tokens.  */
#define IF 258
#define ELSE 259
#define WHILE 260
#define FOR 261
#define FUNCTION 262
#define RETURN 263
#define BREAK 264
#define CONTINUE 265
#define AND 266
#define NOT 267
#define OR 268
#define LOCAL 269
#define TRUE 270
#define FALSE 271
#define NIL 272
#define ASSIGN 273
#define PLUS 274
#define MINUS 275
#define MUL 276
#define DIV 277
#define MOD 278
#define EQ 279
#define NEQ 280
#define PP 281
#define MM 282
#define GREATER 283
#define LESS 284
#define GEQ 285
#define LEQ 286
#define INTEGER 287
#define REAL 288
#define ID 289
#define STR 290
#define WHITESPACE 291
#define BRKT_O 292
#define BRKT_C 293
#define SBRKT_O 294
#define SBRKT_C 295
#define PAR_O 296
#define PAR_C 297
#define SEMICOL 298
#define COMMA 299
#define COLON 300
#define COLON2 301
#define DOT 302
#define DOT2 303
#define LOWER_THAN_ELSE 304
#define UMINUS 305

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 48 "parser.y" /* yacc.c:355  */

	int integer;
	double real;
	char* string;

#line 257 "parser.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_PARSER_H_INCLUDED  */

/* Copy the second part of user declarations.  */
#line 109 "parser.y" /* yacc.c:358  */
/* TOP(low) -> BOTTOM(high) */
#line 117 "parser.y" /* yacc.c:358  */
/* warning!! check the MINUS CASE */

#line 278 "parser.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  68
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   566

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  51
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  41
/* YYNRULES -- Number of rules.  */
#define YYNRULES  100
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  187

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   305

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   136,   136,   137,   140,   141,   144,   145,   146,   147,
     148,   149,   154,   159,   160,   164,   165,   166,   167,   168,
     169,   170,   171,   172,   173,   174,   175,   176,   177,   178,
     182,   183,   184,   185,   190,   195,   200,   205,   209,   216,
     217,   218,   219,   220,   223,   287,   310,   319,   322,   323,
     324,   325,   328,   331,   331,   336,   339,   340,   343,   347,
     350,   351,   354,   355,   358,   359,   362,   365,   366,   369,
     372,   373,   373,   380,   430,   431,   380,   433,   434,   434,
     433,   453,   454,   455,   456,   457,   458,   462,   462,   472,
     476,   476,   494,   499,   500,   503,   503,   509,   509,   515,
     520
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "IF", "ELSE", "WHILE", "FOR", "FUNCTION",
  "RETURN", "BREAK", "CONTINUE", "AND", "NOT", "OR", "LOCAL", "TRUE",
  "FALSE", "NIL", "ASSIGN", "PLUS", "MINUS", "MUL", "DIV", "MOD", "EQ",
  "NEQ", "PP", "MM", "GREATER", "LESS", "GEQ", "LEQ", "INTEGER", "REAL",
  "ID", "STR", "WHITESPACE", "BRKT_O", "BRKT_C", "SBRKT_O", "SBRKT_C",
  "PAR_O", "PAR_C", "SEMICOL", "COMMA", "COLON", "COLON2", "DOT", "DOT2",
  "LOWER_THAN_ELSE", "UMINUS", "$accept", "program", "programcont", "stmt",
  "expr", "term", "assignexpr", "primary", "lvalue", "member", "call",
  "$@1", "callsuffix", "normcall", "methodcall", "elist", "elist2",
  "objectdef", "indexed", "indexed2", "indexedelem", "block", "$@2",
  "funcdef", "$@3", "$@4", "$@5", "$@6", "$@7", "$@8", "const", "idlist",
  "$@9", "idlist2", "$@10", "ifstmt", "whilestmt", "$@11", "forstmt",
  "$@12", "returnstmt", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305
};
# endif

#define YYPACT_NINF -165

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-165)))

#define YYTABLE_NINF -54

#define yytable_value_is_error(Yytable_value) \
  (!!((Yytable_value) == (-54)))

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     139,   -34,   -23,   -14,   -22,   167,     5,    14,   200,   -13,
    -165,  -165,  -165,   200,    12,    12,  -165,  -165,  -165,  -165,
      23,    -8,     8,    28,    63,   139,  -165,   282,  -165,  -165,
    -165,    89,  -165,    57,  -165,  -165,  -165,  -165,  -165,  -165,
    -165,  -165,   200,   200,   200,  -165,  -165,  -165,   307,  -165,
    -165,  -165,  -165,  -165,    71,   -37,    57,   -37,  -165,   139,
     200,   200,    44,    47,    59,   357,    64,  -165,  -165,  -165,
     200,   200,   200,   200,   200,   200,   200,   200,   200,   200,
     200,   200,   200,  -165,   200,  -165,  -165,   200,    55,     4,
     200,   200,    74,   381,   405,   256,    66,    69,    79,  -165,
      83,    85,   229,   256,  -165,  -165,    90,  -165,  -165,    88,
     522,   509,     9,     9,  -165,  -165,  -165,   535,   535,   242,
     242,   242,   242,   494,   429,  -165,   200,   100,  -165,  -165,
    -165,   451,    93,  -165,   139,  -165,  -165,   200,  -165,  -165,
    -165,    88,  -165,   200,  -165,    59,   200,  -165,    97,    99,
    -165,  -165,   146,   139,   332,    79,   108,   116,   473,  -165,
     119,  -165,   200,   139,  -165,   200,  -165,   128,  -165,  -165,
    -165,  -165,   122,  -165,   125,   126,  -165,   132,  -165,  -165,
     132,   108,  -165,   139,  -165,  -165,  -165
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       3,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      85,    86,    84,     0,     0,     0,    81,    82,    44,    83,
      71,    63,     0,     0,     0,     2,     4,     0,    29,    15,
      37,    39,    47,    40,    41,    13,    14,    43,     7,     8,
       9,    10,     0,     0,    61,    73,    77,    99,     0,    11,
      12,    32,    45,    31,     0,    33,     0,    35,    70,     0,
       0,     0,     0,     0,    68,     0,     0,    46,     1,     5,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     6,     0,    34,    36,     0,     0,     0,
       0,    61,     0,     0,     0,    63,     0,     0,    89,   100,
       0,     0,     0,    63,    64,    65,     0,    66,    30,    42,
      27,    28,    16,    17,    18,    19,    20,    25,    26,    21,
      22,    23,    24,    38,     0,    48,    61,     0,    54,    56,
      57,     0,     0,    50,     0,    95,    60,     0,    74,    87,
      78,     0,    72,     0,    62,    68,    61,    49,     0,     0,
      51,    52,    93,     0,     0,    89,    92,     0,     0,    67,
       0,    58,    61,     0,    96,    61,    75,     0,    88,    79,
      69,    55,     0,    94,     0,     0,    90,     0,    59,    97,
       0,    92,    80,     0,    76,    91,    98
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -165,  -165,   111,   -20,    -5,  -165,  -165,  -165,    36,  -165,
      45,  -165,  -165,  -165,  -165,   -82,   -89,  -165,  -165,    30,
      80,  -164,  -165,   -21,  -165,  -165,  -165,  -165,  -165,  -165,
    -165,    22,  -165,     7,  -165,  -165,  -165,  -165,  -165,  -165,
    -165
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    89,   128,   129,   130,    96,    62,    34,    63,   107,
      64,    35,    59,    36,    97,   155,   175,    98,   157,   177,
      37,   140,   156,   168,   181,    38,    39,   153,    40,   183,
      41
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      48,    66,    87,    51,   -53,    69,   136,    42,    53,   132,
      88,   -53,    45,   182,   144,     4,   184,    65,    43,    46,
       8,    52,     9,    10,    11,    12,     9,    44,    13,    60,
      74,    75,    76,   100,    14,    15,    61,    93,    94,    95,
      16,    17,    18,    19,   148,   126,    18,    21,    49,    22,
      55,    57,   127,    54,    23,   102,   103,    50,    23,    56,
      56,    58,    67,    68,   160,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,     4,   123,
     172,    69,   124,   174,   104,   131,    95,   105,     1,   125,
       2,     3,     4,     5,     6,     7,    90,     8,    91,     9,
      10,    11,    12,   106,    92,    13,   109,    84,   133,   137,
     138,    14,    15,   139,   152,    85,    86,    16,    17,    18,
      19,    95,    20,   142,    21,   141,    22,    60,    87,   146,
     -53,    23,   154,   164,   149,   151,    88,   -53,   158,   161,
     162,    95,     1,   173,     2,     3,     4,     5,     6,     7,
     163,     8,   167,     9,    10,    11,    12,    95,   169,    13,
      95,   171,   176,   186,   178,    14,    15,   179,   180,    20,
     101,    16,    17,    18,    19,   159,    20,   166,    21,     8,
      22,     9,    10,    11,    12,    23,   145,    13,   185,     0,
       0,     0,     0,    14,    15,     0,     0,     0,     0,    16,
      17,    18,    19,     0,     0,     0,    21,     0,    22,     0,
      47,     0,     8,    23,     9,    10,    11,    12,     0,     0,
      13,     0,     0,     0,     0,     0,    14,    15,     0,     0,
       0,     0,    16,    17,    18,    19,     0,     0,     0,    21,
      70,    22,    71,     0,     0,     0,    23,     0,    72,    73,
      74,    75,    76,    77,    78,     0,     0,    79,    80,    81,
      82,    72,    73,    74,    75,    76,     0,    70,     0,    71,
     -54,   -54,   -54,   -54,   143,    72,    73,    74,    75,    76,
      77,    78,     0,     0,    79,    80,    81,    82,     0,     0,
       0,     0,     0,    70,     0,    71,     0,     0,     0,     0,
      61,    72,    73,    74,    75,    76,    77,    78,     0,     0,
      79,    80,    81,    82,     0,     0,     0,     0,    70,     0,
      71,     0,     0,     0,     0,    83,    72,    73,    74,    75,
      76,    77,    78,     0,     0,    79,    80,    81,    82,     0,
       0,     0,     0,    70,     0,    71,     0,     0,     0,     0,
      99,    72,    73,    74,    75,    76,    77,    78,     0,     0,
      79,    80,    81,    82,     0,     0,     0,     0,    70,     0,
      71,     0,     0,     0,     0,   165,    72,    73,    74,    75,
      76,    77,    78,     0,     0,    79,    80,    81,    82,     0,
       0,     0,    70,     0,    71,     0,     0,     0,     0,   108,
      72,    73,    74,    75,    76,    77,    78,     0,     0,    79,
      80,    81,    82,     0,     0,     0,    70,     0,    71,     0,
       0,     0,     0,   134,    72,    73,    74,    75,    76,    77,
      78,     0,     0,    79,    80,    81,    82,     0,     0,     0,
      70,     0,    71,     0,     0,     0,     0,   135,    72,    73,
      74,    75,    76,    77,    78,     0,     0,    79,    80,    81,
      82,     0,    70,     0,    71,     0,     0,     0,     0,   147,
      72,    73,    74,    75,    76,    77,    78,     0,     0,    79,
      80,    81,    82,     0,    70,     0,    71,     0,     0,     0,
       0,   150,    72,    73,    74,    75,    76,    77,    78,     0,
       0,    79,    80,    81,    82,    70,     0,    71,     0,     0,
       0,   170,     0,    72,    73,    74,    75,    76,    77,    78,
      70,     0,    79,    80,    81,    82,     0,     0,    72,    73,
      74,    75,    76,    77,    78,     0,     0,    79,    80,    81,
      82,    72,    73,    74,    75,    76,    77,    78,     0,     0,
      79,    80,    81,    82,    72,    73,    74,    75,    76,   -54,
     -54,     0,     0,    79,    80,    81,    82
};

static const yytype_int16 yycheck[] =
{
       5,    22,    39,     8,    41,    25,    95,    41,    13,    91,
      47,    48,    34,   177,   103,     7,   180,    22,    41,    41,
      12,    34,    14,    15,    16,    17,    14,    41,    20,    37,
      21,    22,    23,    54,    26,    27,    44,    42,    43,    44,
      32,    33,    34,    35,   126,    41,    34,    39,    43,    41,
      14,    15,    48,    41,    46,    60,    61,    43,    46,    14,
      15,    38,    34,     0,   146,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,     7,    84,
     162,   101,    87,   165,    40,    90,    91,    40,     3,    34,
       5,     6,     7,     8,     9,    10,    39,    12,    41,    14,
      15,    16,    17,    44,    47,    20,    42,    18,    34,    43,
      41,    26,    27,    34,   134,    26,    27,    32,    33,    34,
      35,   126,    37,    38,    39,    42,    41,    37,    39,    41,
      41,    46,   137,   153,    34,    42,    47,    48,   143,    42,
      41,   146,     3,   163,     5,     6,     7,     8,     9,    10,
       4,    12,    44,    14,    15,    16,    17,   162,    42,    20,
     165,    42,    34,   183,    42,    26,    27,    42,    42,    37,
      59,    32,    33,    34,    35,   145,    37,   155,    39,    12,
      41,    14,    15,    16,    17,    46,   106,    20,   181,    -1,
      -1,    -1,    -1,    26,    27,    -1,    -1,    -1,    -1,    32,
      33,    34,    35,    -1,    -1,    -1,    39,    -1,    41,    -1,
      43,    -1,    12,    46,    14,    15,    16,    17,    -1,    -1,
      20,    -1,    -1,    -1,    -1,    -1,    26,    27,    -1,    -1,
      -1,    -1,    32,    33,    34,    35,    -1,    -1,    -1,    39,
      11,    41,    13,    -1,    -1,    -1,    46,    -1,    19,    20,
      21,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    19,    20,    21,    22,    23,    -1,    11,    -1,    13,
      28,    29,    30,    31,    45,    19,    20,    21,    22,    23,
      24,    25,    -1,    -1,    28,    29,    30,    31,    -1,    -1,
      -1,    -1,    -1,    11,    -1,    13,    -1,    -1,    -1,    -1,
      44,    19,    20,    21,    22,    23,    24,    25,    -1,    -1,
      28,    29,    30,    31,    -1,    -1,    -1,    -1,    11,    -1,
      13,    -1,    -1,    -1,    -1,    43,    19,    20,    21,    22,
      23,    24,    25,    -1,    -1,    28,    29,    30,    31,    -1,
      -1,    -1,    -1,    11,    -1,    13,    -1,    -1,    -1,    -1,
      43,    19,    20,    21,    22,    23,    24,    25,    -1,    -1,
      28,    29,    30,    31,    -1,    -1,    -1,    -1,    11,    -1,
      13,    -1,    -1,    -1,    -1,    43,    19,    20,    21,    22,
      23,    24,    25,    -1,    -1,    28,    29,    30,    31,    -1,
      -1,    -1,    11,    -1,    13,    -1,    -1,    -1,    -1,    42,
      19,    20,    21,    22,    23,    24,    25,    -1,    -1,    28,
      29,    30,    31,    -1,    -1,    -1,    11,    -1,    13,    -1,
      -1,    -1,    -1,    42,    19,    20,    21,    22,    23,    24,
      25,    -1,    -1,    28,    29,    30,    31,    -1,    -1,    -1,
      11,    -1,    13,    -1,    -1,    -1,    -1,    42,    19,    20,
      21,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    -1,    11,    -1,    13,    -1,    -1,    -1,    -1,    40,
      19,    20,    21,    22,    23,    24,    25,    -1,    -1,    28,
      29,    30,    31,    -1,    11,    -1,    13,    -1,    -1,    -1,
      -1,    40,    19,    20,    21,    22,    23,    24,    25,    -1,
      -1,    28,    29,    30,    31,    11,    -1,    13,    -1,    -1,
      -1,    38,    -1,    19,    20,    21,    22,    23,    24,    25,
      11,    -1,    28,    29,    30,    31,    -1,    -1,    19,    20,
      21,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    19,    20,    21,    22,    23,    24,    25,    -1,    -1,
      28,    29,    30,    31,    19,    20,    21,    22,    23,    24,
      25,    -1,    -1,    28,    29,    30,    31
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     5,     6,     7,     8,     9,    10,    12,    14,
      15,    16,    17,    20,    26,    27,    32,    33,    34,    35,
      37,    39,    41,    46,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    68,    72,    74,    81,    86,    87,
      89,    91,    41,    41,    41,    34,    41,    43,    55,    43,
      43,    55,    34,    55,    41,    59,    61,    59,    38,    73,
      37,    44,    67,    69,    71,    55,    74,    34,     0,    54,
      11,    13,    19,    20,    21,    22,    23,    24,    25,    28,
      29,    30,    31,    43,    18,    26,    27,    39,    47,    62,
      39,    41,    47,    55,    55,    55,    66,    75,    78,    43,
      74,    53,    55,    55,    40,    40,    44,    70,    42,    42,
      55,    55,    55,    55,    55,    55,    55,    55,    55,    55,
      55,    55,    55,    55,    55,    34,    41,    48,    63,    64,
      65,    55,    66,    34,    42,    42,    67,    43,    41,    34,
      82,    42,    38,    45,    67,    71,    41,    40,    66,    34,
      40,    42,    54,    88,    55,    76,    83,    79,    55,    70,
      66,    42,    41,     4,    54,    43,    82,    44,    84,    42,
      38,    42,    66,    54,    66,    77,    34,    80,    42,    42,
      42,    85,    72,    90,    72,    84,    54
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    51,    52,    52,    53,    53,    54,    54,    54,    54,
      54,    54,    54,    54,    54,    55,    55,    55,    55,    55,
      55,    55,    55,    55,    55,    55,    55,    55,    55,    55,
      56,    56,    56,    56,    56,    56,    56,    56,    57,    58,
      58,    58,    58,    58,    59,    59,    59,    59,    60,    60,
      60,    60,    61,    62,    61,    61,    63,    63,    64,    65,
      66,    66,    67,    67,    68,    68,    69,    70,    70,    71,
      72,    73,    72,    75,    76,    77,    74,    78,    79,    80,
      74,    81,    81,    81,    81,    81,    81,    83,    82,    82,
      85,    84,    84,    86,    86,    88,    87,    90,    89,    91,
      91
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     0,     1,     2,     2,     1,     1,     1,
       1,     2,     2,     1,     1,     1,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     1,
       3,     2,     2,     2,     2,     2,     2,     1,     3,     1,
       1,     1,     3,     1,     1,     2,     2,     1,     3,     4,
       3,     4,     4,     0,     3,     6,     1,     1,     3,     5,
       2,     0,     3,     0,     3,     3,     2,     3,     0,     5,
       2,     0,     4,     0,     0,     0,     9,     0,     0,     0,
       8,     1,     1,     1,     1,     1,     1,     0,     3,     0,
       0,     4,     0,     5,     7,     0,     6,     0,    10,     2,
       3
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 136 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"program -> programcont\n");}
#line 1568 "parser.c" /* yacc.c:1646  */
    break;

  case 3:
#line 137 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"program -> \n");}
#line 1574 "parser.c" /* yacc.c:1646  */
    break;

  case 4:
#line 140 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"programcont -> stmt\n");}
#line 1580 "parser.c" /* yacc.c:1646  */
    break;

  case 5:
#line 141 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"programcont -> programcont stmt\n");}
#line 1586 "parser.c" /* yacc.c:1646  */
    break;

  case 6:
#line 144 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"stmt -> expr;\n");}
#line 1592 "parser.c" /* yacc.c:1646  */
    break;

  case 7:
#line 145 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"stmt -> ifstmt\n");}
#line 1598 "parser.c" /* yacc.c:1646  */
    break;

  case 8:
#line 146 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"stmt -> whilestmt\n");}
#line 1604 "parser.c" /* yacc.c:1646  */
    break;

  case 9:
#line 147 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"stmt -> forstmt\n");}
#line 1610 "parser.c" /* yacc.c:1646  */
    break;

  case 10:
#line 148 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"stmt -> returnstmt\n");}
#line 1616 "parser.c" /* yacc.c:1646  */
    break;

  case 11:
#line 149 "parser.y" /* yacc.c:1646  */
    {	fprintf(stdout,"stmt -> break;\n");
																				if(!inside_loop){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): Use of \'break\' while not in a loop\n",yylineno);
																				}
																			}
#line 1626 "parser.c" /* yacc.c:1646  */
    break;

  case 12:
#line 154 "parser.y" /* yacc.c:1646  */
    {	fprintf(stdout,"stmt -> continue;\n");
																				if(!inside_loop){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): Use of \'continue\' while not in a loop\n",yylineno);
																				}
																			}
#line 1636 "parser.c" /* yacc.c:1646  */
    break;

  case 13:
#line 159 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"stmt -> block\n");}
#line 1642 "parser.c" /* yacc.c:1646  */
    break;

  case 14:
#line 160 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"stmt -> funcdef\n");}
#line 1648 "parser.c" /* yacc.c:1646  */
    break;

  case 15:
#line 164 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"expr -> assignexpr\n");}
#line 1654 "parser.c" /* yacc.c:1646  */
    break;

  case 16:
#line 165 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"expr -> expr + expr\n");}
#line 1660 "parser.c" /* yacc.c:1646  */
    break;

  case 17:
#line 166 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"expr -> expr - expr\n");}
#line 1666 "parser.c" /* yacc.c:1646  */
    break;

  case 18:
#line 167 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"expr -> expr * expr\n");}
#line 1672 "parser.c" /* yacc.c:1646  */
    break;

  case 19:
#line 168 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"expr -> expr / expr\n");}
#line 1678 "parser.c" /* yacc.c:1646  */
    break;

  case 20:
#line 169 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"expr -> expr \'%\' expr\n");}
#line 1684 "parser.c" /* yacc.c:1646  */
    break;

  case 21:
#line 170 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"expr -> expr > expr\n");}
#line 1690 "parser.c" /* yacc.c:1646  */
    break;

  case 22:
#line 171 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"expr -> expr < expr\n");}
#line 1696 "parser.c" /* yacc.c:1646  */
    break;

  case 23:
#line 172 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"expr -> expr => expr\n");}
#line 1702 "parser.c" /* yacc.c:1646  */
    break;

  case 24:
#line 173 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"expr -> expr <= expr\n");}
#line 1708 "parser.c" /* yacc.c:1646  */
    break;

  case 25:
#line 174 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"expr -> expr == expr\n");}
#line 1714 "parser.c" /* yacc.c:1646  */
    break;

  case 26:
#line 175 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"expr -> expr != expr\n");}
#line 1720 "parser.c" /* yacc.c:1646  */
    break;

  case 27:
#line 176 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"expr -> expr and expr\n");}
#line 1726 "parser.c" /* yacc.c:1646  */
    break;

  case 28:
#line 177 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"expr -> expr or expr\n");}
#line 1732 "parser.c" /* yacc.c:1646  */
    break;

  case 29:
#line 178 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"expr -> term\n");}
#line 1738 "parser.c" /* yacc.c:1646  */
    break;

  case 30:
#line 182 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"term -> (expr)\n");}
#line 1744 "parser.c" /* yacc.c:1646  */
    break;

  case 31:
#line 183 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"term -> -expr\n");}
#line 1750 "parser.c" /* yacc.c:1646  */
    break;

  case 32:
#line 184 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"term -> not expr\n");}
#line 1756 "parser.c" /* yacc.c:1646  */
    break;

  case 33:
#line 185 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"term -> ++lvalue\n");
																				if(func_lv){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																				}
																			}
#line 1766 "parser.c" /* yacc.c:1646  */
    break;

  case 34:
#line 190 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"term -> lvalue++\n");
																				if(func_lv){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																				}
																			}
#line 1776 "parser.c" /* yacc.c:1646  */
    break;

  case 35:
#line 195 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"term -> --lvalue\n");
																				if(func_lv){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																				}
																			}
#line 1786 "parser.c" /* yacc.c:1646  */
    break;

  case 36:
#line 200 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"term -> lvalue--\n");
																				if(func_lv){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																				}
																			}
#line 1796 "parser.c" /* yacc.c:1646  */
    break;

  case 37:
#line 205 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"term -> primary\n");}
#line 1802 "parser.c" /* yacc.c:1646  */
    break;

  case 38:
#line 209 "parser.y" /* yacc.c:1646  */
    {	fprintf(stdout,"assignexpr -> lvalue = expr\n");
																				if(func_lv){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																				}
																			}
#line 1812 "parser.c" /* yacc.c:1646  */
    break;

  case 39:
#line 216 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"primary -> lvalue\n");}
#line 1818 "parser.c" /* yacc.c:1646  */
    break;

  case 40:
#line 217 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"primary -> call\n");}
#line 1824 "parser.c" /* yacc.c:1646  */
    break;

  case 41:
#line 218 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"primary -> objectdef\n");}
#line 1830 "parser.c" /* yacc.c:1646  */
    break;

  case 42:
#line 219 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"primary -> (funcdef)\n");}
#line 1836 "parser.c" /* yacc.c:1646  */
    break;

  case 43:
#line 220 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"primary -> const\n");}
#line 1842 "parser.c" /* yacc.c:1646  */
    break;

  case 44:
#line 223 "parser.y" /* yacc.c:1646  */
    {	
																				unsigned int varexists=0, tempscope=scope, is_var=1;
																				func_lv=0;
																				SymbolTableEntry * tentry;
																				is_func=0;
																				id_is_lib=0;
																				fprintf(stdout,"lvalue -> ID (%s)\n", yytext);
																				if(isLibFunc(table,yytext)){
																					// $$=$1;
																					id_is_lib=1;
																					// fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): \'%s\' shadows a library function\n",yylineno,yytext);
																					f_call =0;
																				}else if(isLibFunc(table,yytext) && f_call ){
																					id_is_lib=1;
																					fprintf(stderr,"\x1b[33mCALL LIB FUNC\x1b[0m  \' \n");
																					f_call=0;
																				}
																				else{
																					while(1){
																						tentry= ScopeLookup(table, yytext, tempscope);
																						if(tentry!=NULL){
																							varexists=1;
																							if(tentry->type==3){
																								is_var=0;
																								is_func = 1;
																								func_lv=1;
																								//fprintf(stderr,"\x1b[31mERROR\x1b[0m: found in line %d: can not use function: %s as a variable\n",yylineno,yytext);
																							}else if(tentry->type==4){
																								is_var=0;
																								func_lv=1;
																								fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): can not use library function \'%s\' as a variable\n",yylineno, yytext);
																							}
																							break;
																						}
																						if(!tempscope)
																							break;
																						
																						tempscope--;
																					}	
																					
																					if(is_var){																					
																						if(varexists){
																							if(inside_function && tentry->value.varVal->scope!=scope){
																								fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): cannot access \'%s\' inside the function\n",yylineno, yytext);
																							}else{
																								/*printf("TEMPSCOPE= %d\n", tempscope);
																								if(tempscope)
																									entry=CreateNode(yytext, tempscope, yylineno, LOC);
																								else
																									entry=CreateNode(yytext, tempscope, yylineno, GLOBAL);
																								*/
																							}
																						}
																						else{
																							if(scope)
																								entry=CreateNode(yytext, scope, yylineno, LOC);
																							else
																								entry=CreateNode(yytext, scope, yylineno, GLOBAL);
																							Insert(table, table->scopeHead, entry);
																						}
																					}
																				}
																			}
#line 1910 "parser.c" /* yacc.c:1646  */
    break;

  case 45:
#line 287 "parser.y" /* yacc.c:1646  */
    {	unsigned int varexists=0, tempscope=scope;
																				
																				if(isLibFunc(table,yytext)){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): \'%s\' shadows a library function\n",yylineno,yytext);
																					break;
																				}
																				else{
																					if(scope==0){
																						entry=CreateNode(yytext, scope, yylineno, GLOBAL);
																						Insert(table, table->scopeHead, entry);
																					}
																					else{
																						if(ScopeLookup2(table, yytext, scope)==0){
																							entry=CreateNode(yytext, scope, yylineno, LOC);
																							Insert(table, table->scopeHead, entry);
																						}
																					}
																					
																					fprintf(stdout,"lvalue -> LOCAL ID (%s)\n", yytext);
																					//fprintf(stdout,"LOCAL ID: %s\n",yytext);
																				}
																			}
#line 1937 "parser.c" /* yacc.c:1646  */
    break;

  case 46:
#line 310 "parser.y" /* yacc.c:1646  */
    {
																				if(ScopeLookup2(table, yytext, 0)==0){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): No global variable \'::%s\' exists\n",yylineno, yytext);
																				}
																			//	entry=CreateNode(yytext, 0, yylineno, GLOBAL);
																			//	Insert(table, table->scopeHead, entry);
																				fprintf(stdout,"lvalue -> :: ID (%s)\n", yytext);
																			}
#line 1950 "parser.c" /* yacc.c:1646  */
    break;

  case 47:
#line 319 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"lvalue from a \'member\'\n");}
#line 1956 "parser.c" /* yacc.c:1646  */
    break;

  case 48:
#line 322 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"member -> lvalue . ID\n");}
#line 1962 "parser.c" /* yacc.c:1646  */
    break;

  case 49:
#line 323 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"member -> lvalue [ EXPR ]\n");}
#line 1968 "parser.c" /* yacc.c:1646  */
    break;

  case 50:
#line 324 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"member -> call . ID\n");}
#line 1974 "parser.c" /* yacc.c:1646  */
    break;

  case 51:
#line 325 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"member -> call [ EXPR ]\n");}
#line 1980 "parser.c" /* yacc.c:1646  */
    break;

  case 52:
#line 328 "parser.y" /* yacc.c:1646  */
    {if(!is_func){fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): can not use function \'%s\' as a variable\n",yylineno,yytext);}

																			fprintf(stdout,"call -> (  elist ) \n");}
#line 1988 "parser.c" /* yacc.c:1646  */
    break;

  case 53:
#line 331 "parser.y" /* yacc.c:1646  */
    {if(id_is_lib){/*fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): \'%s\' shadows a library function\n",yylineno,yytext);*/}}
#line 1994 "parser.c" /* yacc.c:1646  */
    break;

  case 54:
#line 331 "parser.y" /* yacc.c:1646  */
    {	fprintf(stdout,"call -> lvalue callsuffix\n");
																				if(is_func){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): can not use function as lvalue\n",yylineno);
																				}
																			}
#line 2004 "parser.c" /* yacc.c:1646  */
    break;

  case 55:
#line 336 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"call -> (funcdef) (elist)\n");}
#line 2010 "parser.c" /* yacc.c:1646  */
    break;

  case 56:
#line 339 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"callsuffix -> normcall\n");}
#line 2016 "parser.c" /* yacc.c:1646  */
    break;

  case 57:
#line 340 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"callsuffix -> methodcall\n");}
#line 2022 "parser.c" /* yacc.c:1646  */
    break;

  case 58:
#line 343 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"normcall -> (elist)\n");}
#line 2028 "parser.c" /* yacc.c:1646  */
    break;

  case 59:
#line 347 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"methodcall -> ::ID (elist)\n");}
#line 2034 "parser.c" /* yacc.c:1646  */
    break;

  case 60:
#line 350 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"elist -> expr\n");}
#line 2040 "parser.c" /* yacc.c:1646  */
    break;

  case 62:
#line 354 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"elist -> , expr\n");}
#line 2046 "parser.c" /* yacc.c:1646  */
    break;

  case 63:
#line 355 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"elist -> empty\n");}
#line 2052 "parser.c" /* yacc.c:1646  */
    break;

  case 64:
#line 358 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"objectdef -> [elist]\n");}
#line 2058 "parser.c" /* yacc.c:1646  */
    break;

  case 65:
#line 359 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"objectdef -> [indexed]\n");}
#line 2064 "parser.c" /* yacc.c:1646  */
    break;

  case 66:
#line 362 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"indexed -> indexedelem\n");}
#line 2070 "parser.c" /* yacc.c:1646  */
    break;

  case 67:
#line 365 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"indexed -> , indexedelem\n");}
#line 2076 "parser.c" /* yacc.c:1646  */
    break;

  case 68:
#line 366 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"indexed -> empty\n");}
#line 2082 "parser.c" /* yacc.c:1646  */
    break;

  case 69:
#line 369 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"indexedelem -> {expr : expr}  \n");}
#line 2088 "parser.c" /* yacc.c:1646  */
    break;

  case 70:
#line 372 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"block -> {}\n");}
#line 2094 "parser.c" /* yacc.c:1646  */
    break;

  case 71:
#line 373 "parser.y" /* yacc.c:1646  */
    {scope++; addScope(table->scopeHead, scope);}
#line 2100 "parser.c" /* yacc.c:1646  */
    break;

  case 72:
#line 373 "parser.y" /* yacc.c:1646  */
    {
																													fprintf(stdout,"block -> {stmt*}\n");
																													Hide(table, scope);
																													scope--;
																												}
#line 2110 "parser.c" /* yacc.c:1646  */
    break;

  case 73:
#line 380 "parser.y" /* yacc.c:1646  */
    {	tempname = (char *) malloc(strlen(yytext)+1); strcpy(tempname, yytext); 				
																									unsigned int funcexists=0, tempscope=scope, flag=0, func_err=0;
																									if(isLibFunc(table,tempname)){
																										fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): %s shadows a library function\n",yylineno, tempname);
																									}
																									else{
																										SymbolTableEntry*  tentry;
																										//while(1){
																											tentry= ScopeLookup(table, tempname, tempscope);
																											
																										if(tentry!=NULL){
																											funcexists=1;
																											if(tentry->type==0){
																												flag=1;
																												fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): \'%s\' already defined as GLOBAL variable \n",yylineno, yytext);
																											}else if(tentry->type==1){
																												flag=1;
																												fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): \'%s\' already defined as LOCAL variable \n",yylineno, yytext);
																											}else if(tentry->type==2){
																												flag=1;
																												fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): \'%s\' already defined as FORMAL variable \n",yylineno, yytext);
																											}else if(tentry->type==3 && tentry->value.funcVal->scope==scope){
																												func_err=1;
																												fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): \'%s\' already defined as USER function in scope: %d\n",yylineno, yytext, scope);
																											}

																								//			break;
																										}
																									//		if(!tempscope)
																									//			break;
																											
																										//	tempscope--;
																									//	}
																										//fprintf(stdout,"funcdef!!!\n");
																										if(!func_err){
																											if(!flag){																			
																												if(funcexists){
																													entry=CreateNode(tempname, tempscope, yylineno, USERFUNC);
																													fprintf(stdout, "just added Function %s, in scope %d \n", tempname, tempscope);
																												}
																												else{
																													entry=CreateNode(tempname, scope, yylineno, USERFUNC);
																													fprintf(stdout, "just added Function %s, in scope %d \n", tempname, scope);
																												}
																												{fprintf(stdout,"funcdef -> function ID  (idlist) block\n");}
																												Insert(table, table->scopeHead, entry);
																											}
																										}
																									}
																								}
#line 2165 "parser.c" /* yacc.c:1646  */
    break;

  case 74:
#line 430 "parser.y" /* yacc.c:1646  */
    {scope++; addScope(table->scopeHead, scope); inside_function++; in_function=push(in_function, scope); }
#line 2171 "parser.c" /* yacc.c:1646  */
    break;

  case 75:
#line 431 "parser.y" /* yacc.c:1646  */
    {scope--;}
#line 2177 "parser.c" /* yacc.c:1646  */
    break;

  case 76:
#line 431 "parser.y" /* yacc.c:1646  */
    {inside_function--; pop(in_function);display(in_function);}
#line 2183 "parser.c" /* yacc.c:1646  */
    break;

  case 77:
#line 433 "parser.y" /* yacc.c:1646  */
    {scope++; addScope(table->scopeHead, scope); inside_function++; in_function=push(in_function, scope);}
#line 2189 "parser.c" /* yacc.c:1646  */
    break;

  case 78:
#line 434 "parser.y" /* yacc.c:1646  */
    {scope--;}
#line 2195 "parser.c" /* yacc.c:1646  */
    break;

  case 79:
#line 434 "parser.y" /* yacc.c:1646  */
    {	
																											char * buf = (char *) malloc(sizeof(int));
																											char* funcName = (char*) malloc(strlen(buf)+3);

																											funcCounter++;
																											itoa(funcCounter,buf);
																											strcpy(funcName, "_f");
																											funcName=strcat(funcName,buf);
																											
																											entry=CreateNode(funcName, scope, yylineno, USERFUNC);
																											Insert(table, table->scopeHead, entry);
																										}
#line 2212 "parser.c" /* yacc.c:1646  */
    break;

  case 80:
#line 446 "parser.y" /* yacc.c:1646  */
    {	fprintf(stdout,"funcdef -> function (idlist) block\n");
																											inside_function--;
																											
																											pop(in_function);
																										}
#line 2222 "parser.c" /* yacc.c:1646  */
    break;

  case 81:
#line 453 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"const -> integer '%d'\n", (yyvsp[0].integer));}
#line 2228 "parser.c" /* yacc.c:1646  */
    break;

  case 82:
#line 454 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"const -> real\n");}
#line 2234 "parser.c" /* yacc.c:1646  */
    break;

  case 83:
#line 455 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"const -> string\n");}
#line 2240 "parser.c" /* yacc.c:1646  */
    break;

  case 84:
#line 456 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"const -> nil\n");}
#line 2246 "parser.c" /* yacc.c:1646  */
    break;

  case 85:
#line 457 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"const -> true\n");}
#line 2252 "parser.c" /* yacc.c:1646  */
    break;

  case 86:
#line 458 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"const -> false\n");}
#line 2258 "parser.c" /* yacc.c:1646  */
    break;

  case 87:
#line 462 "parser.y" /* yacc.c:1646  */
    {	fprintf(stdout,"idlist -> ID (1)\n");
																				if(isLibFunc(table,yytext)){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): \'%s\' shadows a library function\n",yylineno,yytext);
																				}
																				else{
																					entry=CreateNode(yytext, scope, yylineno, FORMAL);
																					Insert(table,table->scopeHead, entry);
																				}
																			}
#line 2272 "parser.c" /* yacc.c:1646  */
    break;

  case 88:
#line 471 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"idlist -> ID idlist2\n");}
#line 2278 "parser.c" /* yacc.c:1646  */
    break;

  case 89:
#line 472 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"idlist -> empty\n");}
#line 2284 "parser.c" /* yacc.c:1646  */
    break;

  case 90:
#line 476 "parser.y" /* yacc.c:1646  */
    {	fprintf(stdout,"idlist -> ID (2)\n");
																				if(isLibFunc(table,yytext)){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): \'%s\' shadows a library function\n",yylineno,yytext);
																				}
																				else{
																					SymbolTableEntry*  tentry;
																					tentry= ScopeLookup(table, yytext, scope);
																						
																					if(tentry!=NULL){
																						fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): variable \'%s\' already defined in scope: %d\n",yylineno, yytext, scope);
																					}
																					else{
																						entry=CreateNode(yytext, scope, yylineno, FORMAL);
																						Insert(table,table->scopeHead, entry);
																					}
																				}
																			}
#line 2306 "parser.c" /* yacc.c:1646  */
    break;

  case 91:
#line 493 "parser.y" /* yacc.c:1646  */
    {	fprintf(stdout,"idlist2 -> , ID idlist2\n"); }
#line 2312 "parser.c" /* yacc.c:1646  */
    break;

  case 92:
#line 494 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"idlist -> empty\n");}
#line 2318 "parser.c" /* yacc.c:1646  */
    break;

  case 93:
#line 499 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"ifstmt -> if(expr) stmt \n");}
#line 2324 "parser.c" /* yacc.c:1646  */
    break;

  case 94:
#line 500 "parser.y" /* yacc.c:1646  */
    {fprintf(stdout,"ifstmt -> if(expr) stmt else stmt\n");}
#line 2330 "parser.c" /* yacc.c:1646  */
    break;

  case 95:
#line 503 "parser.y" /* yacc.c:1646  */
    {inside_loop++;}
#line 2336 "parser.c" /* yacc.c:1646  */
    break;

  case 96:
#line 504 "parser.y" /* yacc.c:1646  */
    {	fprintf(stdout,"whilestmt -> while(expr) stmt\n");
																				inside_loop--;
																			}
#line 2344 "parser.c" /* yacc.c:1646  */
    break;

  case 97:
#line 509 "parser.y" /* yacc.c:1646  */
    {inside_loop++;}
#line 2350 "parser.c" /* yacc.c:1646  */
    break;

  case 98:
#line 510 "parser.y" /* yacc.c:1646  */
    {	fprintf(stdout,"forstmt -> for(elist;expr;elist) stmt\n");
																				inside_loop--;
																			}
#line 2358 "parser.c" /* yacc.c:1646  */
    break;

  case 99:
#line 515 "parser.y" /* yacc.c:1646  */
    {	fprintf(stdout,"returnstmt -> return;\n");
																				if(!inside_function){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): Use of \'return\' while not in a function\n", yylineno);
																				}
																			}
#line 2368 "parser.c" /* yacc.c:1646  */
    break;

  case 100:
#line 520 "parser.y" /* yacc.c:1646  */
    {	fprintf(stdout,"returnstmt -> return expr;\n");
																				if(!inside_function){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): Use of \'return\' while not in a function\n", yylineno);
																				}
																			}
#line 2378 "parser.c" /* yacc.c:1646  */
    break;


#line 2382 "parser.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 527 "parser.y" /* yacc.c:1906  */


int yyerror (char* yaccProvidedMessage)
{
	fprintf(stderr, "\t%s: at line %d, before token:  <%s> \n", yaccProvidedMessage, yylineno, yytext);
	fprintf(stderr, "INPUT NOT VALID \n");
}


int main(int argc,char** argv){
	table=CreateTable();
    if(argc>1){
        if(!(yyin = fopen(argv[1],"r"))){
            fprintf(stderr,"Cannot read file: %s\n",argv[1]);
            return 1;
        }
    }
    else{
        yyin = stdin;
    }
    

    SymbolTableEntry * newEntry;
    newEntry= CreateNode("print", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

  	newEntry= CreateNode("input", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("objectmemberkeys", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("objecttotalmembers", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("objectcopy", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("totalarguments", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("argument", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("typeof", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("strtonum", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("sqrt", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("cos", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

	newEntry= CreateNode("sin", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);
	
	yyparse();

	printTable(table);
	
    return 0;
}
