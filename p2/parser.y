%{

/*	
 *
 *	Project Name: Lexical Analysis cs340
 *	Authors: 
 *			Fotios Mathioudakis
 *			Leonidas Anagnostou
 *			Evaggelia Athanasaki
 */






/* ========== C  CODE (includes etc) =========== */
#include "symtable.h"
#include "function_stack.h"

	int yyerror (char* yaccProvidedMessage);
//	int alpha_yylex(void* yylval);
	int yylex(void);

	extern int yylineno;
	extern char* yytext;
	extern FILE* yyin;
	//extern unsigned int scope=0;
	SymbolTable* table;
	SymbolTableEntry* entry;
	unsigned int funcCounter=0;
	char* tempname;
	struct f_stack* in_function=NULL;
	int inside_function=0;
	int inside_loop=0;
	int is_func = 0;
	int func_lv= 0;
	int id_is_lib= 0;
	int f_call = 0;
	int f_call2 = 0;


/* ========== YACC definitions etc =========== */
%}

%start program

%union {
	int integer;
	double real;
	char* string;
}

%token IF
%token ELSE
%token WHILE
%token FOR
%token FUNCTION
%token RETURN
%token BREAK
%token CONTINUE
%token AND
%token NOT
%token OR
%token LOCAL
%token TRUE
%token FALSE
%token NIL

%token ASSIGN
%token PLUS
%token MINUS
%token MUL
%token DIV
%token MOD
%token EQ
%token NEQ
%token PP
%token MM
%token GREATER
%token LESS
%token GEQ
%token LEQ

%token <integer> INTEGER
%token <real> REAL
%token <string> ID
%token <string> STR

%token WHITESPACE

%token BRKT_O
%token BRKT_C
%token SBRKT_O
%token SBRKT_C
%token PAR_O
%token PAR_C
%token SEMICOL
%token COMMA
%token COLON
%token COLON2
%token DOT
%token DOT2

%nonassoc LOWER_THAN_ELSE
%nonassoc ELSE


%{/* TOP(low) -> BOTTOM(high) */%}
%right ASSIGN
%left OR
%left AND
%nonassoc EQ NEQ
%nonassoc GREATER LESS GEQ LEQ
%left PLUS MINUS
%left MUL DIV MOD
%{/* warning!! check the MINUS CASE */%}
%right NOT PP MM UMINUS
%left DOT DOT2
%left SBRKT_O SBRKT_C
%left PAR_O PAR_C










%%



program:			programcont												{fprintf(stdout,"program -> programcont\n");}
					| 														{fprintf(stdout,"program -> \n");}
					;

programcont:			stmt												{fprintf(stdout,"programcont -> stmt\n");}
					| programcont stmt 										{fprintf(stdout,"programcont -> programcont stmt\n");}
					;

stmt:				expr SEMICOL											{fprintf(stdout,"stmt -> expr;\n");}
					| ifstmt												{fprintf(stdout,"stmt -> ifstmt\n");}
					| whilestmt												{fprintf(stdout,"stmt -> whilestmt\n");}
					| forstmt												{fprintf(stdout,"stmt -> forstmt\n");}
					| returnstmt											{fprintf(stdout,"stmt -> returnstmt\n");}
					| BREAK SEMICOL											{	fprintf(stdout,"stmt -> break;\n");
																				if(!inside_loop){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): Use of \'break\' while not in a loop\n",yylineno);
																				}
																			}
					| CONTINUE SEMICOL										{	fprintf(stdout,"stmt -> continue;\n");
																				if(!inside_loop){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): Use of \'continue\' while not in a loop\n",yylineno);
																				}
																			}
					| block													{fprintf(stdout,"stmt -> block\n");}
					| funcdef												{fprintf(stdout,"stmt -> funcdef\n");}
					;


expr:				assignexpr												{fprintf(stdout,"expr -> assignexpr\n");}
					| expr PLUS expr										{fprintf(stdout,"expr -> expr + expr\n");}
					| expr MINUS expr										{fprintf(stdout,"expr -> expr - expr\n");}
					| expr MUL expr 										{fprintf(stdout,"expr -> expr * expr\n");}
					| expr DIV expr 										{fprintf(stdout,"expr -> expr / expr\n");}
					| expr MOD expr 										{fprintf(stdout,"expr -> expr \'%\' expr\n");}
					| expr GREATER expr 									{fprintf(stdout,"expr -> expr > expr\n");}
					| expr LESS expr 										{fprintf(stdout,"expr -> expr < expr\n");}
					| expr GEQ expr 										{fprintf(stdout,"expr -> expr => expr\n");}
					| expr LEQ expr 										{fprintf(stdout,"expr -> expr <= expr\n");}
					| expr EQ expr  										{fprintf(stdout,"expr -> expr == expr\n");}
					| expr NEQ expr 										{fprintf(stdout,"expr -> expr != expr\n");}
					| expr AND expr 										{fprintf(stdout,"expr -> expr and expr\n");}
					| expr OR expr 											{fprintf(stdout,"expr -> expr or expr\n");}
					| term													{fprintf(stdout,"expr -> term\n");}
					;


term:				PAR_O expr PAR_C										{fprintf(stdout,"term -> (expr)\n");}
					| MINUS expr %prec UMINUS								{fprintf(stdout,"term -> -expr\n");}
					| NOT expr 												{fprintf(stdout,"term -> not expr\n");}
					| PP lvalue 											{fprintf(stdout,"term -> ++lvalue\n");
																				if(func_lv){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																				}
																			}
					| lvalue PP 											{fprintf(stdout,"term -> lvalue++\n");
																				if(func_lv){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																				}
																			}
					| MM lvalue												{fprintf(stdout,"term -> --lvalue\n");
																				if(func_lv){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																				}
																			}
					| lvalue MM 											{fprintf(stdout,"term -> lvalue--\n");
																				if(func_lv){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																				}
																			}
					| primary 												{fprintf(stdout,"term -> primary\n");}
					;


assignexpr:			lvalue ASSIGN expr 										{	fprintf(stdout,"assignexpr -> lvalue = expr\n");
																				if(func_lv){
																					fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): can not use FUNCTION as lvalue\n",yylineno );
																				}
																			}
					;

primary:			lvalue													{fprintf(stdout,"primary -> lvalue\n");}												
					| call 													{fprintf(stdout,"primary -> call\n");}
					| objectdef 											{fprintf(stdout,"primary -> objectdef\n");}
					| PAR_O funcdef PAR_C 									{fprintf(stdout,"primary -> (funcdef)\n");}
					| const 												{fprintf(stdout,"primary -> const\n");}
					;

lvalue: 			ID 														{	
																				unsigned int varexists=0, tempscope=scope, is_var=1;
																				func_lv=0;
																				SymbolTableEntry * tentry;
																				is_func=0;
																				id_is_lib=0;
																				fprintf(stdout,"lvalue -> ID (%s)\n", yytext);
																				if(isLibFunc(table,yytext)){
																					// $$=$1;
																					id_is_lib=1;
																					// fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): \'%s\' shadows a library function\n",yylineno,yytext);
																					f_call =0;
																				}else if(isLibFunc(table,yytext) && f_call ){
																					id_is_lib=1;
																					fprintf(stderr,"\x1b[33mCALL LIB FUNC\x1b[0m  \' \n");
																					f_call=0;
																				}
																				else{
																					while(1){
																						tentry= ScopeLookup(table, yytext, tempscope);
																						if(tentry!=NULL){
																							varexists=1;
																							if(tentry->type==3){
																								is_var=0;
																								is_func = 1;
																								func_lv=1;
																								//fprintf(stderr,"\x1b[31mERROR\x1b[0m: found in line %d: can not use function: %s as a variable\n",yylineno,yytext);
																							}else if(tentry->type==4){
																								is_var=0;
																								func_lv=1;
																								fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): can not use library function \'%s\' as a variable\n",yylineno, yytext);
																							}
																							break;
																						}
																						if(!tempscope)
																							break;
																						
																						tempscope--;
																					}	
																					
																					if(is_var){																					
																						if(varexists){
																							if(inside_function && tentry->value.varVal->scope!=scope){
																								fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): cannot access \'%s\' inside the function\n",yylineno, yytext);
																							}else{
																								/*printf("TEMPSCOPE= %d\n", tempscope);
																								if(tempscope)
																									entry=CreateNode(yytext, tempscope, yylineno, LOC);
																								else
																									entry=CreateNode(yytext, tempscope, yylineno, GLOBAL);
																								*/
																							}
																						}
																						else{
																							if(scope)
																								entry=CreateNode(yytext, scope, yylineno, LOC);
																							else
																								entry=CreateNode(yytext, scope, yylineno, GLOBAL);
																							Insert(table, table->scopeHead, entry);
																						}
																					}
																				}
																			}

					| LOCAL ID 												{	unsigned int varexists=0, tempscope=scope;
																				
																				if(isLibFunc(table,yytext)){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): \'%s\' shadows a library function\n",yylineno,yytext);
																					break;
																				}
																				else{
																					if(scope==0){
																						entry=CreateNode(yytext, scope, yylineno, GLOBAL);
																						Insert(table, table->scopeHead, entry);
																					}
																					else{
																						if(ScopeLookup2(table, yytext, scope)==0){
																							entry=CreateNode(yytext, scope, yylineno, LOC);
																							Insert(table, table->scopeHead, entry);
																						}
																					}
																					
																					fprintf(stdout,"lvalue -> LOCAL ID (%s)\n", yytext);
																					//fprintf(stdout,"LOCAL ID: %s\n",yytext);
																				}
																			}

					| COLON2 ID 											{
																				if(ScopeLookup2(table, yytext, 0)==0){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): No global variable \'::%s\' exists\n",yylineno, yytext);
																				}
																			//	entry=CreateNode(yytext, 0, yylineno, GLOBAL);
																			//	Insert(table, table->scopeHead, entry);
																				fprintf(stdout,"lvalue -> :: ID (%s)\n", yytext);
																			}

					| member												{fprintf(stdout,"lvalue from a \'member\'\n");}
					;

member:				lvalue DOT ID 											{fprintf(stdout,"member -> lvalue . ID\n");}															
					| lvalue SBRKT_O expr SBRKT_C 							{fprintf(stdout,"member -> lvalue [ EXPR ]\n");}
					| call DOT ID 											{fprintf(stdout,"member -> call . ID\n");}
					| call SBRKT_O expr SBRKT_C 							{fprintf(stdout,"member -> call [ EXPR ]\n");}
					;

call:				call PAR_O elist PAR_C 									{if(!is_func){fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): can not use function \'%s\' as a variable\n",yylineno,yytext);}

																			fprintf(stdout,"call -> (  elist ) \n");}							
					| lvalue {if(id_is_lib){/*fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): \'%s\' shadows a library function\n",yylineno,yytext);*/}} callsuffix 									{	fprintf(stdout,"call -> lvalue callsuffix\n");
																				if(is_func){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): can not use function as lvalue\n",yylineno);
																				}
																			}			
					| PAR_O funcdef PAR_C PAR_O elist PAR_C 				{fprintf(stdout,"call -> (funcdef) (elist)\n");}
					;

callsuffix:			normcall 												{fprintf(stdout,"callsuffix -> normcall\n");}
					| methodcall											{fprintf(stdout,"callsuffix -> methodcall\n");}
					;

normcall:			PAR_O  elist  PAR_C 										{fprintf(stdout,"normcall -> (elist)\n");}
					;


methodcall:			DOT2 ID  PAR_O   elist  PAR_C  								{fprintf(stdout,"methodcall -> ::ID (elist)\n");}
					;

elist:				expr elist2												{fprintf(stdout,"elist -> expr\n");}
					|												
					;

elist2:				COMMA expr elist2 										{fprintf(stdout,"elist -> , expr\n");}
					|														{fprintf(stdout,"elist -> empty\n");}
					;

objectdef:			SBRKT_O elist2 SBRKT_C									{fprintf(stdout,"objectdef -> [elist]\n");}
					| SBRKT_O indexed SBRKT_C								{fprintf(stdout,"objectdef -> [indexed]\n");}
					;

indexed:			indexedelem	indexed2									{fprintf(stdout,"indexed -> indexedelem\n");}
					;

indexed2:			COMMA indexedelem indexed2 								{fprintf(stdout,"indexed -> , indexedelem\n");}
 					|														{fprintf(stdout,"indexed -> empty\n");}
					;

indexedelem:		BRKT_O expr COLON expr BRKT_C 							{fprintf(stdout,"indexedelem -> {expr : expr}  \n");}
					;   

block:				BRKT_O BRKT_C											{fprintf(stdout,"block -> {}\n");}
					| BRKT_O {scope++; addScope(table->scopeHead, scope);} programcont BRKT_C 					{
																													fprintf(stdout,"block -> {stmt*}\n");
																													Hide(table, scope);
																													scope--;
																												}
					;

funcdef:			FUNCTION ID 																{	tempname = (char *) malloc(strlen(yytext)+1); strcpy(tempname, yytext); 				
																									unsigned int funcexists=0, tempscope=scope, flag=0, func_err=0;
																									if(isLibFunc(table,tempname)){
																										fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): %s shadows a library function\n",yylineno, tempname);
																									}
																									else{
																										SymbolTableEntry*  tentry;
																										//while(1){
																											tentry= ScopeLookup(table, tempname, tempscope);
																											
																										if(tentry!=NULL){
																											funcexists=1;
																											if(tentry->type==0){
																												flag=1;
																												fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): \'%s\' already defined as GLOBAL variable \n",yylineno, yytext);
																											}else if(tentry->type==1){
																												flag=1;
																												fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): \'%s\' already defined as LOCAL variable \n",yylineno, yytext);
																											}else if(tentry->type==2){
																												flag=1;
																												fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): \'%s\' already defined as FORMAL variable \n",yylineno, yytext);
																											}else if(tentry->type==3 && tentry->value.funcVal->scope==scope){
																												func_err=1;
																												fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): \'%s\' already defined as USER function in scope: %d\n",yylineno, yytext, scope);
																											}

																								//			break;
																										}
																									//		if(!tempscope)
																									//			break;
																											
																										//	tempscope--;
																									//	}
																										//fprintf(stdout,"funcdef!!!\n");
																										if(!func_err){
																											if(!flag){																			
																												if(funcexists){
																													entry=CreateNode(tempname, tempscope, yylineno, USERFUNC);
																													fprintf(stdout, "just added Function %s, in scope %d \n", tempname, tempscope);
																												}
																												else{
																													entry=CreateNode(tempname, scope, yylineno, USERFUNC);
																													fprintf(stdout, "just added Function %s, in scope %d \n", tempname, scope);
																												}
																												{fprintf(stdout,"funcdef -> function ID  (idlist) block\n");}
																												Insert(table, table->scopeHead, entry);
																											}
																										}
																									}
																								}
					PAR_O {scope++; addScope(table->scopeHead, scope); inside_function++; in_function=push(in_function, scope); }
					idlist {scope--;} PAR_C block {inside_function--; pop(in_function);display(in_function);}	
																																												
					| FUNCTION PAR_O {scope++; addScope(table->scopeHead, scope); inside_function++; in_function=push(in_function, scope);}
					idlist {scope--;} PAR_C  															{	
																											char * buf = (char *) malloc(sizeof(int));
																											char* funcName = (char*) malloc(strlen(buf)+3);

																											funcCounter++;
																											itoa(funcCounter,buf);
																											strcpy(funcName, "_f");
																											funcName=strcat(funcName,buf);
																											
																											entry=CreateNode(funcName, scope, yylineno, USERFUNC);
																											Insert(table, table->scopeHead, entry);
																										}
					block																				{	fprintf(stdout,"funcdef -> function (idlist) block\n");
																											inside_function--;
																											
																											pop(in_function);
																										}
					;

const:				INTEGER 												{fprintf(stdout,"const -> integer '%d'\n", $1);}
					| REAL 													{fprintf(stdout,"const -> real\n");}
					| STR 													{fprintf(stdout,"const -> string\n");}
					| NIL 													{fprintf(stdout,"const -> nil\n");}
					| TRUE 													{fprintf(stdout,"const -> true\n");}
					| FALSE 												{fprintf(stdout,"const -> false\n");}
					;

idlist:																		
					ID														{	fprintf(stdout,"idlist -> ID (1)\n");
																				if(isLibFunc(table,yytext)){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): \'%s\' shadows a library function\n",yylineno,yytext);
																				}
																				else{
																					entry=CreateNode(yytext, scope, yylineno, FORMAL);
																					Insert(table,table->scopeHead, entry);
																				}
																			}
					idlist2 												{fprintf(stdout,"idlist -> ID idlist2\n");}	
					|														{fprintf(stdout,"idlist -> empty\n");}
					;

idlist2:																		
					COMMA ID												{	fprintf(stdout,"idlist -> ID (2)\n");
																				if(isLibFunc(table,yytext)){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): \'%s\' shadows a library function\n",yylineno,yytext);
																				}
																				else{
																					SymbolTableEntry*  tentry;
																					tentry= ScopeLookup(table, yytext, scope);
																						
																					if(tentry!=NULL){
																						fprintf(stderr, "\x1b[31mERROR\x1b[0m line(%d): variable \'%s\' already defined in scope: %d\n",yylineno, yytext, scope);
																					}
																					else{
																						entry=CreateNode(yytext, scope, yylineno, FORMAL);
																						Insert(table,table->scopeHead, entry);
																					}
																				}
																			}
					idlist2 												{	fprintf(stdout,"idlist2 -> , ID idlist2\n"); }						
					|														{fprintf(stdout,"idlist -> empty\n");}
					;



ifstmt:				IF PAR_O expr PAR_C stmt %prec LOWER_THAN_ELSE			{fprintf(stdout,"ifstmt -> if(expr) stmt \n");}
					|IF PAR_O expr PAR_C stmt ELSE stmt 					{fprintf(stdout,"ifstmt -> if(expr) stmt else stmt\n");}
					;

whilestmt:			WHILE PAR_O expr PAR_C									{inside_loop++;} 
					stmt 													{	fprintf(stdout,"whilestmt -> while(expr) stmt\n");
																				inside_loop--;
																			}
					;

forstmt:			FOR PAR_O elist SEMICOL expr SEMICOL elist PAR_C 		{inside_loop++;}
					stmt 													{	fprintf(stdout,"forstmt -> for(elist;expr;elist) stmt\n");
																				inside_loop--;
																			}
					;

returnstmt:			RETURN SEMICOL 											{	fprintf(stdout,"returnstmt -> return;\n");
																				if(!inside_function){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): Use of \'return\' while not in a function\n", yylineno);
																				}
																			}
					| RETURN expr SEMICOL 									{	fprintf(stdout,"returnstmt -> return expr;\n");
																				if(!inside_function){
																					fprintf(stderr,"\x1b[31mERROR\x1b[0m line(%d): Use of \'return\' while not in a function\n", yylineno);
																				}
																			}
					;

%%

int yyerror (char* yaccProvidedMessage)
{
	fprintf(stderr, "\t%s: at line %d, before token:  <%s> \n", yaccProvidedMessage, yylineno, yytext);
	fprintf(stderr, "INPUT NOT VALID \n");
}


int main(int argc,char** argv){
	table=CreateTable();
    if(argc>1){
        if(!(yyin = fopen(argv[1],"r"))){
            fprintf(stderr,"Cannot read file: %s\n",argv[1]);
            return 1;
        }
    }
    else{
        yyin = stdin;
    }
    

    SymbolTableEntry * newEntry;
    newEntry= CreateNode("print", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

  	newEntry= CreateNode("input", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("objectmemberkeys", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("objecttotalmembers", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("objectcopy", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("totalarguments", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("argument", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("typeof", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("strtonum", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("sqrt", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

    newEntry= CreateNode("cos", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);

	newEntry= CreateNode("sin", 0, 0, LIBFUNC);
    Insert(table, table->scopeHead, newEntry);
	
	yyparse();

	printTable(table);
	
    return 0;
}