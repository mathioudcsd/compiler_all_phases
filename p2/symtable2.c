 //test
#include "symtable.h"

unsigned int HashFunction(const char* x){
	unsigned int i=0;  
	unsigned int hashNum=0;
	return	hashNum=(hashNum*314+(unsigned int)x[0])%HASHSIZE;
}

SymbolTable* CreateTable(){
	int i;
	SymbolTable* new;
	new=(SymbolTable*)malloc(sizeof(SymbolTable));
	
	if(new==NULL){
		fprintf(stderr, "Malloc Error: new Symbol table\n");
		return NULL;
	}
	
	new->scopeHead=(ScopeList*) malloc(sizeof(ScopeList) ); 
	new->scopeHead->symTableNode = NULL; 
	new->scopeHead->next=NULL; 

	for(i=0;i<HASHSIZE;i++)
		new->hashTable[i]=NULL;
	return new;
}

int addScope(ScopeList* scopeList, unsigned int scope){
	ScopeList* scopePtr=scopeList;
	unsigned int hop=0;

	//uparxei
	if(scopePtr!= NULL && scope==0){
		return 1;
	}
	

	while(scopePtr->next != NULL ){
		if(hop == scope){
			return 1;
		}
		hop++;
		scopePtr = scopePtr->next;
	}

	ScopeList* newscope= (ScopeList*) malloc(sizeof(ScopeList) );
	newscope->symTableNode= NULL;
	newscope->next= NULL;	
	scopePtr->next=newscope;
	return 0;
}




SymbolTableEntry* CreateNode(const char* name, unsigned int scope, unsigned int line, enum SymbolTableType type){
	char* nameBuff;
	// Arguments* argPtr;
	// Arguments* currArgBuff;
	// Arguments* prevArgBuff;
	Variable* varBuff;
	Function* funcBuff;

	SymbolTableEntry* buff = (SymbolTableEntry*)malloc(sizeof(SymbolTableEntry));

	if(buff==NULL){
		fprintf(stderr, "Malloc Error: new Node creation\n");
		return NULL;
	}

	switch(type){
		case 0:
		case 1:
		case 2:
			varBuff = (Variable*)malloc(sizeof(Variable));
			if(varBuff==NULL){
				fprintf(stderr, "Malloc Error: varVal buff\n");
			}
			nameBuff=(char*)malloc(strlen(name));
			nameBuff=strdup(name);
			buff->value.varVal=varBuff;
			buff->value.varVal->name=nameBuff;
			buff->value.varVal->scope=scope;
			buff->value.varVal->line=line;
			break;
		case 3:
		case 4:
			funcBuff = (Function*)malloc(sizeof(Function));
			if(funcBuff==NULL){
				fprintf(stderr, "Malloc Error: funcVal buff\n");
			}
			nameBuff=(char*)malloc(strlen(name));
			nameBuff=strdup(name);
			buff->value.funcVal=funcBuff;
			buff->value.funcVal->name=nameBuff;
			buff->value.funcVal->scope=scope;
			buff->value.funcVal->line=line;

			// if(args==NULL){
			// 	buff->value.funcVal-> args = NULL;
			// }
			// else{
			// 	argPtr=args;
			// 	currArgBuff=(Arguments*)malloc(sizeof(Arguments));
			// 	currArgBuff->name = (const char*)malloc(strlen(argPtr->name));
			// 	currArgBuff->name = strdup(argPtr->name);
			// 	currArgBuff->next=NULL;
			// 	prevArgBuff=currArgBuff;
			// 	buff->value.funcVal->args = currArgBuff;
			// 	argPtr=argPtr->next;
			// 	while(argPtr!=NULL){
			// 		currArgBuff=(Arguments*)malloc(sizeof(Arguments));
			// 		currArgBuff->name = (const char*)malloc(strlen(argPtr->name));
			// 		currArgBuff->name = strdup(argPtr->name);
			// 		currArgBuff->next=NULL;
			// 		prevArgBuff->next=currArgBuff;
			// 		prevArgBuff=currArgBuff;
			// 		argPtr=argPtr->next;
			// 	}
			//}
			break;
	}
	buff->isActive=1;
	buff->type=type; 
	buff->nextInTable=NULL; 
	buff->nextInScope=NULL;
	
	return buff;
}


int Insert(SymbolTable* tbl, ScopeList* scopeList, SymbolTableEntry* newNode){
	unsigned int hash;
	unsigned int scope;
	unsigned int i=0;
	ScopeList* scopePtr=scopeList;
	SymbolTableEntry* symEntry;
	SymbolTableEntry* buffer;
	SymbolTableEntry* curr;

	switch(newNode->type){
		case 0:
		case 1:
		case 2:
			hash=HashFunction(newNode->value.varVal->name);
			scope=newNode->value.varVal->scope;
			break;
		case 3:
		case 4:
			hash=HashFunction(newNode->value.funcVal->name);
			scope=newNode->value.funcVal->scope;
			break;
	}

	if(tbl->hashTable[hash]==NULL){
		tbl->hashTable[hash]=newNode;
		tbl->hashTable[hash]->nextInTable=NULL;
	}
	else{ 
		curr=tbl->hashTable[hash];
		while(curr->nextInTable!=NULL){
			curr=curr->nextInTable;
		}
		curr->nextInTable=newNode;
		curr=newNode;
		curr->nextInTable=NULL;
	}

	addScope(scopeList, scope);
	/* add sto scopeList */
	scopePtr = scopeList;
	while(i<scope){
		scopePtr=scopePtr->next;
		i++;
	}
	if(scopePtr->symTableNode==NULL){
		scopePtr->symTableNode=newNode;
		scopePtr->next=NULL;
	}
	else{
		symEntry=scopePtr->symTableNode;
		while(symEntry->nextInScope!=NULL){
			symEntry=symEntry->nextInScope;
		}
		symEntry->nextInScope=newNode;
	}
	return 0;
}


void Hide(SymbolTable* tbl, unsigned int scope){
	ScopeList* slist= tbl->scopeHead;
	unsigned int num= 0;

	while(num<scope){
		if(slist->next==NULL){
			fprintf(stderr, "Hide Error: scope does not exist\n");
			return;
		}
		slist= slist->next;
		num++;
	}
	
	SymbolTableEntry * buffer= slist->symTableNode;

	if(num>0){
		while(buffer){
		if(buffer->isActive==1){
			if(buffer->type<3)
				fprintf(stdout, "De-Activating variable : %s\n",buffer->value.varVal->name);
			else
				fprintf(stdout, "De-Activating function : %s\n",buffer->value.funcVal->name);
		}
		buffer->isActive= 0;
		buffer= buffer->nextInScope;
		}
	}
	

	return;
}




SymbolTableEntry* Lookup(SymbolTable* tbl, const char* name){
	unsigned int key= HashFunction(name);
	SymbolTableEntry * buffer= tbl->hashTable[key];

	while(buffer){
		switch(buffer->type){
		case 0:
		case 1:
		case 2:
			if( strcmp(buffer->value.varVal->name, name )==0 )
				return buffer;
			break;
		case 3:
		case 4:
			if( strcmp(buffer->value.funcVal->name, name )==0 )
				return buffer;
			break;
		}

		buffer= buffer->nextInTable;
	}

	return NULL;
}


SymbolTableEntry* ScopeLookup(SymbolTable* tbl, const char* name, unsigned int scope){
	ScopeList* slist= tbl->scopeHead;
	unsigned int num= 0;

	while(num<scope){
		if(slist->next==NULL){
			sleep(1);
			fprintf(stderr, "ScopeLookup Error: scope does not exist\n");
			return NULL;
		}
		slist= slist->next;
		num++;
	}
	
	SymbolTableEntry * buffer= slist->symTableNode;

	while(buffer){
		switch(buffer->type){
		case 0:
		case 1:
		case 2:
			if( strcmp(buffer->value.varVal->name, name )==0 ){
				if(buffer->isActive)
					return buffer;
			}
			break;
		case 3:
		case 4:
			if( strcmp(buffer->value.funcVal->name, name )==0 ){
				if(buffer->isActive)
					return buffer;
			}
			break;
		}
		buffer= buffer->nextInScope;
	}

	return NULL;
}


int ScopeLookup2(SymbolTable* tbl, const char* name, unsigned int scope){
	ScopeList* slist= tbl->scopeHead;
	unsigned int num= 0;

	while(num<scope){
		if(slist->next==NULL){
			printf("EEEEEEEEERor\n");
			return 0;
		}
		slist= slist->next;
		num++;
	}
	
	SymbolTableEntry * buffer= slist->symTableNode;

	while(buffer){
		switch(buffer->type){
		case 0:
		case 1:
		case 2:
			if( strcmp(buffer->value.varVal->name, name )==0 ){
				if(buffer->isActive)
					return 1;
			}
			break;
		case 3:
		case 4:
			if( strcmp(buffer->value.funcVal->name, name )==0 ){
				if(buffer->isActive)
					return 1;
			}
			break;
		}
		buffer= buffer->nextInScope;
	}

	return 0;
}




void printTable(SymbolTable* s){
	int i = 0;
	SymbolTableEntry* ptr=s->hashTable[0];
	int type;
	fprintf(stdout, "===================================\n" );
	fprintf(stdout, "LINE \t SCOPE \t TYPE \t\t\t NAME\n");
	fprintf(stdout, "______________________________________________________\n");
	while(i<HASHSIZE){
		ptr=s->hashTable[i];
		while(ptr!=NULL){
			type=(int)ptr->type;
			switch (ptr->type){
				case 0:
					fprintf(stdout, "%d \t (%d) \t GLOBAL VARIABLE \t %s\n", ptr->value.varVal->line, ptr->value.varVal->scope,ptr->value.varVal->name );
					break;
				case 1:
					fprintf(stdout, "%d \t (%d) \t LOCAL VARIABLE \t %s\n", ptr->value.varVal->line, ptr->value.varVal->scope,ptr->value.varVal->name);
					break;
				case 2:
					fprintf(stdout, "%d \t (%d) \t FORMAL VARIABLE \t %s\n", ptr->value.varVal->line, ptr->value.varVal->scope,ptr->value.varVal->name);
					break;
				case 3:
					fprintf(stdout, "%d \t (%d) \t USERFUNC \t\t %s\n", ptr->value.funcVal->line, ptr->value.funcVal->scope,ptr->value.funcVal->name );
					break;
				case 4:
					//fprintf(stdout, "%d \t (%d) \t LIBFUNC \t\t %s\n", ptr->value.funcVal->line,ptr->value.funcVal->scope, ptr->value.funcVal->name );
					break;	
			}
			ptr=ptr->nextInTable;
		}
		i++;
	}
}

void printScopeTable(ScopeList* scopeList){
	ScopeList* scopePtr =scopeList;
	SymbolTableEntry* ptr=scopePtr->symTableNode;
	int type;
	while(scopePtr!=NULL){
		while(ptr!=NULL){
			type=(int)ptr->type;
			if(type<3 && type>=0){
				fprintf(stdout, "%d ", ptr->value.varVal->scope);
				fprintf(stdout, "%s \n",ptr->value.varVal->name);
			}
			else{
				int i=0;
				//while(ptr->value.funcVal->args !=NULL){
					fprintf(stdout, "%d ", ptr->value.funcVal->scope);
					fprintf(stdout, "%s \t",ptr->value.funcVal->name);
					//printf("argument %d = %s\n",i++, ptr->value.funcVal->args->name );
					//ptr->value.funcVal->args=ptr->value.funcVal->args->next;
				//}
			}
			ptr=ptr->nextInScope;
		}
		scopePtr=scopePtr->next;
		if(scopePtr==NULL)
			break;
		ptr=scopePtr->symTableNode;
	}
}

// Arguments* CreateArgsList(Arguments* args,const char* a){
// 	 Arguments* head = args;

// 	if (head==NULL){
// 		Arguments *node = (Arguments*) malloc(sizeof(Arguments));
// 		node -> name = a;
// 		node -> next = NULL;
// 		head = node;
// 		return head;
// 	}
// 	else{
// 		Arguments *temp = head;
// 		while(temp -> next!=NULL){
// 			temp = temp -> next;
// 		}
// 		Arguments *node = (Arguments*) malloc(sizeof(Arguments));
// 		node ->name = a;
// 		node -> next = NULL;		
// 		temp -> next = node;
// 		return head;
// 	}
	
// }

 void reverse(char* s)
 {
     int i, j;
     char c;
 
     for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
         c = s[i];
         s[i] = s[j];
         s[j] = c;
     }
 }

void itoa(int n, char* s)
 {
     int i, sign;
 
     if ((sign = n) < 0)  /* record sign */
         n = -n;          /* make n positive */
     i = 0;
     do {       /* generate digits in reverse order */
         s[i++] = n % 10 + '0';   /* get next digit */
     } while ((n /= 10) > 0);     /* delete it */
     if (sign < 0)
         s[i++] = '-';
     s[i] = '\0';
     reverse(s);
 }

int isLibFunc(SymbolTable* s,const char* text){
	ScopeList* slist= s->scopeHead;
	unsigned int num= 0;

	SymbolTableEntry * buffer= slist->symTableNode;

	while(num<12){
		
		if( strcmp(buffer->value.funcVal->name, text )==0 ){
			return 1;
		}
		num++;
		buffer= buffer->nextInScope;
	}
	return 0;
}

void report_error(int lineno, char* text){
	fprintf(stderr,"\x1b[31mERROR line(%d): \'%s\' shadows a library function\n\x1b[0m", lineno, text);
}




/*
int main(void){
			int i=0;
	SymbolTable* table = CreateTable();
	
	SymbolTableEntry* a = CreateNode("ohh", 0, 22, 2, NULL);
	Insert(table, table->scopeHead, a);


	a = CreateNode("darling", 1, 20, 2, NULL);
	Insert(table, table->scopeHead, a);

	a = CreateNode("what", 2, 20, 2, NULL);
	Insert(table, table->scopeHead, a);

	addScope(table->scopeHead, 3);
	a = CreateNode("have", 4, 20, 2, NULL);
	Insert(table, table->scopeHead, a);


	printTable(table);
	return 1;
}
/*
	a = CreateNode("i", 4, 20, 2, NULL);
	Insert(table, table->scopeHead, a);


	a = CreateNode("done", 0, 20, 2, NULL);
	Insert(table, table->scopeHead, a);

	a = CreateNode("done", 4, 20, 2, NULL);
	Insert(table, table->scopeHead, a);

	a = CreateNode("done", 5, 20, 2, NULL);
	Insert(table, table->scopeHead, a);

	a = CreateNode("done", 6, 20, 2, NULL);
	Insert(table, table->scopeHead, a);

	a = CreateNode("done", 7, 20, 2, NULL);
	Insert(table, table->scopeHead, a);

 	printScopeTable(table->scopeHead);
	

	printTable(table);

	//----------LOOKUPS tests---------------------*/

/*	if(ScopeLookup(table, "page", 1))
		fprintf(stdout, "page found in scope 1\n");
	else
		fprintf(stdout, "page NOT found in scope 1\n");
	

	if(ScopeLookup(table, "page", 30))
		fprintf(stdout, "page found in scope 30\n");
	else
		fprintf(stdout, "page NOT found in scope 30\n");

	if(ScopeLookup(table, "darling",1))
		fprintf(stdout, "darling found\n");
	else
		fprintf(stdout, "darling NOT found\n");


	return 0;


}
*/